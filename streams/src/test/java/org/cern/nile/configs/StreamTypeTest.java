package org.cern.nile.configs;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;

class StreamTypeTest {

  @Test
  void findByValue_MapsToRouting_forValueRouting() {
    assertEquals(StreamType.ROUTING, StreamType.valueOf("ROUTING"));
  }

  @Test
  void findByValue_MapsToDecoding_forValueDecoding() {
    assertEquals(StreamType.DECODING, StreamType.valueOf("DECODING"));
  }

  @Test
  void findByValue_MapsToEnrichment_forValueEnrichment() {
    assertEquals(StreamType.ENRICHMENT, StreamType.valueOf("ENRICHMENT"));
  }

  @Test
  void findByValue_ThrowsRuntimeException_forUnknownStreamType() {
    assertThrows(IllegalArgumentException.class, () -> StreamType.valueOf("Unknown"));
  }
}
