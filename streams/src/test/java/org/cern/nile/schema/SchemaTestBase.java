package org.cern.nile.schema;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import org.junit.jupiter.api.BeforeEach;

public class SchemaTestBase {
  public Map<String, Object> data;

  @BeforeEach
  void setUp() {
    data = new HashMap<>();
    data.put("byte_col", (byte) 1);
    data.put("short_col", (short) 2);
    data.put("int_col", 3);
    data.put("long_col", (long) 4);
    data.put("float_col", 5.0f);
    data.put("double_col", 6.0);
    data.put("boolean_col", true);
    data.put("string_col", "test");
    data.put("timestamp_col", 1501834166000L);
    data.put("date_col", new Date());
    data.put("bytes_col", new byte[]{1, 2, 3});
  }
}
