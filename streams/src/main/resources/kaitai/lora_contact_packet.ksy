meta:
  id: lora_contact_packet
  endian: le

seq:
  - id: seq
    type: u2
    doc: tag internal sequence number to detect duplicates
  - id: my_tag_id
    type: u2
    doc: Own tag id
  - id: encounters
    type: encounter_record
    repeat: eos

types:
  encounter_record:
    seq:
      - id: rf_tag_id
        type: u2
      - id: num_contacts
        type: u2
      - id: started_at
        type: packed_time

  packed_time:
    seq:
      - id: raw_time
        type: u2
        doc: Had to result to operations as this bit-packed string uses non-consecutive bits being a short in little endian
    instances:
      minute:
        value: raw_time & 0x3f
      hour:
        value: raw_time >> 6 & 0x1f
        doc: In 24 hour format
      day:
        value: raw_time >> 11 & 0x1f
