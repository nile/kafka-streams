package org.cern.nile.decoders.generated;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import com.google.gson.JsonParser;
import java.util.Map;
import org.cern.nile.decoders.KaitaiPacketDecoder;
import org.cern.nile.exceptions.DecodingException;
import org.junit.jupiter.api.Test;

public class LoraEnAccessControlDecoderTest {

  private static final String LORA_EN_ACCESS_CONTROL_DATA_FRAME = "{\"data\":\"QCIAAQAAAAAAAA4=\"}";
  private static final String LORA_EN_ACCESS_CONTROL_KEEP_ALIVE_FRAME = "{\"data\":\"MKAA+AAAAAAAAAI=\"}";
  private static final String LORA_EN_ACCESS_CONTROL_UNSUPPORTED_FRAME = "{\"data\":\"EAAhwAAAQxMBAA==\"}";

  @Test
  void decode_DecodesMessageData_forLoraEnAccessControlDataFrame() throws DecodingException {
    Map<String, Object> packet = KaitaiPacketDecoder.decode(JsonParser.parseString(LORA_EN_ACCESS_CONTROL_DATA_FRAME).getAsJsonObject().get("data"),
        LoraEnAccessControlPacket.class);

    assertEquals(1L, packet.get("FRAMECOUNTER"));
    assertFalse((boolean) packet.get("CONFIG"));
    assertTrue((boolean) packet.get("LOWBAT"));
    assertEquals(1, packet.get("CHANNEL1INFO"));
    assertEquals(0, packet.get("CHANNEL2INFO"));
    assertEquals(0, packet.get("CHANNEL3INFO"));
    assertEquals(0, packet.get("CHANNEL4INFO"));
    assertFalse((boolean) packet.get("CHANNEL1CURRENTSTATE"));
    assertTrue((boolean) packet.get("CHANNEL2CURRENTSTATE"));
    assertFalse((boolean) packet.get("CHANNEL3CURRENTSTATE"));
    assertFalse((boolean) packet.get("CHANNEL4CURRENTSTATE"));
    assertTrue((boolean) packet.get("CHANNEL1PREVIOUSSTATE"));
    assertTrue((boolean) packet.get("CHANNEL2PREVIOUSSTATE"));
    assertFalse((boolean) packet.get("CHANNEL3PREVIOUSSTATE"));
    assertFalse((boolean) packet.get("CHANNEL4PREVIOUSSTATE"));
    assertNull(packet.get("TIMESTAMP"));
  }

  @Test
  void decode_DecodesMessageData_forLoraEnAccessControlKeepAliveFrame() throws DecodingException {
    Map<String, Object> packet =
        KaitaiPacketDecoder.decode(JsonParser.parseString(LORA_EN_ACCESS_CONTROL_KEEP_ALIVE_FRAME).getAsJsonObject().get("data"),
            LoraEnAccessControlPacket.class);
    assertEquals(5L, packet.get("FRAMECOUNTER"));
    assertFalse((boolean) packet.get("CONFIG"));
    assertFalse((boolean) packet.get("LOWBAT"));
    assertEquals(248, packet.get("CHANNEL1INFO"));
    assertEquals(0, packet.get("CHANNEL2INFO"));
    assertEquals(0, packet.get("CHANNEL3INFO"));
    assertEquals(0, packet.get("CHANNEL4INFO"));
    assertFalse((boolean) packet.get("CHANNEL1CURRENTSTATE"));
    assertTrue((boolean) packet.get("CHANNEL2CURRENTSTATE"));
    assertFalse((boolean) packet.get("CHANNEL3CURRENTSTATE"));
    assertFalse((boolean) packet.get("CHANNEL4CURRENTSTATE"));
    assertNull(packet.get("TIMESTAMP"));
  }

  @Test
  void decode_Fails_forLoraEnAccessControlUnsupportedFrame() {
    assertThrows(DecodingException.class, () -> KaitaiPacketDecoder.decode(JsonParser.parseString(LORA_EN_ACCESS_CONTROL_UNSUPPORTED_FRAME).getAsJsonObject().get("data"),
            LoraEnAccessControlPacket.class), "Error while decoding packet: Unsupported frame");
  }

}
