package org.cern.nile.streams.kaitai;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import com.google.gson.JsonObject;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.cern.nile.streams.BaseStreamDecodeTest;
import org.junit.jupiter.api.Test;

public class LoraCrackSensorsDecodeTest extends BaseStreamDecodeTest {

  private static final String LORA_BE_GM_ASG_CRACK_SENSORS_DATA = "{\"applicationID\":\"24\",\"applicationName\":\"lora-BE-gm-asg-crack-sensors\",\"deviceName\":\"gm-asg-crack-sensors-00002\",\"devEUI\":\"70b3d59ba000912c\",\"rxInfo\":[{\"gatewayID\":\"fcc23dfffe20e692\",\"uplinkID\":\"8ee561ba-8773-4af7-847e-b2a08fffaf13\",\"name\":\"lora-0887-gw-1\",\"time\":\"2022-11-18T13:02:29.574529606Z\",\"rssi\":-112,\"loRaSNR\":-13.8,\"location\":{\"latitude\":46.26196,\"longitude\":6.05774,\"altitude\":486}}],\"txInfo\":{\"frequency\":867300000,\"dr\":0},\"adr\":true,\"fCnt\":1,\"fPort\":5,\"data\":\"AAc/3sLha7FAX8b9\"}";

  @Override
  protected LoraCrackSensorsDecode createStreamDecodeInstance() {
    return new LoraCrackSensorsDecode(BaseStreamDecodeTest.SOURCE_TOPIC, BaseStreamDecodeTest.SINK_TOPIC);
  }

  @Test
  void LoraCrackSensorsDecodeTopology_CreatesOutputRecord_forCorrectData() {
    pipeRecord(LORA_BE_GM_ASG_CRACK_SENSORS_DATA);
    final ProducerRecord<String, JsonObject> outputRecord = testDriver.readOutput(SINK_TOPIC, KEY_DESERIALIZER, VALUE_DESERIALIZER);

    assertEquals(18.55, outputRecord.value().get("temperature").getAsDouble());
    assertEquals(3545.62, outputRecord.value().get("batteryVoltage").getAsDouble());
    assertEquals((float) -112.710335, outputRecord.value().get("displacementRaw").getAsFloat());
    assertEquals((float) 3.4965203, outputRecord.value().get("displacementPhysical").getAsFloat());
    assertNotNull(outputRecord.value().get("timestamp"), "Timestamp is null");
  }

}