package org.cern.nile.streams.kaitai;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import io.kaitai.struct.KaitaiStruct;
import java.text.ParseException;
import java.time.Instant;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import lombok.Setter;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.kstream.Consumed;
import org.cern.nile.decoders.KaitaiPacketDecoder;
import org.cern.nile.json.JsonSerde;
import org.cern.nile.streams.AbstractStream;

/**
 * A stream that decodes a KaitaiStruct packet from a Lora message.
 *
 * @param <T> The KaitaiStruct packet to decode.
 */
@Setter
public abstract class LoraDecoderStream<T extends KaitaiStruct> extends AbstractStream {

  private final Class<T> packetClass;

  /**
   * A map containing the custom metadata fields that need to be transformed.
   * The key represents the original field name (metadataValueName) in the JsonObject, and
   * the value represents the new field name (customFieldName) to be used in the resultMap.
   */
  private Map<String, String> customMetadataFields;

  /**
   * A boolean value for whether to add the frame type in the decoded packet. Intended to be used with packets that include multiple different frames.
   */
  private boolean addFrameType;

  /**
   * A boolean value for whether to add the gateway details (rxInfo) in the decoded packet. Includes info about the gateway with the highest SNR.
   */
  private boolean addGatewayDetails;

  /**
   * A boolean value for whether to add the transmission details (txInfo) in the decoded packet.
   */
  private boolean addTxDetails;

  /**
   * Creates a new LoraDecoderStream.
   *
   * @param packetClass The KaitaiStruct packet to decode.
   */
  public LoraDecoderStream(Class<T> packetClass) {
    this.packetClass = packetClass;
    this.customMetadataFields = Collections.emptyMap();
    this.addFrameType = false;
    this.addGatewayDetails = false;
    this.addTxDetails = false;
  }

  /**
   * Creates a new LoraDecoderStream.
   *
   * @param sourceTopic The source topic.
   * @param sinkTopic   The sink topic.
   * @param packetClass The KaitaiStruct packet to decode.
   */
  public LoraDecoderStream(String sourceTopic, String sinkTopic, Class<T> packetClass) {
    this.sourceTopic = sourceTopic;
    this.sinkTopic = sinkTopic;
    this.packetClass = packetClass;
    this.customMetadataFields = Collections.emptyMap();
    this.addFrameType = false;
    this.addGatewayDetails = false;
    this.addTxDetails = false;
  }

  /**
   * Custom enrichments to be applied to the map.
   *
   * @param inputMap     The map to enrich.
   * @param metadataJson The value to enrich the map with.
   * @return the inputMap with no enrichment (in case this method is not overriden)
   */
  public Map<String, Object> enrichCustomFunction(Map<String, Object> inputMap, JsonObject metadataJson) {
    // Override this method to add custom enrichments
    return inputMap;
  }

  @Override
  protected void createTopology(StreamsBuilder builder) {
    builder.stream(sourceTopic, Consumed.with(Serdes.String(), new JsonSerde())).filter(this::filterRecord)
        .transformValues(InjectOffsetTransformer::new).mapValues(this::mapValues).filter(AbstractStream::filterNull).to(sinkTopic);
  }

  private Map<String, Object> mapValues(JsonObject metadataJson) {
    try {
      Map<String, Object> map = KaitaiPacketDecoder.decode(metadataJson.get("data"), packetClass, addFrameType);
      Map<String, Object> enrichedMap = enrichMapWithCommonFields(map, metadataJson);
      enrichedMap = enrichMapWithCustomMetadata(enrichedMap, metadataJson);
      enrichedMap = enrichCustomFunction(enrichedMap, metadataJson);
      if (addGatewayDetails) {
        enrichedMap = enrichMapWithGatewayDetails(enrichedMap, metadataJson.get("rxInfo").getAsJsonArray());
      }
      if (addTxDetails) {
        enrichedMap = enrichMapWithTxDetails(enrichedMap, metadataJson.get("txInfo").getAsJsonObject());
      }
      return enrichedMap;
    } catch (RuntimeException | ParseException e) {
      logStreamsException(e);
      return null;
    } finally {
      lastReadOffset = metadataJson.get("offset").getAsLong();
    }
  }

  private Map<String, Object> enrichMapWithCommonFields(Map<String, Object> inputMap, JsonObject metadataJson) throws ParseException {
    Map<String, Object> enrichedMap = new HashMap<>(inputMap);
    addTimestamp(metadataJson.get("rxInfo").getAsJsonArray(), enrichedMap);
    enrichedMap.put("applicationID", metadataJson.get("applicationID").getAsString());
    enrichedMap.put("applicationName", metadataJson.get("applicationName").getAsString());
    enrichedMap.put("deviceName", metadataJson.get("deviceName").getAsString());
    enrichedMap.put("devEUI", metadataJson.get("devEUI").getAsString());
    return enrichedMap;
  }

  private Map<String, Object> enrichMapWithGatewayDetails(Map<String, Object> inputMap, JsonArray rxInfo) {
    Map<String, Object> enrichedMap = new HashMap<>(inputMap);
    double maxSnr = Double.NEGATIVE_INFINITY;
    JsonObject maxSnrRxInfo = null;
    for (int i = 0; i < rxInfo.size(); i++) {
      JsonObject currentInfo = rxInfo.get(i).getAsJsonObject();
      JsonElement snr = currentInfo.get("loRaSNR");
      if (snr != null && !snr.isJsonNull()) {
        double snrValue = snr.getAsDouble();
        if (snrValue > maxSnr) {
          maxSnr = snrValue;
          maxSnrRxInfo = currentInfo;
        }
      }
    }
    if (maxSnrRxInfo != null) {
      putAsStringIfNotNullOrJsonNull(enrichedMap, maxSnrRxInfo, "gatewayID", "maxSnrRxInfo_gatewayID");
      putAsStringIfNotNullOrJsonNull(enrichedMap, maxSnrRxInfo, "uplinkID", "maxSnrRxInfo_uplinkID");
      putAsStringIfNotNullOrJsonNull(enrichedMap, maxSnrRxInfo, "name", "maxSnrRxInfo_name");

      JsonElement timeElement = maxSnrRxInfo.get("time");
      if (timeElement != null && !timeElement.isJsonNull()) {
        enrichedMap.put("maxSnrRxInfo_time", timeElement.getAsString());
      } else {
        enrichedMap.put("maxSnrRxInfo_time", timestampToString(Long.parseLong(enrichedMap.get("timestamp").toString())));
      }

      putAsStringIfNotNullOrJsonNull(enrichedMap, maxSnrRxInfo, "rssi", "maxSnrRxInfo_rssi");
      putAsStringIfNotNullOrJsonNull(enrichedMap, maxSnrRxInfo, "loRaSNR", "maxSnrRxInfo_loRaSNR");

      JsonObject location = maxSnrRxInfo.getAsJsonObject("location");
      if (location != null) {
        putAsStringIfNotNullOrJsonNull(enrichedMap, location, "latitude", "maxSnrRxInfo_location_latitude");
        putAsStringIfNotNullOrJsonNull(enrichedMap, location, "longitude", "maxSnrRxInfo_location_longitude");
        putAsStringIfNotNullOrJsonNull(enrichedMap, location, "altitude", "maxSnrRxInfo_location_altitude");
      }
    }
    return enrichedMap;
  }

  private String timestampToString(long timestamp) {
    Instant instant = Instant.ofEpochMilli(timestamp);
    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSSSSSSSS'Z'")
        .withZone(ZoneId.of("Z"));
    return formatter.format(instant);
  }

  private Map<String, Object> enrichMapWithTxDetails(Map<String, Object> inputMap, JsonObject txInfo) {
    Map<String, Object> enrichedMap = new HashMap<>(inputMap);
    putIfNotNullOrJsonNull(enrichedMap, txInfo, "frequency", "txInfo_frequency");
    putIfNotNullOrJsonNull(enrichedMap, txInfo, "dr", "txInfo_dr");
    return enrichedMap;
  }

  private void putAsStringIfNotNullOrJsonNull(Map<String, Object> output, JsonObject jsonObject, String key,
                                              String outputKey) {
    JsonElement element = jsonObject.get(key);
    if (element != null && !element.isJsonNull()) {
      output.put(outputKey, element.getAsString());
    } else {
      LOGGER.warn("Element {} is null or JsonNull", key);
    }
  }

  private void putIfNotNullOrJsonNull(Map<String, Object> output, JsonObject jsonObject, String key, String outputKey) {
    JsonElement element = jsonObject.get(key);
    if (element != null && !element.isJsonNull()) {
      output.put(outputKey, element);
    } else {
      LOGGER.warn("Element {} is null or JsonNull", key);
    }
  }

  /**
   * Enriches the inputMap with custom metadata from the metadataJson object based on customMetadataFields.
   * The method iterates through customMetadataFields, using its keys to find original field names in metadataJson
   * and its values as new field names for the resultMap.
   * The resultMap is then updated with the new field names and their corresponding values from metadataJson.
   * If a field has an empty value in metadataJson, the field will be removed from the resultMap.
   *
   * @param inputMap     A map containing the original data to be enriched with custom metadata.
   * @param metadataJson A JsonObject containing the metadata to be added to the inputMap.
   * @return the inputMap enriched with custom metadata.
   */
  private Map<String, Object> enrichMapWithCustomMetadata(Map<String, Object> inputMap, JsonObject metadataJson) {
    Map<String, Object> enrichedMap = new HashMap<>(inputMap);
    for (Map.Entry<String, String> customFieldEntry : customMetadataFields.entrySet()) {
      String metadataValueName = customFieldEntry.getKey();
      String customFieldName = customFieldEntry.getValue();
      if (metadataJson.has(metadataValueName)) {
        JsonElement fieldValue = metadataJson.get(metadataValueName);
        if (fieldValue.isJsonNull() || fieldValue.getAsString().isEmpty()) {
          enrichedMap.remove(customFieldName);
        } else {
          enrichedMap.put(customFieldName, fieldValue);
          if (!metadataValueName.equals(customFieldName)) {
            enrichedMap.remove(metadataValueName);
          }
        }
      } else if (inputMap.containsKey(metadataValueName) && !metadataValueName.equals(customFieldName)) {
        Object originalValue = inputMap.get(metadataValueName);
        enrichedMap.put(customFieldName, originalValue);
        enrichedMap.remove(metadataValueName);
      }
    }
    return enrichedMap;
  }

}
