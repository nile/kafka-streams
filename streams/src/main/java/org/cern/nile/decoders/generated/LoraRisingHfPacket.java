// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

package org.cern.nile.decoders.generated;

import io.kaitai.struct.ByteBufferKaitaiStream;
import io.kaitai.struct.KaitaiStruct;
import io.kaitai.struct.KaitaiStream;
import java.io.IOException;


/**
 * @see <a href="https://risinghf.com/wp-content/uploads/2020/07/RHF76-052_Rev1.3.pdf">Source</a>
 */
public class LoraRisingHfPacket extends KaitaiStruct {
    public static LoraRisingHfPacket fromFile(String fileName) throws IOException {
        return new LoraRisingHfPacket(new ByteBufferKaitaiStream(fileName));
    }

    public LoraRisingHfPacket(KaitaiStream _io) {
        this(_io, null, null);
    }

    public LoraRisingHfPacket(KaitaiStream _io, KaitaiStruct _parent) {
        this(_io, _parent, null);
    }

    public LoraRisingHfPacket(KaitaiStream _io, KaitaiStruct _parent, LoraRisingHfPacket _root) {
        super(_io);
        this._parent = _parent;
        this._root = _root == null ? this : _root;
        _read();
    }
    private void _read() {
        this.header = this._io.readU1();
        this.temperatureRawHidden = this._io.readU2le();
        this.humidityRawHidden = this._io.readU1();
        this.periodRawHidden = this._io.readU2le();
        this.rssiRawHidden = this._io.readU1();
        this.snrRawHidden = this._io.readS1();
        this.batteryRawHidden = this._io.readU1();
    }
    private Double humidity;

    /**
     * Humidity reading in percent.
     */
    public Double humidity() {
        if (this.humidity != null)
            return this.humidity;
        double _tmp = (double) ((((125 * humidityRawHidden()) / 256.0) - 6));
        this.humidity = _tmp;
        return this.humidity;
    }
    private Double rssi;

    /**
     * Received signal strength indication (RSSI) in dBm.
     */
    public Double rssi() {
        if (this.rssi != null)
            return this.rssi;
        double _tmp = (double) ((-180.0 + rssiRawHidden()));
        this.rssi = _tmp;
        return this.rssi;
    }
    private Double battery;

    /**
     * Battery voltage in volts.
     */
    public Double battery() {
        if (this.battery != null)
            return this.battery;
        double _tmp = (double) (((batteryRawHidden() + 150) * 0.01));
        this.battery = _tmp;
        return this.battery;
    }
    private Double temperature;

    /**
     * Temperature reading in degrees Celsius.
     */
    public Double temperature() {
        if (this.temperature != null)
            return this.temperature;
        double _tmp = (double) ((((175.72 * temperatureRawHidden()) / 65536.0) - 46.85));
        this.temperature = _tmp;
        return this.temperature;
    }
    private Double period;

    /**
     * Transmission period in seconds.
     */
    public Double period() {
        if (this.period != null)
            return this.period;
        double _tmp = (double) ((periodRawHidden() * 2.0));
        this.period = _tmp;
        return this.period;
    }
    private Double snr;

    /**
     * Signal-to-noise ratio (SNR) in dB.
     */
    public Double snr() {
        if (this.snr != null)
            return this.snr;
        double _tmp = (double) ((snrRawHidden() / 4.0));
        this.snr = _tmp;
        return this.snr;
    }
    private int header;
    private int temperatureRawHidden;
    private int humidityRawHidden;
    private int periodRawHidden;
    private int rssiRawHidden;
    private byte snrRawHidden;
    private int batteryRawHidden;
    private LoraRisingHfPacket _root;
    private KaitaiStruct _parent;

    /**
     * Frame magic byte.
     */
    public int header() { return header; }

    /**
     * Raw temperature reading from the sensor. The actual value can be calculated as (175.72 * temperature_raw) / 65536.0 - 46.85.
     */
    public int temperatureRawHidden() { return temperatureRawHidden; }

    /**
     * Raw humidity reading from the sensor. The actual value can be calculated as (125 * humidity_raw) / 256.0 - 6.
     */
    public int humidityRawHidden() { return humidityRawHidden; }

    /**
     * Raw transmission period setting from the sensor, in seconds. The actual value can be calculated as period_raw * 2.0.
     */
    public int periodRawHidden() { return periodRawHidden; }

    /**
     * Raw received signal strength indication (RSSI), in dBm. The actual value can be calculated as -180 + rssi_raw.
     */
    public int rssiRawHidden() { return rssiRawHidden; }

    /**
     * Raw signal-to-noise ratio. The actual value can be calculated as snr_raw / 4.0.
     */
    public byte snrRawHidden() { return snrRawHidden; }

    /**
     * Raw battery voltage reading. The actual value can be calculated as (battery_raw + 150) * 0.01.
     */
    public int batteryRawHidden() { return batteryRawHidden; }
    public LoraRisingHfPacket _root() { return _root; }
    public KaitaiStruct _parent() { return _parent; }
}
