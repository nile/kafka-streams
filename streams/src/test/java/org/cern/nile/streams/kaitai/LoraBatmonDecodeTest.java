package org.cern.nile.streams.kaitai;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import com.google.gson.JsonObject;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.cern.nile.streams.AbstractStream;
import org.cern.nile.streams.BaseStreamDecodeTest;
import org.junit.jupiter.api.Test;

public class LoraBatmonDecodeTest extends BaseStreamDecodeTest {

  private static final String TIMBER_DATA =
      "{\"applicationID\":\"19\",\"applicationName\":\"BE-cern-epr-batmon\",\"deviceName\":\"BE-cern-epr-batmon-1\",\"devEUI\":\"A0B0000000000001\",\"rxInfo\":[{\"gatewayID\":\"fcc23dfffe20e692\",\"uplinkID\":\"147fa2d3-edb9-4503-b445-caca09a33941\",\"name\":\"lora-0887-gw-1\",\"time\":\"2022-03-17T10:48:37.513167179Z\",\"rssi\":-109,\"loRaSNR\":0,\"location\":{\"latitude\":46.26196,\"longitude\":6.05774,\"altitude\":486}},{\"gatewayID\":\"fcc23dfffe2095ff\",\"uplinkID\":\"ddbd39a9-9de8-4725-bacf-004d89df03d9\",\"name\":\"lora-0866-gw-2\",\"time\":\"2022-03-17T10:48:37.5067581Z\",\"rssi\":-79,\"loRaSNR\":11.8,\"location\":{\"latitude\":46.25694,\"longitude\":6.05851,\"altitude\":470}},{\"gatewayID\":\"fcc23dfffe0f57a3\",\"uplinkID\":\"b5b8cfc4-c776-4a24-b27f-30e0a9c2bfa0\",\"name\":\"lora-0866-gw\",\"time\":\"2022-03-17T10:48:37.516201203Z\",\"rssi\":-70,\"loRaSNR\":11.9,\"location\":{\"latitude\":46.25694,\"longitude\":6.05851,\"altitude\":470}},{\"gatewayID\":\"fcc23dfffe20e31f\",\"uplinkID\":\"72cd595b-753c-46f5-8aa5-bd64930b6c6d\",\"name\":\"lora-0887-gw-2\",\"time\":\"2022-03-17T10:48:37.51271213Z\",\"rssi\":-107,\"loRaSNR\":-12.8,\"location\":{\"latitude\":46.26196,\"longitude\":6.05774,\"altitude\":486}}],\"txInfo\":{\"frequency\":867100000,\"dr\":2},\"adr\":false,\"fCnt\":687,\"fPort\":5,\"data\":\"6AD//wAAAggjDMUGmQSoFAE0FQEAAAAAPB4BSCcBAAAAABMADQAQAA8AAgAMAAQABQA=\"}";

  private static final String TIMBER_DATA_NO_MAXSNR_TIMESTAMP =
      "{\"applicationID\":\"19\",\"applicationName\":\"BE-cern-epr-batmon\",\"deviceName\":\"BE-cern-epr-batmon-1\",\"devEUI\":\"A0B0000000000001\",\"rxInfo\":[{\"gatewayID\":\"fcc23dfffe20e692\",\"uplinkID\":\"147fa2d3-edb9-4503-b445-caca09a33941\",\"name\":\"lora-0887-gw-1\",\"time\":\"2022-03-17T10:48:37.513167179Z\",\"rssi\":-109,\"loRaSNR\":0,\"location\":{\"latitude\":46.26196,\"longitude\":6.05774,\"altitude\":486}},{\"gatewayID\":\"fcc23dfffe2095ff\",\"uplinkID\":\"ddbd39a9-9de8-4725-bacf-004d89df03d9\",\"name\":\"lora-0866-gw-2\",\"time\":\"2022-03-17T10:48:37.5067581Z\",\"rssi\":-79,\"loRaSNR\":11.8,\"location\":{\"latitude\":46.25694,\"longitude\":6.05851,\"altitude\":470}},{\"gatewayID\":\"fcc23dfffe0f57a3\",\"uplinkID\":\"b5b8cfc4-c776-4a24-b27f-30e0a9c2bfa0\",\"name\":\"lora-0866-gw\",\"rssi\":-70,\"loRaSNR\":11.9,\"location\":{\"latitude\":46.25694,\"longitude\":6.05851,\"altitude\":470}},{\"gatewayID\":\"fcc23dfffe20e31f\",\"uplinkID\":\"72cd595b-753c-46f5-8aa5-bd64930b6c6d\",\"name\":\"lora-0887-gw-2\",\"time\":\"2022-03-17T10:48:37.51271213Z\",\"rssi\":-107,\"loRaSNR\":-12.8,\"location\":{\"latitude\":46.26196,\"longitude\":6.05774,\"altitude\":486}}],\"txInfo\":{\"frequency\":867100000,\"dr\":2},\"adr\":false,\"fCnt\":687,\"fPort\":5,\"data\":\"6AD//wAAAggjDMUGmQSoFAE0FQEAAAAAPB4BSCcBAAAAABMADQAQAA8AAgAMAAQABQA=\"}";

  private static final String TIMBER_DATA_NOT_TIMESTAMP =
      "{\"applicationID\":\"19\",\"applicationName\":\"BE-cern-epr-batmon\",\"deviceName\":\"BE-cern-epr-batmon-1\",\"devEUI\":\"A0B0000000000001\",\"rxInfo\":[{\"gatewayID\":\"fcc23dfffe20e692\",\"uplinkID\":\"147fa2d3-edb9-4503-b445-caca09a33941\",\"name\":\"lora-0887-gw-1\",\"" +
          "rssi\":-109,\"loRaSNR\":0,\"location\":{\"latitude\":46.26196,\"longitude\":6.05774,\"altitude\":486}},{\"gatewayID\":\"fcc23dfffe2095ff\",\"uplinkID\":\"ddbd39a9-9de8-4725-bacf-004d89df03d9\",\"name\":\"lora-0866-gw-2\",\"rssi\":-79,\"loRaSNR\":11.8,\"location\":{\"latitude\":46.25694,\"longitude\":6.05851,\"altitude\":470}},{\"gatewayID\":\"fcc23dfffe0f57a3\",\"uplinkID\":\"b5b8cfc4-c776-4a24-b27f-30e0a9c2bfa0\",\"name\":\"lora-0866-gw\",\"rssi\":-70,\"loRaSNR\":11.9,\"location\":{\"latitude\":46.25694,\"longitude\":6.05851,\"altitude\":470}},{\"gatewayID\":\"fcc23dfffe20e31f\",\"uplinkID\":\"72cd595b-753c-46f5-8aa5-bd64930b6c6d\",\"name\":\"lora-0887-gw-2\",\"rssi\":-107,\"loRaSNR\":-12.8,\"location\":{\"latitude\":46.26196,\"longitude\":6.05774,\"altitude\":486}}],\"txInfo\":{\"frequency\":867100000,\"dr\":2},\"adr\":false,\"fCnt\":687,\"fPort\":5,\"data\":\"6AD//wAAAggjDMUGmQSoFAE0FQEAAAAAPB4BSCcBAAAAABMADQAQAA8AAgAMAAQABQA=\"}";

  @Override
  protected AbstractStream createStreamDecodeInstance() {
    return new LoraBatmonDecode(BaseStreamDecodeTest.SOURCE_TOPIC, BaseStreamDecodeTest.SINK_TOPIC);
  }

  @Test
  void LoraBatmonDecodeTopology_CreatesOutputRecord_forCorrectData() {
    pipeRecord(TIMBER_DATA);
    final ProducerRecord<String, JsonObject> outputRecord = testDriver.readOutput(SINK_TOPIC, KEY_DESERIALIZER, VALUE_DESERIALIZER);

    assertEquals(232, outputRecord.value().get("packet_number").getAsInt());
    assertEquals(255, outputRecord.value().get("swbuild").getAsInt());
    assertEquals(255, outputRecord.value().get("dummy_byte").getAsInt());
    assertEquals((float) 3.30322265625, outputRecord.value().get("mon_3_3").getAsFloat());
    assertEquals((float) 5.006396484374999, outputRecord.value().get("mon_5").getAsFloat());
    assertEquals((float) 6.422335880002299, outputRecord.value().get("v_bat").getAsFloat());
    assertEquals(1177, outputRecord.value().get("extwtd_cnt").getAsInt());
    assertEquals(70824, outputRecord.value().get("fgdos_1_sensor_frequency").getAsInt());
    assertEquals(70964, outputRecord.value().get("fgdos_1_reference_frequency").getAsInt());
    assertEquals(0, outputRecord.value().get("fgdos_1_recharge_count").getAsInt());
    assertEquals(0, outputRecord.value().get("fgdos_1_recharge_frequency").getAsInt());
    assertEquals(73276, outputRecord.value().get("fgdos_2_sensor_frequency").getAsInt());
    assertEquals(75592, outputRecord.value().get("fgdos_2_reference_frequency").getAsInt());
    assertEquals(0, outputRecord.value().get("fgdos_2_recharge_count").getAsInt());
    assertEquals(0, outputRecord.value().get("fgdos_2_recharge_frequency").getAsInt());
    assertEquals(19, outputRecord.value().get("toshiba_m1_seu").getAsInt());
    assertEquals(13, outputRecord.value().get("toshiba_m2_seu").getAsInt());
    assertEquals(16, outputRecord.value().get("toshiba_m3_seu").getAsInt());
    assertEquals(15, outputRecord.value().get("toshiba_m4_seu").getAsInt());
    assertEquals(2, outputRecord.value().get("cypress_m5_seu").getAsInt());
    assertEquals(12, outputRecord.value().get("cypress_m6_seu").getAsInt());
    assertEquals(4, outputRecord.value().get("cypress_m7_seu").getAsInt());
    assertEquals(5, outputRecord.value().get("cypress_m8_seu").getAsInt());

    assertEquals("fcc23dfffe0f57a3", outputRecord.value().get("maxSnrRxInfo_gatewayID").getAsString());
    assertEquals("b5b8cfc4-c776-4a24-b27f-30e0a9c2bfa0", outputRecord.value().get("maxSnrRxInfo_uplinkID").getAsString());
    assertEquals("lora-0866-gw", outputRecord.value().get("maxSnrRxInfo_name").getAsString());
    assertEquals(-70, outputRecord.value().get("maxSnrRxInfo_rssi").getAsInt());
    assertEquals(11.9, outputRecord.value().get("maxSnrRxInfo_loRaSNR").getAsDouble());
    assertEquals(46.25694, outputRecord.value().get("maxSnrRxInfo_location_latitude").getAsDouble());
    assertEquals(6.05851, outputRecord.value().get("maxSnrRxInfo_location_longitude").getAsDouble());
    assertEquals(470.0, outputRecord.value().get("maxSnrRxInfo_location_altitude").getAsDouble());
    assertEquals("2022-03-17T10:48:37.516201203Z", outputRecord.value().get("maxSnrRxInfo_time").getAsString());
    assertEquals(867100000, outputRecord.value().get("txInfo_frequency").getAsLong());
    assertEquals(2, outputRecord.value().get("txInfo_dr").getAsInt());

    assertFalse(outputRecord.value().get("adr").getAsBoolean());
    assertEquals(687, outputRecord.value().get("fCnt").getAsInt());
    assertEquals(5, outputRecord.value().get("fPort").getAsInt());
  }

  @Test
  void LoraBatmonDecodeTopology_CreatesOutputRecord_forCorrectDataButNoMaxSnrTimestamp() {
    pipeRecord(TIMBER_DATA_NO_MAXSNR_TIMESTAMP);
    final ProducerRecord<String, JsonObject> outputRecord = testDriver.readOutput(SINK_TOPIC, KEY_DESERIALIZER, VALUE_DESERIALIZER);

    assertEquals(232, outputRecord.value().get("packet_number").getAsInt());
    assertEquals("2022-03-17T10:48:37.513000000Z", outputRecord.value().get("maxSnrRxInfo_time").getAsString());
    assertNotNull(outputRecord.value().get("timestamp"), "Timestamp is null");
  }

  @Test
  void LoraBatmonDecodeTopology_CreatesOutputRecord_forDataWithNoTimestamp() {
    pipeRecord(TIMBER_DATA_NOT_TIMESTAMP);
    final ProducerRecord<String, JsonObject> outputRecord = testDriver.readOutput(SINK_TOPIC, KEY_DESERIALIZER, VALUE_DESERIALIZER);
    assertNotNull(outputRecord);
  }

}
