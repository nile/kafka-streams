package org.cern.nile.exceptions;

public class UnknownStreamTypeException extends RuntimeException {

  public UnknownStreamTypeException(String message) {
    super(message);
  }

}