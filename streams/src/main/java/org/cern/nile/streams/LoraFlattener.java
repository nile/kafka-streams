package org.cern.nile.streams;

import com.github.wnameless.json.flattener.JsonFlattener;
import com.google.gson.JsonObject;
import java.util.Map;
import java.util.stream.Collectors;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.kstream.Consumed;
import org.cern.nile.json.JsonSerde;

public class LoraFlattener extends AbstractStream {

  @Override
  protected void createTopology(StreamsBuilder builder) {
    builder
        .stream(sourceTopic, Consumed.with(Serdes.String(), new JsonSerde()))
        .mapValues(this::removeDataField)
        .mapValues(this::flatten)
        .to(sinkTopic);
  }

  JsonObject removeDataField(JsonObject value) {
    value.remove("data");
    return value;
  }

  Object flatten(JsonObject value) {
    return JsonFlattener.flattenAsMap(value.toString())
        .entrySet()
        .stream()
        .filter(e -> e.getValue() != null)
        .collect(Collectors
            .toMap(e -> e.getKey().replace('.', '_')
                .replace('[', '_')
                .replace("]", ""), Map.Entry::getValue));
  }

}
