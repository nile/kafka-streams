package org.cern.nile.exceptions;

public class MissingPropertyException extends RuntimeException {

  public MissingPropertyException(String message) {
    super(message);
  }

}