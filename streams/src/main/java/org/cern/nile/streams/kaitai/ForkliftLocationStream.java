package org.cern.nile.streams.kaitai;

import com.google.gson.JsonObject;
import java.util.Map;
import org.cern.nile.decoders.generated.ForkliftLocationPacket;

public class ForkliftLocationStream extends LoraDecoderStream<ForkliftLocationPacket> {

  public ForkliftLocationStream() {
    super(ForkliftLocationPacket.class);
    super.setAddFrameType(true);
  }

  ForkliftLocationStream(String sourceTopic, String sinkTopic) {
    super(sourceTopic, sinkTopic, ForkliftLocationPacket.class);
    setAddFrameType(true);
  }

  @Override
  public Map<String, Object> enrichCustomFunction(Map<String, Object> inputMap, JsonObject metadataJson) {
    if (((double) inputMap.get("latitude") == 0.0 && (double) inputMap.get("longitude") == 0.0) || !((boolean) inputMap.get("gnssFix"))) {
      return null;
    }
    return inputMap;
  }

}