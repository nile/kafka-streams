package org.cern.nile.schema.connect;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;
import java.util.Map;
import org.cern.nile.schema.SchemaTestBase;
import org.junit.jupiter.api.Test;

class SchemaInjectorTest extends SchemaTestBase {

  @Test
  @SuppressWarnings("unchecked")
  void inject_ReturnsCorrectSchemaAndPayload_WhenInputDataIsValid() {
    final Map<String, Object> result = SchemaInjector.inject(data);
    assertNotNull(result);
    assertTrue(result.containsKey("schema"));
    assertTrue(result.containsKey("payload"));
    final Map<String, Object> schema = (Map<String, Object>) result.get("schema");
    assertEquals("struct", schema.get("type"));
    final List<Map<String, Object>> fields = (List<Map<String, Object>>) schema.get("fields");
    assertEquals(data.size(), fields.size());

    for (Map<String, Object> field : fields) {
      final String fieldName = (String) field.get("field");
      assertTrue(data.containsKey(fieldName));
      assertNotNull(field.get("type"));


      if (fieldName.equals("timestamp_col")) {
        assertFalse(Boolean.parseBoolean(field.get("optional").toString()));
      } else {
        assertTrue(Boolean.parseBoolean(field.get("optional").toString()));
      }

      if (fieldName.equals("timestamp_col")) {
        assertEquals("org.apache.kafka.connect.data.Timestamp", field.get("name"));
        assertEquals(1, field.get("version"));
      } else if (fieldName.equals("date_col")) {
        assertEquals("org.apache.kafka.connect.data.Date", field.get("name"));
        assertEquals(1, field.get("version"));
      }
    }

    final Map<String, Object> payload = (Map<String, Object>) result.get("payload");
    assertEquals(data, payload);
  }

  @Test
  void inject_ThrowsIllegalArgumentException_WhenNullValuePresent() {
    data.put("nullValue", null);
    assertThrows(IllegalArgumentException.class, () -> SchemaInjector.inject(data));
  }

  @Test
  void inject_ThrowsIllegalArgumentException_WhenUnsupportedTypePresent() {
    data.put("unsupportedType", new Object());
    assertThrows(IllegalArgumentException.class, () -> SchemaInjector.inject(data));
  }
}
