package org.cern.nile.streams.kaitai;

import static org.junit.jupiter.api.Assertions.assertEquals;

import com.google.gson.JsonObject;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.cern.nile.streams.AbstractStream;
import org.cern.nile.streams.BaseStreamDecodeTest;
import org.junit.jupiter.api.Test;

public class LoraItComputerCenterTempCayenneLppPacketDecodeTest extends BaseStreamDecodeTest {

  private static final String LORA_IT_COMPUTER_CENTER_TEMP_DATA_FRAME =
      "{\"applicationID\":\"31\",\"applicationName\":\"lora-IT-computer-center-temp\",\"deviceName\":\"CC-pressure-1\",\"devEUI\":\"3433333063377d19\",\"rxInfo\":[{\"gatewayID\":\"fcc23dfffe20a8b0\",\"uplinkID\":\"b1bef212-5f2a-4999-8a22-33985919fbd9\",\"name\":\"lora-0031-gw\",\"time\":\"2023-06-29T10:16:45.779622946Z\",\"rssi\":-112,\"loRaSNR\":-9.2,\"location\":{\"latitude\":46.23254,\"longitude\":6.04501,\"altitude\":460}},{\"gatewayID\":\"fcc23dfffe21f4aa\",\"uplinkID\":\"ee432978-7ec1-42fd-9ad2-e8884ec5533d\",\"name\":\"lora-0227-gw-2\",\"time\":\"2023-06-29T10:16:45.778497265Z\",\"rssi\":-111,\"loRaSNR\":-15.2,\"location\":{\"latitude\":46.23431,\"longitude\":6.04088,\"altitude\":488}},{\"gatewayID\":\"fcc23dfffe2095aa\",\"uplinkID\":\"79903486-7095-42e9-9a30-0815ada32d36\",\"name\":\"lora-0361-gw-1\",\"time\":\"2023-06-29T10:16:45.775917708Z\",\"rssi\":-109,\"loRaSNR\":5,\"location\":{\"latitude\":46.23283,\"longitude\":6.0472,\"altitude\":480}},{\"gatewayID\":\"fcc23dfffe0f76fb\",\"uplinkID\":\"516b5a6e-6568-4abc-81ee-793795b4cec1\",\"name\":\"lora-0227-gw\",\"time\":\"2023-06-29T10:16:45.776692206Z\",\"rssi\":-115,\"loRaSNR\":-13.8,\"location\":{\"latitude\":46.23431,\"longitude\":6.04088,\"altitude\":488}},{\"gatewayID\":\"fcc23dfffe20ef66\",\"uplinkID\":\"f163e402-7a55-4ddd-979c-af21ce9edfb2\",\"name\":\"lora-0361-gw-2\",\"time\":\"2023-06-29T10:16:45.774279123Z\",\"rssi\":-110,\"loRaSNR\":-1.2,\"location\":{\"latitude\":46.23283,\"longitude\":6.0472,\"altitude\":480}}],\"txInfo\":{\"frequency\":867900000,\"dr\":1},\"adr\":true,\"fCnt\":12719,\"fPort\":1,\"data\":\"AWcBOAJoOwNnAHIEAgTE\",\"object\":{\"analogInput\":{\"4\":12.2},\"humiditySensor\":{\"2\":29.5},\"temperatureSensor\":{\"1\":31.2,\"3\":11.4}}}";
  private static final String LORA_IT_COMPUTER_CENTER_TEMP_UNSUPPORTED_FRAME =
      "{\"applicationID\":\"31\",\"applicationName\":\"lora-IT-computer-center-temp\",\"deviceName\":\"CC-pressure-1\",\"devEUI\":\"3433333063377d19\",\"rxInfo\":[{\"gatewayID\":\"fcc23dfffe20a8b0\",\"uplinkID\":\"b1bef212-5f2a-4999-8a22-33985919fbd9\",\"name\":\"lora-0031-gw\",\"time\":\"2023-06-29T10:16:45.779622946Z\",\"rssi\":-112,\"loRaSNR\":-9.2,\"location\":{\"latitude\":46.23254,\"longitude\":6.04501,\"altitude\":460}},{\"gatewayID\":\"fcc23dfffe21f4aa\",\"uplinkID\":\"ee432978-7ec1-42fd-9ad2-e8884ec5533d\",\"name\":\"lora-0227-gw-2\",\"time\":\"2023-06-29T10:16:45.778497265Z\",\"rssi\":-111,\"loRaSNR\":-15.2,\"location\":{\"latitude\":46.23431,\"longitude\":6.04088,\"altitude\":488}},{\"gatewayID\":\"fcc23dfffe2095aa\",\"uplinkID\":\"79903486-7095-42e9-9a30-0815ada32d36\",\"name\":\"lora-0361-gw-1\",\"time\":\"2023-06-29T10:16:45.775917708Z\",\"rssi\":-109,\"loRaSNR\":5,\"location\":{\"latitude\":46.23283,\"longitude\":6.0472,\"altitude\":480}},{\"gatewayID\":\"fcc23dfffe0f76fb\",\"uplinkID\":\"516b5a6e-6568-4abc-81ee-793795b4cec1\",\"name\":\"lora-0227-gw\",\"time\":\"2023-06-29T10:16:45.776692206Z\",\"rssi\":-115,\"loRaSNR\":-13.8,\"location\":{\"latitude\":46.23431,\"longitude\":6.04088,\"altitude\":488}},{\"gatewayID\":\"fcc23dfffe20ef66\",\"uplinkID\":\"f163e402-7a55-4ddd-979c-af21ce9edfb2\",\"name\":\"lora-0361-gw-2\",\"time\":\"2023-06-29T10:16:45.774279123Z\",\"rssi\":-110,\"loRaSNR\":-1.2,\"location\":{\"latitude\":46.23283,\"longitude\":6.0472,\"altitude\":480}}],\"txInfo\":{\"frequency\":867900000,\"dr\":1},\"adr\":true,\"fCnt\":12719,\"fPort\":1,\"data\":\"Oh4WCwcX\"}";

  @Override
  protected AbstractStream createStreamDecodeInstance() {
    return new LoraItComputerCenterTempCayenneLppPacketDecode(BaseStreamDecodeTest.SOURCE_TOPIC, BaseStreamDecodeTest.SINK_TOPIC);
  }

  @Test
  void LoraItComputerCenterTempCayenneLppPacketDecodeTopology_CreatesOutputRecord_forCorrectData() {
    pipeRecord(LORA_IT_COMPUTER_CENTER_TEMP_DATA_FRAME);
    final ProducerRecord<String, JsonObject> outputRecord = testDriver.readOutput(SINK_TOPIC, KEY_DESERIALIZER, VALUE_DESERIALIZER);

    assertEquals(31.2, outputRecord.value().get("temperature").getAsDouble());
    assertEquals(29.5, outputRecord.value().get("relativeHumidity").getAsDouble());
    assertEquals(12.2, outputRecord.value().get("differentialPressure").getAsDouble());
    assertEquals(11.4, outputRecord.value().get("dewPoint").getAsDouble());
  }

  @Test
  void LoraItComputerCenterTempCayenneLppPacketDecodeTopology_Ignores_unsupportedFrame() {
    pipeRecord(LORA_IT_COMPUTER_CENTER_TEMP_UNSUPPORTED_FRAME);
    pipeRecord(LORA_IT_COMPUTER_CENTER_TEMP_DATA_FRAME);
    final ProducerRecord<String, JsonObject> outputRecord = testDriver.readOutput(SINK_TOPIC, KEY_DESERIALIZER, VALUE_DESERIALIZER);
    assertEquals(31.2, outputRecord.value().get("temperature").getAsDouble());
  }

}
