package org.cern.nile.schema.db;

import static org.cern.nile.schema.db.OracleTableCreationCommandGenerator.generateOracleCreationCommand;

import java.util.List;
import java.util.Map;
import org.cern.nile.schema.connect.SchemaInjector;

/**
 * Generates a database schema creation command from a Connect schema.
 */
public final class ConnectSchemaToTableCreationCommandGenerator {

  private ConnectSchemaToTableCreationCommandGenerator() {
  }

  /**
   * Generates a database schema creation command from a Connect schema.
   *
   * @param data      Data for which to generate the schema.
   * @param dbType    Database type.
   * @param tableName Table name.
   * @return Database schema creation command.
   */
  public static String getTableCreationCommand(Map<String, Object> data, DbType dbType, String tableName) {
    List<Map<String, Object>> connectSchemaFields = getSchemaFields(data);
    switch (dbType) {
      case ORACLE:
        return generateOracleCreationCommand(connectSchemaFields, tableName);
      default:
        throw new IllegalArgumentException("Unsupported database type: " + dbType);
    }
  }

  @SuppressWarnings("unchecked")
  private static List<Map<String, Object>> getSchemaFields(Map<String, Object> data) {
    return (List<Map<String, Object>>) ((Map<String, Object>) SchemaInjector.inject(data).get("schema")).get("fields");
  }

}
