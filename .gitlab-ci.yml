variables:
  KUBE_NAMESPACE_PROD: streams-prod
  KUBE_NAMESPACE_QA: streams-qa
  PROJ_CERTS: streams-secret/config/certs
  PROJ_CERTS_QA: streams-secret-qa/config/certs

stages:
  - validation
  - package
  - docker
  - secrets
  - deploy

# Templates
.build_template: &bt
  image: maven:3.6.1-jdk-11-slim

.k8s_template: &k8st
  image: gitlab-registry.cern.ch/cloud/ciadm
  stage: deploy
  when: manual
  before_script:
    - curl -o helm3.tar.gz https://get.helm.sh/helm-v3.5.0-linux-amd64.tar.gz; mkdir -p helm3; tar zxvf helm3.tar.gz -C helm3; cp helm3/linux-amd64/helm /usr/local/bin/helm3; rm -rf helm*
    - mkdir "$CI_PROJECT_DIR/.kube"
    - cp "$KUBECONFIG_FILE_CONTENTS" "$CI_PROJECT_DIR/.kube/nile.conf"
    - export KUBECONFIG="$CI_PROJECT_DIR/.kube/nile.conf"
    - printf "%b" "add_entry -password -p nile@CERN.CH -k 1 -e arcfour-hmac-md5\n${NILE_PW}\nwrite_kt ${PROJ_CERTS}/nile.keytab"|ktutil
    - cp /etc/krb5.conf ${PROJ_CERTS}
    - cp /etc/pki/java/cacerts ${PROJ_CERTS}
    - printf "%b" "add_entry -password -p nile@CERN.CH -k 1 -e arcfour-hmac-md5\n${NILE_PW}\nwrite_kt ${PROJ_CERTS_QA}/nile.keytab"|ktutil
    - cp /etc/krb5.conf ${PROJ_CERTS_QA}
    - cp /etc/pki/java/cacerts ${PROJ_CERTS_QA}
  environment:
    name: all

# Jobs
checkstyle:
  <<: *bt
  stage: validation
  script:
    - mvn -f streams/ validate

clean_compile:
  <<: *bt
  stage: validation
  script:
    - mvn -q -f streams/ clean compile

test:
  <<: *bt
  stage: validation
  script:
    - mvn -q -f streams/ test

build_pkg:
  <<: *bt
  stage: package
  script:
    - mvn -q -f streams/ package -DskipTests
  artifacts:
    paths:
      - streams/target/
    expire_in: 1 hour

streams_docker:
  only:
    - qa
    - master
  stage: docker
  image:
    # We recommend using the CERN version of the Kaniko image: gitlab-registry.cern.ch/ci-tools/docker-image-builder
    name: gitlab-registry.cern.ch/ci-tools/docker-image-builder
    entrypoint: [ "" ]
  script:
    # Prepare Kaniko configuration file
    - echo "{\"auths\":{\"$CI_REGISTRY\":{\"username\":\"$CI_REGISTRY_USER\",\"password\":\"$CI_REGISTRY_PASSWORD\"}}}" > /kaniko/.docker/config.json
    # Build and push the image from the Dockerfile at the root of the project.
    # To push to a specific docker tag, amend the --destination parameter, e.g. --destination $CI_REGISTRY_IMAGE:$CI_BUILD_REF_NAME
    # See https://docs.gitlab.com/ee/ci/variables/predefined_variables.html#variables-reference for available variables
    - /kaniko/executor --context $CI_PROJECT_DIR/streams --dockerfile $CI_PROJECT_DIR/streams/Dockerfile --destination $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_NAME


secrets_deploy:
  <<: *k8st
  stage: secrets
  only:
    - qa
    - master
  script:
    # Secrets Deployment
    - |-
      if [[ $CI_COMMIT_REF_NAME == "master" ]]; then
        helm3 upgrade --install --namespace=${KUBE_NAMESPACE_PROD} streams-secret --set influx.pw=${INFLUX_PW} streams-secret/
      else
        helm3 upgrade --install --namespace=${KUBE_NAMESPACE_QA} streams-secret-qa --set influx.pw=${INFLUX_PW} streams-secret-qa/
      fi

lora_routing_deploy:
  <<: *k8st
  only:
    - qa
    - master
  script:
    # Lora Deployment
    - |-
      if [[ $CI_COMMIT_REF_NAME == "master" ]]; then
        helm3 upgrade --install --namespace=${KUBE_NAMESPACE_PROD} lora-routing -f streams-chart/config/lora-routing/values.yaml streams-chart/
      else
        helm3 upgrade --install --namespace=${KUBE_NAMESPACE_QA} lora-routing-qa -f streams-chart-qa/config/lora-routing/values.yaml streams-chart-qa/
      fi

lora_deploy_flattener:
  <<: *k8st
  only:
    - qa
    - master
  script:
    # Lora Flattener Deployment
    - |-
      if [[ $CI_COMMIT_REF_NAME == "master" ]]; then
        helm3 upgrade --install --namespace=${KUBE_NAMESPACE_PROD} lora-flattener -f streams-chart/config/lora-flattener/values.yaml streams-chart/
      else
        helm3 upgrade --install --namespace=${KUBE_NAMESPACE_QA} lora-flattener-qa -f streams-chart-qa/config/lora-flattener/values.yaml streams-chart-qa/
      fi

lora_contact_deploy:
  <<: *k8st
  only:
    - qa
    - master
  script:
    # Lora Contact Deployment
    - |-
      if [[ $CI_COMMIT_REF_NAME == "master" ]]; then
        helm3 upgrade --install --namespace=${KUBE_NAMESPACE_PROD} lora-contact -f streams-chart/config/lora-contact/values.yaml streams-chart/
      else
        helm3 upgrade --install --namespace=${KUBE_NAMESPACE_QA} lora-contact-qa -f streams-chart-qa/config/lora-contact/values.yaml streams-chart-qa/
      fi

rp_wmon_calibration_deploy:
  <<: *k8st
  only:
    - qa
    - master
  script:
    # RP Deployment
    - |-
      if [[ $CI_COMMIT_REF_NAME == "master" ]]; then
        helm3 upgrade --install --namespace=${KUBE_NAMESPACE_PROD} rp-calibration-decoding -f streams-chart/config/rp-calibration/values.yaml streams-chart/
      else
        helm3 upgrade --install --namespace=${KUBE_NAMESPACE_QA} rp-calibration-decoding-qa -f streams-chart-qa/config/rp-calibration/values.yaml streams-chart-qa/
      fi

rp_wmon_production_deploy:
  <<: *k8st
  only:
    - qa
    - master
  script:
    # RP Deployment
    - |-
      if [[ $CI_COMMIT_REF_NAME == "master" ]]; then
        helm3 upgrade --install --namespace=${KUBE_NAMESPACE_PROD} rp-production-decoding -f streams-chart/config/rp-production/values.yaml streams-chart/
      else
        helm3 upgrade --install --namespace=${KUBE_NAMESPACE_QA} rp-production-decoding-qa -f streams-chart-qa/config/rp-production/values.yaml streams-chart-qa/
      fi

remus_deploy:
  <<: *k8st
  only:
    - qa
    - master
  script:
    # Remus Deployment
    - |-
      if [[ $CI_COMMIT_REF_NAME == "master" ]]; then
        helm3 upgrade --install --namespace=${KUBE_NAMESPACE_PROD} remus-enrichment -f streams-chart/config/remus/values.yaml streams-chart/
      else
        helm3 upgrade --install --namespace=${KUBE_NAMESPACE_QA} remus-enrichment-qa -f streams-chart-qa/config/remus/values.yaml streams-chart-qa/
      fi

remus_test_deploy:
  <<: *k8st
  only:
    - qa
    - master
  script:
    # Remus Deployment
    - |-
      if [[ $CI_COMMIT_REF_NAME == "master" ]]; then
        helm3 upgrade --install --namespace=${KUBE_NAMESPACE_PROD} remus-test-enrichment -f streams-chart/config/remus-test/values.yaml streams-chart/
      else
        helm3 upgrade --install --namespace=${KUBE_NAMESPACE_QA} remus-test-enrichment-qa -f streams-chart-qa/config/remus-test/values.yaml streams-chart-qa/
      fi

geolocation_deploy:
  <<: *k8st
  only:
    - qa
    - master
  script:
    # Geolocation Deployment
    - |-
      if [[ $CI_COMMIT_REF_NAME == "master" ]]; then
        helm3 upgrade --install --namespace=${KUBE_NAMESPACE_PROD} geolocation-decoding -f streams-chart/config/geolocation/values.yaml streams-chart/
      else
        helm3 upgrade --install --namespace=${KUBE_NAMESPACE_QA} geolocation-decoding-qa -f streams-chart-qa/config/geolocation/values.yaml streams-chart-qa/
      fi

lora_gateway_uplink_deploy:
  <<: *k8st
  only:
    - qa
    - master
  script:
    # Lora Gateway Uplink Deployment
    - |-
      if [[ $CI_COMMIT_REF_NAME == "master" ]]; then
        helm3 upgrade --install --namespace=${KUBE_NAMESPACE_PROD} lora-gateway-uplink-flattener -f streams-chart/config/lora-gateway-uplink-flattener/values.yaml streams-chart/
      else
        helm3 upgrade --install --namespace=${KUBE_NAMESPACE_QA} lora-gateway-uplink-flattener-qa -f streams-chart-qa/config/lora-gateway-uplink-flattener/values.yaml streams-chart-qa/
      fi

lora_be_it_rising_hf_deploy:
  <<: *k8st
  only:
    - qa
    - master
  script:
    # Lora BE IT Rising HF Deployment
    - |-
      if [[ $CI_COMMIT_REF_NAME == "master" ]]; then
        helm3 upgrade --install --namespace=${KUBE_NAMESPACE_PROD} lora-be-it-temp-decoding -f streams-chart/config/lora-be-it-temp-data/values.yaml streams-chart/
      else
        helm3 upgrade --install --namespace=${KUBE_NAMESPACE_QA} lora-be-it-temp-decoding-qa -f streams-chart-qa/config/lora-be-it-temp-data/values.yaml streams-chart-qa/
      fi

lora_be_cern_epr_batmon_deploy:
  <<: *k8st
  only:
    - qa
    - master
  script:
    # Lora BE CERN EPR Batmon Deployment
    - |-
      if [[ $CI_COMMIT_REF_NAME == "master" ]]; then
        helm3 upgrade --install --namespace=${KUBE_NAMESPACE_PROD} lora-be-cern-epr-batmon-decoding -f streams-chart/config/lora-be-cern-epr-batmon-data/values.yaml streams-chart/
      else
        helm3 upgrade --install --namespace=${KUBE_NAMESPACE_QA} lora-be-cern-epr-batmon-decoding-qa -f streams-chart-qa/config/lora-be-cern-epr-batmon-data/values.yaml streams-chart-qa/
      fi

lora_be_cern_epr_hum_temp_batmon_deploy:
  <<: *k8st
  only:
    - qa
    - master
  script:
    # Lora BE CERN EPR Hum Temp Batmon Deployment
    - |-
      if [[ $CI_COMMIT_REF_NAME == "master" ]]; then
        helm3 upgrade --install --namespace=${KUBE_NAMESPACE_PROD} lora-be-cern-epr-hum-temp-batmon-decoding -f streams-chart/config/lora-be-cern-epr-hum-temp-batmon-data/values.yaml streams-chart/
      else
        helm3 upgrade --install --namespace=${KUBE_NAMESPACE_QA} lora-be-cern-epr-hum-temp-batmon-decoding-qa -f streams-chart-qa/config/lora-be-cern-epr-hum-temp-batmon-data/values.yaml streams-chart-qa/
      fi

lora_be_gm_asg_crack_sensors_deploy:
  <<: *k8st
  only:
    - qa
    - master
  script:
    # Lora BE gm asg crack sensors Deployment
    - |-
      if [[ $CI_COMMIT_REF_NAME == "master" ]]; then
        helm3 upgrade --install --namespace=${KUBE_NAMESPACE_PROD} lora-be-gm-asg-crack-sensors-decoding -f streams-chart/config/lora-be-gm-asg-crack-sensors/values.yaml streams-chart/
      else
        helm3 upgrade --install --namespace=${KUBE_NAMESPACE_QA} lora-be-gm-asg-crack-sensors-decoding-qa -f streams-chart-qa/config/lora-be-gm-asg-crack-sensors/values.yaml streams-chart-qa/
      fi

lora_en_access_control_deploy:
  <<: *k8st
  only:
    - qa
    - master
  script:
    # Lora EN access control sensors Deployment
    - |-
      if [[ $CI_COMMIT_REF_NAME == "master" ]]; then
        helm3 upgrade --install --namespace=${KUBE_NAMESPACE_PROD} lora-en-access-control-decoding -f streams-chart/config/lora-en-access-control/values.yaml streams-chart/
      else
        helm3 upgrade --install --namespace=${KUBE_NAMESPACE_QA} lora-en-access-control-decoding-qa -f streams-chart-qa/config/lora-en-access-control/values.yaml streams-chart-qa/
      fi

lora_en_parking_control_deploy:
  <<: *k8st
  only:
    - qa
    - master
  script:
    # Lora EN parking control sensors Deployment
    - |-
      if [[ $CI_COMMIT_REF_NAME == "master" ]]; then
        helm3 upgrade --install --namespace=${KUBE_NAMESPACE_PROD} lora-en-parking-control-decoding -f streams-chart/config/lora-en-parking-control/values.yaml streams-chart/
      else
        helm3 upgrade --install --namespace=${KUBE_NAMESPACE_QA} lora-en-parking-control-decoding-qa -f streams-chart-qa/config/lora-en-parking-control/values.yaml streams-chart-qa/
      fi

lora_it_computer_center_deploy:
  <<: *k8st
  only:
    - qa
    - master
  script:
    # Lora IT computer center temperatures sensors Deployment
    - |-
      if [[ $CI_COMMIT_REF_NAME == "master" ]]; then
        helm3 upgrade --install --namespace=${KUBE_NAMESPACE_PROD} lora-it-computer-center-decoding -f streams-chart/config/lora-it-computer-center-temp/values.yaml streams-chart/
      else
        helm3 upgrade --install --namespace=${KUBE_NAMESPACE_QA} lora-it-computer-center-decoding-qa -f streams-chart-qa/config/lora-it-computer-center-temp/values.yaml streams-chart-qa/
      fi


lora_forklift_location_deploy:
  <<: *k8st
  only:
    - qa
    - master
  script:
    # Lora IT computer center temperatures sensors Deployment
    - |-
      if [[ $CI_COMMIT_REF_NAME == "master" ]]; then
        helm3 upgrade --install --namespace=${KUBE_NAMESPACE_PROD} lora-en-sam-tg-forklift-location-decoding -f streams-chart/config/lora-forklift-location/values.yaml streams-chart/
      else
        helm3 upgrade --install --namespace=${KUBE_NAMESPACE_QA} lora-en-sam-tg-forklift-location-decoding-qa -f streams-chart-qa/config/lora-forklift-location/values.yaml streams-chart-qa/
      fi    


nile_test:
  <<: *k8st
  only:
    - qa
    - master
  script:
    # Lora Gateway Uplink Deployment
    - |-
      if [[ $CI_COMMIT_REF_NAME == "master" ]]; then
        helm3 upgrade --install --namespace=${KUBE_NAMESPACE_PROD} nile-test -f streams-chart/config/gp-qa-test/values.yaml streams-chart/
      else
        helm3 upgrade --install --namespace=${KUBE_NAMESPACE_QA} nile-test-qa -f streams-chart-qa/config/gp-qa-test/values.yaml streams-chart-qa/
      fi
