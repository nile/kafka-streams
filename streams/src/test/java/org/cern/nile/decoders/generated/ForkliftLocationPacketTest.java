package org.cern.nile.decoders.generated;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import com.google.gson.JsonParser;
import java.util.Map;
import org.cern.nile.decoders.KaitaiPacketDecoder;
import org.cern.nile.exceptions.DecodingException;
import org.junit.jupiter.api.Test;

public class ForkliftLocationPacketTest {

  private static final String START_FRAME_LAT_LON_0 = "{\"data\":\"hAAAAAAAAAAAmDsN\"}"; // 0x84 or 132

  private static final String START_FRAME = "{\"data\":\"hABGil8ACT1IlLoF\"}"; // 0x84 or 132

  private static final String STOP_FRAME = "{\"data\":\"gABGlOUACTxEnLUD\"}"; // 0x8 or 128

  private static final String CYCLIC_FRAME = "{\"data\":\"EABGjGsACTppnr8H\"}"; // 0x10 or 16

  private static final String UNSUPPORTED_FRAME = "{\"data\":\"EAAhwAAAQxMBAA==\"}";


  @Test
  void decode_DecodesMessageData_ForStartFrameWithLatLon0() {
    Map<String, Object> packet =
        KaitaiPacketDecoder.decode(JsonParser.parseString(START_FRAME_LAT_LON_0).getAsJsonObject().get("data"), ForkliftLocationPacket.class);
    assertEquals(3.52, packet.get("batteryVoltage"));
    assertEquals(19, packet.get("temperature"));
    assertEquals(3, packet.get("accuracy"));
    assertEquals(0, packet.get("speed"));
    assertEquals(0.0, packet.get("latitude"));
    assertEquals(0.0, packet.get("longitude"));
    assertFalse((Boolean) packet.get("gnssFix"));
    assertFalse((Boolean) packet.get("inputState2"));
    assertTrue((Boolean) packet.get("inputState1"));
  }


  @Test
  void decode_DecodesMessageData_ForStartFrame() {
    Map<String, Object> packet =
        KaitaiPacketDecoder.decode(JsonParser.parseString(START_FRAME).getAsJsonObject().get("data"), ForkliftLocationPacket.class);
    assertEquals(3.48, packet.get("batteryVoltage"));
    assertEquals(18, packet.get("temperature"));
    assertEquals(1, packet.get("accuracy"));
    assertEquals(0, packet.get("speed"));
    assertEquals(46.22943, packet.get("latitude"));
    assertEquals(6.05512, packet.get("longitude"));
    assertTrue((Boolean) packet.get("gnssFix"));
    assertFalse((Boolean) packet.get("inputState2"));
    assertTrue((Boolean) packet.get("inputState1"));
  }

  @Test
  void decode_DecodesMessageData_ForStopFrame() {
    Map<String, Object> packet =
        KaitaiPacketDecoder.decode(JsonParser.parseString(STOP_FRAME).getAsJsonObject().get("data"), ForkliftLocationPacket.class);
    assertEquals(3.56, packet.get("batteryVoltage"));
    assertEquals(13, packet.get("temperature"));
    assertEquals(0, packet.get("accuracy"));
    assertEquals(0, packet.get("speed"));
    assertEquals(46.25637, packet.get("latitude"));
    assertEquals(6.05252, packet.get("longitude"));
    assertTrue((Boolean) packet.get("gnssFix"));
    assertTrue((Boolean) packet.get("inputState2"));
    assertTrue((Boolean) packet.get("inputState1"));
  }

  @Test
  void decode_DecodesMessageData_ForCyclicFrame() {
    Map<String, Object> packet =
        KaitaiPacketDecoder.decode(JsonParser.parseString(CYCLIC_FRAME).getAsJsonObject().get("data"), ForkliftLocationPacket.class);
    assertEquals(3.58, packet.get("batteryVoltage"));
    assertEquals(23, packet.get("temperature"));
    assertEquals(1, packet.get("accuracy"));
    assertEquals(0, packet.get("speed"));
    assertEquals(46.23467, packet.get("latitude"));
    assertEquals(6.04777, packet.get("longitude"));
    assertTrue((Boolean) packet.get("gnssFix"));
    assertTrue((Boolean) packet.get("inputState2"));
    assertTrue((Boolean) packet.get("inputState1"));
  }

  @Test
  void decode_Fails_forUnsupportedFrame() {
    assertThrows(
        DecodingException.class, () -> KaitaiPacketDecoder.decode(JsonParser.parseString(UNSUPPORTED_FRAME).getAsJsonObject().get("data"),
            ForkliftLocationPacket.class), "Error while decoding packet: Unsupported frame");
  }

}
