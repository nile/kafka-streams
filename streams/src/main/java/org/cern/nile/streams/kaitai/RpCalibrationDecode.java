package org.cern.nile.streams.kaitai;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import java.util.HashMap;
import java.util.Map;
import org.cern.nile.decoders.generated.RpCalibrationPacket;

public class RpCalibrationDecode extends LoraDecoderStream<RpCalibrationPacket> {

  private static final Map<String, String> CUSTOM_METADATA_FIELD_MAP = new HashMap<>();

  static {
    CUSTOM_METADATA_FIELD_MAP.put("deviceName", "device_name");
    CUSTOM_METADATA_FIELD_MAP.put("fPort", "fPort");
  }

  public RpCalibrationDecode() {
    super(RpCalibrationPacket.class);
    super.setCustomMetadataFields(CUSTOM_METADATA_FIELD_MAP);
  }

  RpCalibrationDecode(String sourceTopic, String sinkTopic) {
    super(sourceTopic, sinkTopic, RpCalibrationPacket.class);
    super.setCustomMetadataFields(CUSTOM_METADATA_FIELD_MAP);
  }

  @Override
  public Map<String, Object> enrichCustomFunction(Map<String, Object> map, JsonObject value) {
    setRxInfo(map, (JsonArray) value.get("rxInfo"));
    return map;
  }

  private static void setRxInfo(Map<String, Object> output, JsonArray rxInfo) {
    int index = 0;
    for (JsonElement element : rxInfo) {
      JsonObject entry = element.getAsJsonObject();
      addGatewayProperty(entry, "rssi", "rssi_gw_%d", index, output);
      addGatewayProperty(entry, "loRaSNR", "snr_gw_%d", index, output);
      index++;
    }
  }

  private static void addGatewayProperty(JsonObject entry, String propertyName, String formattedName, int index, Map<String, Object> output) {
    if (entry.has(propertyName)) {
      output.put(String.format(formattedName, index), entry.get(propertyName).getAsInt());
    }
  }


}