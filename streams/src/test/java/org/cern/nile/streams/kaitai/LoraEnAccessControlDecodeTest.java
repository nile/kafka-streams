package org.cern.nile.streams.kaitai;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import com.google.gson.JsonObject;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.cern.nile.streams.BaseStreamDecodeTest;
import org.junit.jupiter.api.Test;

public class LoraEnAccessControlDecodeTest extends BaseStreamDecodeTest {

  private static final String LORA_EN_ACCESS_CONTROL_DATA_FRAME =
      "{\"applicationID\":\"26\",\"applicationName\":\"lora-EN-access-control-testing\",\"deviceName\":\"test-adeunis\",\"devEUI\":\"0018b21000006a53\",\"rxInfo\":[{\"gatewayID\":\"fcc23dfffe20d444\",\"uplinkID\":\"4efe2660-8894-4ffa-a3e4-0b3ed5819de2\",\"name\":\"lora-3182-gw-2\",\"time\":\"2023-03-16T14:52:36.637416804Z\",\"rssi\":-107,\"loRaSNR\":0,\"location\":{\"latitude\":46.23539,\"longitude\":6.05547,\"altitude\":480}},{\"gatewayID\":\"fcc23dfffe20c913\",\"uplinkID\":\"3aea7dc3-68aa-4788-a29b-6a5f163af44b\",\"name\":\"lora-0024-gw-1\",\"time\":\"2023-03-16T14:52:36.637121089Z\",\"rssi\":-109,\"loRaSNR\":-12.8,\"location\":{\"latitude\":46.22932,\"longitude\":6.05161,\"altitude\":480}},{\"gatewayID\":\"fcc23dfffe2064e4\",\"uplinkID\":\"46b7656c-97c8-4816-8e03-88a35f479c40\",\"name\":\"lora-0060-gw-2\",\"time\":\"2023-03-16T14:52:36.636894509Z\",\"rssi\":-106,\"loRaSNR\":-0.5,\"location\":{\"latitude\":46.23144,\"longitude\":6.05452,\"altitude\":480}},{\"gatewayID\":\"fcc23dfffe2095aa\",\"uplinkID\":\"fbd2e72c-9b0a-4406-ba3a-136f62b07730\",\"name\":\"lora-0361-gw-1\",\"time\":\"2023-03-16T14:52:36.64034413Z\",\"rssi\":-106,\"loRaSNR\":3.8,\"location\":{\"latitude\":46.23283,\"longitude\":6.0472,\"altitude\":480}},{\"gatewayID\":\"fcc23dfffe0f78a6\",\"uplinkID\":\"00da7b17-d221-4288-a523-82cd047e2550\",\"name\":\"lora-0060-gw\",\"time\":\"2023-03-16T14:52:36.640756418Z\",\"rssi\":-107,\"loRaSNR\":0,\"location\":{\"latitude\":46.23144,\"longitude\":6.05452,\"altitude\":480}},{\"gatewayID\":\"fcc23dfffe20ef66\",\"uplinkID\":\"4fa291de-7179-443a-9b28-e230faaac05f\",\"name\":\"lora-0361-gw-2\",\"time\":\"2023-03-16T14:52:36.634109748Z\",\"rssi\":-107,\"loRaSNR\":-3,\"location\":{\"latitude\":46.23283,\"longitude\":6.0472,\"altitude\":480}},{\"gatewayID\":\"fcc23dfffe207e29\",\"uplinkID\":\"57fea069-dd11-481b-886f-0e477b4ec36f\",\"name\":\"lora-3182-gw-1\",\"time\":\"2023-03-16T14:52:36.63994713Z\",\"rssi\":-107,\"loRaSNR\":-3.8,\"location\":{\"latitude\":46.23539,\"longitude\":6.05547,\"altitude\":480}},{\"gatewayID\":\"fcc23dfffe0f76fb\",\"uplinkID\":\"d5a3f8a2-aecc-420e-91eb-b16a494e48bc\",\"name\":\"lora-0227-gw\",\"time\":\"2023-03-16T14:52:36.636738592Z\",\"rssi\":-99,\"loRaSNR\":-8.2,\"location\":{\"latitude\":46.23431,\"longitude\":6.04088,\"altitude\":488}},{\"gatewayID\":\"fcc23dfffe20d13b\",\"uplinkID\":\"016d97e3-425a-4193-a5c6-3b572c565346\",\"name\":\"lora-0024-gw-2\",\"time\":\"2023-03-16T14:52:36.638382612Z\",\"rssi\":-112,\"loRaSNR\":-7.2,\"location\":{\"latitude\":46.22932,\"longitude\":6.05161,\"altitude\":480}}],\"txInfo\":{\"frequency\":868500000,\"dr\":1},\"adr\":true,\"fCnt\":11672,\"fPort\":1,\"data\":\"QAAAAgAAAAAAAA8=\",\"object\":{\"channel_1_info\":2,\"channel_1_open\":true,\"channel_1_prev\":true,\"channel_2_info\":0,\"channel_2_open\":true,\"channel_2_prev\":true,\"channel_3_info\":0,\"channel_3_open\":false,\"channel_3_prev\":false,\"channel_4_info\":0,\"channel_4_open\":false,\"channel_4_prev\":false,\"configuration\":false,\"flag_1\":false,\"flag_2\":false,\"low_batery\":false,\"status\":0,\"timestamp\":0}}";
  private static final String LORA_EN_ACCESS_CONTROL_KEEP_ALIVE_FRAME =
      "{\"applicationID\":\"26\",\"applicationName\":\"lora-EN-access-control-testing\",\"deviceName\":\"test-adeunis\",\"devEUI\":\"0018b21000006a53\",\"rxInfo\":[{\"gatewayID\":\"fcc23dfffe2095aa\",\"uplinkID\":\"0bbbd4f1-72ea-446f-b0f7-5842f6a5509c\",\"name\":\"lora-0361-gw-1\",\"time\":\"2023-03-14T15:31:39.012859517Z\",\"rssi\":-105,\"loRaSNR\":-5,\"location\":{\"latitude\":46.23283,\"longitude\":6.0472,\"altitude\":480}},{\"gatewayID\":\"fcc23dfffe20d444\",\"uplinkID\":\"ff288ee9-c8b3-4f18-8b6c-335e87044a15\",\"name\":\"lora-3182-gw-2\",\"time\":\"2023-03-14T15:31:39.021213359Z\",\"rssi\":-102,\"loRaSNR\":4.5,\"location\":{\"latitude\":46.23539,\"longitude\":6.05547,\"altitude\":480}},{\"gatewayID\":\"fcc23dfffe20ef66\",\"uplinkID\":\"ed52fcf7-08a5-495f-ab05-ca7477ce0b53\",\"name\":\"lora-0361-gw-2\",\"time\":\"2023-03-14T15:31:39.018132727Z\",\"rssi\":-104,\"loRaSNR\":-8.2,\"location\":{\"latitude\":46.23283,\"longitude\":6.0472,\"altitude\":480}},{\"gatewayID\":\"fcc23dfffe0f78a6\",\"uplinkID\":\"bf20ba30-1618-48c7-b949-90da1c84cf96\",\"name\":\"lora-0060-gw\",\"time\":\"2023-03-14T15:31:39.016324333Z\",\"rssi\":-99,\"loRaSNR\":4.2,\"location\":{\"latitude\":46.23144,\"longitude\":6.05452,\"altitude\":480}},{\"gatewayID\":\"fcc23dfffe207e29\",\"uplinkID\":\"032ae7df-92cd-40d8-94f1-a235fd42956c\",\"name\":\"lora-3182-gw-1\",\"time\":\"2023-03-14T15:31:39.015794847Z\",\"rssi\":-106,\"loRaSNR\":-2.8,\"location\":{\"latitude\":46.23539,\"longitude\":6.05547,\"altitude\":480}},{\"gatewayID\":\"fcc23dfffe20d13b\",\"uplinkID\":\"e040f5a8-ae28-4259-84b0-62214fade147\",\"name\":\"lora-0024-gw-2\",\"time\":\"2023-03-14T15:31:39.014884912Z\",\"rssi\":-106,\"loRaSNR\":-15.2,\"location\":{\"latitude\":46.22932,\"longitude\":6.05161,\"altitude\":480}},{\"gatewayID\":\"fcc23dfffe2064e4\",\"uplinkID\":\"21a577e5-db6c-46e1-af98-9d6882faf0bf\",\"name\":\"lora-0060-gw-2\",\"time\":\"2023-03-14T15:31:39.007622725Z\",\"rssi\":-99,\"loRaSNR\":2.8,\"location\":{\"latitude\":46.23144,\"longitude\":6.05452,\"altitude\":480}}],\"txInfo\":{\"frequency\":868300000,\"dr\":1},\"adr\":true,\"fCnt\":11101,\"fPort\":1,\"data\":\"MKAA+AAAAAAAAAI=\",\"object\":{\"channel_1_info\":248,\"channel_1_open\":false,\"channel_2_info\":0,\"channel_2_open\":true,\"channel_3_info\":0,\"channel_3_open\":false,\"channel_4_info\":0,\"channel_4_open\":false,\"configuration\":false,\"flag_1\":false,\"flag_2\":false,\"low_batery\":false,\"status\":160,\"timestamp\":0}}";
  private static final String LORA_EN_ACCESS_CONTROL_UNSUPPORTED_FRAME =
      "{\"applicationID\":\"26\",\"applicationName\":\"lora-EN-access-control-testing\",\"deviceName\":\"test-adeunis\",\"devEUI\":\"0018b21000006a53\",\"rxInfo\":[{\"gatewayID\":\"fcc23dfffe2095aa\",\"uplinkID\":\"0bbbd4f1-72ea-446f-b0f7-5842f6a5509c\",\"name\":\"lora-0361-gw-1\",\"time\":\"2023-03-14T15:31:39.012859517Z\",\"rssi\":-105,\"loRaSNR\":-5,\"location\":{\"latitude\":46.23283,\"longitude\":6.0472,\"altitude\":480}},{\"gatewayID\":\"fcc23dfffe20d444\",\"uplinkID\":\"ff288ee9-c8b3-4f18-8b6c-335e87044a15\",\"name\":\"lora-3182-gw-2\",\"time\":\"2023-03-14T15:31:39.021213359Z\",\"rssi\":-102,\"loRaSNR\":4.5,\"location\":{\"latitude\":46.23539,\"longitude\":6.05547,\"altitude\":480}},{\"gatewayID\":\"fcc23dfffe20ef66\",\"uplinkID\":\"ed52fcf7-08a5-495f-ab05-ca7477ce0b53\",\"name\":\"lora-0361-gw-2\",\"time\":\"2023-03-14T15:31:39.018132727Z\",\"rssi\":-104,\"loRaSNR\":-8.2,\"location\":{\"latitude\":46.23283,\"longitude\":6.0472,\"altitude\":480}},{\"gatewayID\":\"fcc23dfffe0f78a6\",\"uplinkID\":\"bf20ba30-1618-48c7-b949-90da1c84cf96\",\"name\":\"lora-0060-gw\",\"time\":\"2023-03-14T15:31:39.016324333Z\",\"rssi\":-99,\"loRaSNR\":4.2,\"location\":{\"latitude\":46.23144,\"longitude\":6.05452,\"altitude\":480}},{\"gatewayID\":\"fcc23dfffe207e29\",\"uplinkID\":\"032ae7df-92cd-40d8-94f1-a235fd42956c\",\"name\":\"lora-3182-gw-1\",\"time\":\"2023-03-14T15:31:39.015794847Z\",\"rssi\":-106,\"loRaSNR\":-2.8,\"location\":{\"latitude\":46.23539,\"longitude\":6.05547,\"altitude\":480}},{\"gatewayID\":\"fcc23dfffe20d13b\",\"uplinkID\":\"e040f5a8-ae28-4259-84b0-62214fade147\",\"name\":\"lora-0024-gw-2\",\"time\":\"2023-03-14T15:31:39.014884912Z\",\"rssi\":-106,\"loRaSNR\":-15.2,\"location\":{\"latitude\":46.22932,\"longitude\":6.05161,\"altitude\":480}},{\"gatewayID\":\"fcc23dfffe2064e4\",\"uplinkID\":\"21a577e5-db6c-46e1-af98-9d6882faf0bf\",\"name\":\"lora-0060-gw-2\",\"time\":\"2023-03-14T15:31:39.007622725Z\",\"rssi\":-99,\"loRaSNR\":2.8,\"location\":{\"latitude\":46.23144,\"longitude\":6.05452,\"altitude\":480}}],\"txInfo\":{\"frequency\":868300000,\"dr\":1},\"adr\":true,\"fCnt\":11101,\"fPort\":1,\"data\":\"EAAhwAAAQxMBAA==\",\"object\":{\"channel_1_info\":248,\"channel_1_open\":false,\"channel_2_info\":0,\"channel_2_open\":true,\"channel_3_info\":0,\"channel_3_open\":false,\"channel_4_info\":0,\"channel_4_open\":false,\"configuration\":false,\"flag_1\":false,\"flag_2\":false,\"low_batery\":false,\"status\":160,\"timestamp\":0}}";

  @Override
  protected LoraEnAccessControlDecode createStreamDecodeInstance() {
    return new LoraEnAccessControlDecode(BaseStreamDecodeTest.SOURCE_TOPIC, BaseStreamDecodeTest.SINK_TOPIC);
  }

  @Test
  void LoraEnAccessControlDecodeTopology_CreatesOutputRecord_forDataFrame() {
    pipeRecord(LORA_EN_ACCESS_CONTROL_DATA_FRAME);
    final ProducerRecord<String, JsonObject> outputRecord = testDriver.readOutput(SINK_TOPIC, KEY_DESERIALIZER, VALUE_DESERIALIZER);

    JsonObject payload = outputRecord.value().getAsJsonObject("payload");

    assertEquals("0018b21000006a53", payload.get("DEVEUI").getAsString());

    assertEquals("0", payload.get("FRAMECOUNTER").getAsString());
    assertFalse(payload.get("CONFIG").getAsBoolean());
    assertFalse(payload.get("LOWBAT").getAsBoolean());
    assertEquals("2", payload.get("CHANNEL1INFO").getAsString());
    assertEquals("0", payload.get("CHANNEL2INFO").getAsString());
    assertEquals("0", payload.get("CHANNEL3INFO").getAsString());
    assertEquals("0", payload.get("CHANNEL4INFO").getAsString());
    assertEquals("QAAAAgAAAAAAAA8=", payload.get("DATA").getAsString());
    assertEquals("DataFrame", payload.get("FRAMETYPE").getAsString());
    assertTrue(payload.get("CHANNEL1CURRENTSTATE").getAsBoolean());
    assertTrue(payload.get("CHANNEL2CURRENTSTATE").getAsBoolean());
    assertFalse(payload.get("CHANNEL3CURRENTSTATE").getAsBoolean());
    assertFalse(payload.get("CHANNEL4CURRENTSTATE").getAsBoolean());
    assertTrue(payload.get("CHANNEL1PREVIOUSSTATE").getAsBoolean());
    assertTrue(payload.get("CHANNEL2PREVIOUSSTATE").getAsBoolean());
    assertFalse(payload.get("CHANNEL3PREVIOUSSTATE").getAsBoolean());
    assertFalse(payload.get("CHANNEL4PREVIOUSSTATE").getAsBoolean());
    assertNotNull(payload.get("TIMESTAMP"), "Timestamp is null");
  }


  @Test
  void LoraEnAccessControlDecodeTopology_CreatesOutputRecord_forKeepAliveFrame() {
    pipeRecord(LORA_EN_ACCESS_CONTROL_KEEP_ALIVE_FRAME);
    final ProducerRecord<String, JsonObject> outputRecord = testDriver.readOutput(SINK_TOPIC, KEY_DESERIALIZER, VALUE_DESERIALIZER);

    JsonObject payload = outputRecord.value().getAsJsonObject("payload");

    assertFalse(payload.get("CONFIG").getAsBoolean());
    assertFalse(payload.get("LOWBAT").getAsBoolean());
    assertEquals(248, payload.get("CHANNEL1INFO").getAsInt());
    assertEquals(0, payload.get("CHANNEL2INFO").getAsInt());
    assertEquals(0, payload.get("CHANNEL3INFO").getAsInt());
    assertEquals(0, payload.get("CHANNEL4INFO").getAsInt());
    assertEquals("MKAA+AAAAAAAAAI=", payload.get("DATA").getAsString());
    assertEquals("KeepAliveFrame", payload.get("FRAMETYPE").getAsString());
    assertFalse(payload.get("CHANNEL1CURRENTSTATE").getAsBoolean());
    assertTrue(payload.get("CHANNEL2CURRENTSTATE").getAsBoolean());
    assertFalse(payload.get("CHANNEL3CURRENTSTATE").getAsBoolean());
    assertFalse(payload.get("CHANNEL4CURRENTSTATE").getAsBoolean());
    assertNotNull(payload.get("TIMESTAMP"), "Timestamp is null");
  }

  @Test
  void LoraEnAccessControlDecodeTopology_ThrowsException_forUnsupportedFrame() {
    pipeRecord(LORA_EN_ACCESS_CONTROL_UNSUPPORTED_FRAME);

    pipeRecord(LORA_EN_ACCESS_CONTROL_KEEP_ALIVE_FRAME);
    final ProducerRecord<String, JsonObject> outputRecord = testDriver.readOutput(SINK_TOPIC, KEY_DESERIALIZER, VALUE_DESERIALIZER);
    JsonObject payload = outputRecord.value().getAsJsonObject("payload");
    assertFalse(payload.get("CONFIG").getAsBoolean());
  }

}
