package org.cern.nile.decoders.generated;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

import com.google.gson.JsonParser;
import java.util.Map;
import org.cern.nile.decoders.KaitaiPacketDecoder;
import org.cern.nile.exceptions.DecodingException;
import org.junit.jupiter.api.Test;

public class LoraEnParkingControlDecoderTest {

  private static final String LORA_EN_PARKING_CONTROL_STATUS_REPORT_FRAME = "{\"data\":\"NzMuMzAwAAAAFisxOS4w\"}";
  private static final String LORA_EN_PARKING_CONTROL_OCCUPATION_STATUS_CHANGE_FRAME = "{\"data\":\"FTMuMzAxAAE3FCsyMi4w\"}";

  @Test
  void decode_DecodesMessageData_forLoraEnParkingControlStatusReportPacket() throws DecodingException {
    Map<String, Object> packet =
        KaitaiPacketDecoder.decode(JsonParser.parseString(LORA_EN_PARKING_CONTROL_STATUS_REPORT_FRAME).getAsJsonObject().get("data"),
            LoraEnParkingControlPacket.class);
    assertEquals("vacant", packet.get("carStatus"));
    assertEquals(3.3, (double) packet.get("batteryVoltage"));
    assertEquals(19.0, packet.get("temperature"));
  }

  @Test
  void decode_DecodesMessageData_forLoraEnParkingControlOccupationStatusChangePacket() throws DecodingException {
    Map<String, Object> packet =
        KaitaiPacketDecoder.decode(JsonParser.parseString(LORA_EN_PARKING_CONTROL_OCCUPATION_STATUS_CHANGE_FRAME).getAsJsonObject().get("data"),
            LoraEnParkingControlPacket.class);
    assertEquals("occupied", packet.get("carStatus"));
    assertEquals(3.3, (double) packet.get("batteryVoltage"));
    assertEquals(22.0, packet.get("temperature"));
    assertNull(packet.get("idTagBeaconRssi"));
    assertNull(packet.get("idTagUid"));
  }

}
