package org.cern.nile.streams.kaitai;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

import com.google.gson.JsonObject;
import java.io.IOException;
import java.text.ParseException;
import java.time.DateTimeException;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.cern.nile.streams.BaseStreamDecodeTest;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

public class RpCalibrationDecodeTest extends BaseStreamDecodeTest {

  private static final String CORRECT_MESSAGE =
      "{\"applicationID\":\"5\",\"applicationName\":\"cern-rp-wmon-test\",\"deviceName\":\"rp-wmon-44\","
          + "\"devEUI\":\"00e839851c156ad8\",\"rxInfo\":[{\"gatewayID\":\"fcc23dfffe0f76fb\",\"uplinkID\":\"28084e96-8c1a-4255-b3b0-7f71ba815819\","
          + "\"name\":\"lora-0227-gw\",\"time\":\"2020-11-18T16:45:38.463126844Z\",\"rssi\":-108,\"loRaSNR\":5.8,\"location\":{\"latitude\":0,"
          + "\"longitude\":0,\"altitude\":0}},{\"gatewayID\":\"fcc23dfffe0f78a6\",\"uplinkID\":\"59ec4ab4-d040-458d-9700-dbfc397bcb32\","
          + "\"name\":\"lora-0060-gw\",\"time\":\"2020-11-18T16:45:38.452045474Z\",\"rssi\":-107,\"loRaSNR\":-2.2,\"location\":{\"latitude\":0,"
          + "\"longitude\":0,\"altitude\":0}}],\"txInfo\":{\"frequency\":867100000,\"dr\":4},\"adr\":true,\"fCnt\":0,\"fPort\":8,"
          + "\"data\":\"3QABAQAAADIAAAAyAAAAAQAAAAIOEA4QDeMAAAAQASIAD/8G7goMAAI=\"}";

  private static final String INCORRECT_DATA_NO_RSSI_NO_SNR =
      "{\"applicationID\":\"5\",\"applicationName\":\"cern-rp-wmon-test\",\"deviceName\":\"rp-wmon-44\","
          + "\"devEUI\":\"00e839851c156ad8\",\"rxInfo\":[{\"gatewayID\":\"fcc23dfffe0f76fb\",\"uplinkID\":\"28084e96-8c1a-4255-b3b0-7f71ba815819\","
          + "\"name\":\"lora-0227-gw\",\"time\":\"2020-11-18T16:45:38.463126844Z\",\"location\":{\"latitude\":0,"
          + "\"longitude\":0,\"altitude\":0}},{\"gatewayID\":\"fcc23dfffe0f78a6\",\"uplinkID\":\"59ec4ab4-d040-458d-9700-dbfc397bcb32\","
          + "\"name\":\"lora-0060-gw\",\"time\":\"2020-11-18T16:45:38.452045474Z\",\"location\":{\"latitude\":0,"
          + "\"longitude\":0,\"altitude\":0}}],\"txInfo\":{\"frequency\":867100000,\"dr\":4},\"adr\":true,\"fCnt\":0,\"fPort\":8,"
          + "\"data\":\"3QABAQAAADIAAAAyAAAAAQAAAAIOEA4QDeMAAAAQASIAD/8G7goMAAI=\"}";

  private static final String DATA_MESSAGE_PARSE_ERROR =
      "{\"applicationID\":\"5\",\"applicationName\":\"cern-rp-wmon-test\",\"deviceName\":\"rp-wmon-44\","
          + "\"devEUI\":\"00e839851c156ad8\",\"rxInfo\":[{\"gatewayID\":\"fcc23dfffe0f76fb\",\"uplinkID\":\"28084e96-8c1a-4255-b3b0-7f71ba815819\","
          + "\"name\":\"lora-0227-gw\",\"time\":\"2020-11-18T16@###:45:38.463126844Z\",\"rssi\":-108,\"loRaSNR\":5.8,\"location\":{\"latitude\":0,"
          + "\"longitude\":0,\"altitude\":0}},{\"gatewayID\":\"fcc23dfffe0f78a6\",\"uplinkID\":\"59ec4ab4-d040-458d-9700-dbfc397bcb32\","
          + "\"name\":\"lora-0060-gw\",\"time\":\"2020-11-18T16:45:38.452045474Z\",\"rssi\":-107,\"loRaSNR\":-2.2,\"location\":{\"latitude\":0,"
          + "\"longitude\":0,\"altitude\":0}}],\"txInfo\":{\"frequency\":867100000,\"dr\":4},\"adr\":true,\"fCnt\":0,\"fPort\":8,"
          + "\"data\":\"3QABAQAAADIAAAAyAAAAAQAAAAIOEA4QDeMAAAAQASIAD/8G7goMAAI=\"}";

  private static final String DATA_NULL_POINTER_TIME =
      "{\"applicationID\":\"5\",\"applicationName\":\"cern-rp-wmon-test\",\"deviceName\":\"rp-wmon-44\","
          + "\"devEUI\":\"00e839851c156ad8\",\"rxInfo\":[{\"gatewayID\":\"fcc23dfffe0f76fb\",\"uplinkID\":\"28084e96-8c1a-4255-b3b0-7f71ba815819\","
          + "\"name\":\"lora-0227-gw\",\"rssi\":-108,\"loRaSNR\":5.8,\"location\":{\"latitude\":0,"
          + "\"longitude\":0,\"altitude\":0}},{\"gatewayID\":\"fcc23dfffe0f78a6\",\"uplinkID\":\"59ec4ab4-d040-458d-9700-dbfc397bcb32\","
          + "\"name\":\"lora-0060-gw\",\"rssi\":-107,\"loRaSNR\":-2.2,\"location\":{\"latitude\":0,"
          + "\"longitude\":0,\"altitude\":0}}],\"txInfo\":{\"frequency\":867100000,\"dr\":4},\"adr\":true,\"fCnt\":0,\"fPort\":8,"
          + "\"data\":\"3QABAQAAADIAAAAyAAAAAQAAAAIOEA4QDeMAAAAQASIAD/8G7goMAAI=\"}";

  @Override
  protected RpCalibrationDecode createStreamDecodeInstance() {
    return new RpCalibrationDecode(BaseStreamDecodeTest.SOURCE_TOPIC, BaseStreamDecodeTest.SINK_TOPIC);
  }

  @Test
  void decode_DecodesMessage_forCorrectMessage() throws IOException, ParseException {
    pipeRecord(CORRECT_MESSAGE);
    final ProducerRecord<String, JsonObject> outputRecord = testDriver.readOutput(SINK_TOPIC, KEY_DESERIALIZER, VALUE_DESERIALIZER);

    assertEquals(2572, outputRecord.value().get("monVin").getAsInt());
    assertEquals(4.765986328125, outputRecord.value().get("monVin_voltage").getAsDouble());
    assertEquals(3.2991943359375, outputRecord.value().get("mon3v3_voltage").getAsDouble());
    assertEquals(2, outputRecord.value().get("exwdtc").getAsInt());
    assertEquals(8, outputRecord.value().get("fPort").getAsInt());
    assertEquals(50, outputRecord.value().get("counts").getAsInt());
    assertEquals(4095, outputRecord.value().get("mon3v3").getAsInt());
    assertEquals(221, outputRecord.value().get("dev_ID").getAsInt());
    assertEquals(1.429248046875, outputRecord.value().get("mon5_voltage").getAsDouble());
    assertEquals("rp-wmon-44", outputRecord.value().get("device_name").getAsString());
    assertEquals(16, outputRecord.value().get("shock_counts").getAsInt());
    assertEquals(1, outputRecord.value().get("alarm").getAsInt());
    assertEquals(290, outputRecord.value().get("temperature").getAsInt());
    assertEquals(50, outputRecord.value().get("alarm_counts").getAsInt());
    assertEquals(1774, outputRecord.value().get("mon5").getAsInt());
    assertEquals(3600, outputRecord.value().get("checking_time").getAsInt());
    assertEquals(1, outputRecord.value().get("package_num").getAsInt());
    assertEquals(-108, outputRecord.value().get("rssi_gw_0").getAsInt());
    assertEquals(-107, outputRecord.value().get("rssi_gw_1").getAsInt());
    assertEquals(5, outputRecord.value().get("snr_gw_0").getAsInt());
    assertEquals(-2, outputRecord.value().get("snr_gw_1").getAsInt());
    assertNotNull(outputRecord.value().get("timestamp"), "Timestamp is null");
  }


  @Test
  void decode_DecodesMessage_forCorrectMessageWithNoRssi() {
    pipeRecord(INCORRECT_DATA_NO_RSSI_NO_SNR);
    final ProducerRecord<String, JsonObject> outputRecord = testDriver.readOutput(SINK_TOPIC, KEY_DESERIALIZER, VALUE_DESERIALIZER);

    assertNull(outputRecord.value().get("rssi_gw_0"));
    assertNull(outputRecord.value().get("rssi_gw_1"));
    assertNull(outputRecord.value().get("snr_gw_0"));
    assertNull(outputRecord.value().get("snr_gw_1"));
  }

  @Test
  @Disabled
  void decode_ThrowsParseException_forWrongMessageFormat() {
    assertThrows(ParseException.class, () -> pipeRecord(DATA_MESSAGE_PARSE_ERROR));
  }

  @Test
  @Disabled
  void decode_ThrowsNullPtrException_forMessageWithNoTime() {
    assertThrows(DateTimeException.class, () -> pipeRecord(DATA_NULL_POINTER_TIME));
  }

}
