package org.cern.nile.exceptions;

public class DecodingException extends RuntimeException {

  public DecodingException(String message, Throwable err) {
    super(message, err);
  }

  public DecodingException(String message) {
    super(message);
  }

}
