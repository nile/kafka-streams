// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

package org.cern.nile.decoders.generated;

import io.kaitai.struct.ByteBufferKaitaiStream;
import io.kaitai.struct.KaitaiStruct;
import io.kaitai.struct.KaitaiStream;
import java.io.IOException;
import java.nio.charset.Charset;


/**
 * @see <a href="https://cernbox.cern.ch/s/XCEjcjpMMtGcvGR">Source</a>
 */
public class LoraEnParkingControlPacket extends KaitaiStruct {
    public static LoraEnParkingControlPacket fromFile(String fileName) throws IOException {
        return new LoraEnParkingControlPacket(new ByteBufferKaitaiStream(fileName));
    }

    public LoraEnParkingControlPacket(KaitaiStream _io) {
        this(_io, null, null);
    }

    public LoraEnParkingControlPacket(KaitaiStream _io, KaitaiStruct _parent) {
        this(_io, _parent, null);
    }

    public LoraEnParkingControlPacket(KaitaiStream _io, KaitaiStruct _parent, LoraEnParkingControlPacket _root) {
        super(_io);
        this._parent = _parent;
        this._root = _root == null ? this : _root;
        _read();
    }
    private void _read() {
        this.frameCode = this._io.readU1();
        switch (frameCode()) {
        case 55: {
            this.payload = new StatusReport(this._io, this, _root);
            break;
        }
        case 21: {
            this.payload = new OccupationStatusChange(this._io, this, _root);
            break;
        }
        }
    }
    public static class CarStatus extends KaitaiStruct {
        public static CarStatus fromFile(String fileName) throws IOException {
            return new CarStatus(new ByteBufferKaitaiStream(fileName));
        }

        public CarStatus(KaitaiStream _io) {
            this(_io, null, null);
        }

        public CarStatus(KaitaiStream _io, KaitaiStruct _parent) {
            this(_io, _parent, null);
        }

        public CarStatus(KaitaiStream _io, KaitaiStruct _parent, LoraEnParkingControlPacket _root) {
            super(_io);
            this._parent = _parent;
            this._root = _root;
            _read();
        }
        private void _read() {
            this.carStatusCharHidden = new String(this._io.readBytes(1), Charset.forName("ASCII"));
        }
        private String carStatus;
        public String carStatus() {
            if (this.carStatus != null)
                return this.carStatus;
            this.carStatus = (carStatusCharHidden().equals("0") ? "vacant" : "occupied");
            return this.carStatus;
        }
        private String carStatusCharHidden;
        private LoraEnParkingControlPacket _root;
        private KaitaiStruct _parent;
        public String carStatusCharHidden() { return carStatusCharHidden; }
        public LoraEnParkingControlPacket _root() { return _root; }
        public KaitaiStruct _parent() { return _parent; }
    }
    public static class Temperature extends KaitaiStruct {
        public static Temperature fromFile(String fileName) throws IOException {
            return new Temperature(new ByteBufferKaitaiStream(fileName));
        }

        public Temperature(KaitaiStream _io) {
            this(_io, null, null);
        }

        public Temperature(KaitaiStream _io, KaitaiStruct _parent) {
            this(_io, _parent, null);
        }

        public Temperature(KaitaiStream _io, KaitaiStruct _parent, LoraEnParkingControlPacket _root) {
            super(_io);
            this._parent = _parent;
            this._root = _root;
            _read();
        }
        private void _read() {
            this.temperatureSignHidden = new String(this._io.readBytes(1), Charset.forName("ASCII"));
            this.temperatureValueHidden = new String(this._io.readBytes(4), Charset.forName("ASCII"));
        }
        private String temperature;
        public String temperature() {
            if (this.temperature != null)
                return this.temperature;
            this.temperature = temperatureSignHidden() + temperatureValueHidden();
            return this.temperature;
        }
        private String temperatureSignHidden;
        private String temperatureValueHidden;
        private LoraEnParkingControlPacket _root;
        private KaitaiStruct _parent;
        public String temperatureSignHidden() { return temperatureSignHidden; }
        public String temperatureValueHidden() { return temperatureValueHidden; }
        public LoraEnParkingControlPacket _root() { return _root; }
        public KaitaiStruct _parent() { return _parent; }
    }
    public static class StatusReport extends KaitaiStruct {
        public static StatusReport fromFile(String fileName) throws IOException {
            return new StatusReport(new ByteBufferKaitaiStream(fileName));
        }

        public StatusReport(KaitaiStream _io) {
            this(_io, null, null);
        }

        public StatusReport(KaitaiStream _io, LoraEnParkingControlPacket _parent) {
            this(_io, _parent, null);
        }

        public StatusReport(KaitaiStream _io, LoraEnParkingControlPacket _parent, LoraEnParkingControlPacket _root) {
            super(_io);
            this._parent = _parent;
            this._root = _root;
            _read();
        }
        private void _read() {
            this.batteryVoltage = new String(this._io.readBytes(4), Charset.forName("ASCII"));
            this.carStatusData = new CarStatus(this._io, this, _root);
            this.factoryUsedHidden = this._io.readBytes(4);
            this.temperatureData = new Temperature(this._io, this, _root);
        }
        private String batteryVoltage;
        private CarStatus carStatusData;
        private byte[] factoryUsedHidden;
        private Temperature temperatureData;
        private LoraEnParkingControlPacket _root;
        private LoraEnParkingControlPacket _parent;

        /**
         * Battery voltage as a string (e.g., "3.55")
         */
        public String batteryVoltage() { return batteryVoltage; }
        public CarStatus carStatusData() { return carStatusData; }

        /**
         * Factory used bytes
         */
        public byte[] factoryUsedHidden() { return factoryUsedHidden; }
        public Temperature temperatureData() { return temperatureData; }
        public LoraEnParkingControlPacket _root() { return _root; }
        public LoraEnParkingControlPacket _parent() { return _parent; }
    }
    public static class OccupationStatusChange extends KaitaiStruct {
        public static OccupationStatusChange fromFile(String fileName) throws IOException {
            return new OccupationStatusChange(new ByteBufferKaitaiStream(fileName));
        }

        public OccupationStatusChange(KaitaiStream _io) {
            this(_io, null, null);
        }

        public OccupationStatusChange(KaitaiStream _io, LoraEnParkingControlPacket _parent) {
            this(_io, _parent, null);
        }

        public OccupationStatusChange(KaitaiStream _io, LoraEnParkingControlPacket _parent, LoraEnParkingControlPacket _root) {
            super(_io);
            this._parent = _parent;
            this._root = _root;
            _read();
        }
        private void _read() {
            this.batteryVoltage = new String(this._io.readBytes(4), Charset.forName("ASCII"));
            this.carStatusData = new CarStatus(this._io, this, _root);
            this.factoryUsedHidden = this._io.readBytes(4);
            this.temperatureData = new Temperature(this._io, this, _root);
            if ((_io().size() - _io().pos()) >= 12) {
                this.idTagUid = new String(this._io.readBytes(12), Charset.forName("ASCII"));
            }
            if ((_io().size() - _io().pos()) >= 2) {
                this.idTagBeaconRssi = new String(this._io.readBytes(2), Charset.forName("ASCII"));
            }
        }
        private String batteryVoltage;
        private CarStatus carStatusData;
        private byte[] factoryUsedHidden;
        private Temperature temperatureData;
        private String idTagUid;
        private String idTagBeaconRssi;
        private LoraEnParkingControlPacket _root;
        private LoraEnParkingControlPacket _parent;

        /**
         * Battery voltage as a string (e.g., "3.55")
         */
        public String batteryVoltage() { return batteryVoltage; }
        public CarStatus carStatusData() { return carStatusData; }

        /**
         * Factory used bytes
         */
        public byte[] factoryUsedHidden() { return factoryUsedHidden; }
        public Temperature temperatureData() { return temperatureData; }

        /**
         * ID Tag UID as a string (e.g., "AC233F6E224DD25")
         */
        public String idTagUid() { return idTagUid; }

        /**
         * ID Tag Beacon RSSI as a string (e.g., "54")
         */
        public String idTagBeaconRssi() { return idTagBeaconRssi; }
        public LoraEnParkingControlPacket _root() { return _root; }
        public LoraEnParkingControlPacket _parent() { return _parent; }
    }
    private int frameCode;
    private KaitaiStruct payload;
    private LoraEnParkingControlPacket _root;
    private KaitaiStruct _parent;

    /**
     * Frame code
     */
    public int frameCode() { return frameCode; }
    public KaitaiStruct payload() { return payload; }
    public LoraEnParkingControlPacket _root() { return _root; }
    public KaitaiStruct _parent() { return _parent; }
}
