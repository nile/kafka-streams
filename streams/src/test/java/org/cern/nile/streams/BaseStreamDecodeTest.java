package org.cern.nile.streams;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Properties;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.common.serialization.Deserializer;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.common.serialization.Serializer;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.Topology;
import org.apache.kafka.streams.TopologyTestDriver;
import org.apache.kafka.streams.test.ConsumerRecordFactory;
import org.cern.nile.configs.StreamConfig;
import org.cern.nile.json.JsonSerde;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class BaseStreamDecodeTest {
  protected static final Logger LOGGER = LoggerFactory.getLogger(BaseStreamDecodeTest.class.getName());
  protected static final String ROUTING_PROPERTIES = "src/test/resources/routing.properties";

  protected static final Serializer<String> KEY_SERIALIZER = Serdes.String().serializer();
  protected static final Serializer<JsonObject> VALUE_SERIALIZER = new JsonSerde().serializer();
  protected static final Deserializer<String> KEY_DESERIALIZER = Serdes.String().deserializer();
  protected static final Deserializer<JsonObject> VALUE_DESERIALIZER = new JsonSerde().deserializer();

  protected static final String SOURCE_TOPIC = StreamConfig.ClientProperties.SOURCE_TOPIC.getValue();
  protected static final String SINK_TOPIC = StreamConfig.DecodingProperties.SINK_TOPIC.getValue();

  protected TopologyTestDriver testDriver;
  protected ConsumerRecordFactory<String, JsonObject> recordFactory;

  protected abstract AbstractStream createStreamDecodeInstance();

  @BeforeEach
  void setUp() throws IOException {
    final StreamsBuilder builder = new StreamsBuilder();

    final Properties routingProperties = new Properties();
    routingProperties.load(new FileInputStream(ROUTING_PROPERTIES));

    final AbstractStream streamDecode = createStreamDecodeInstance();
    streamDecode.configure(routingProperties);
    streamDecode.createTopology(builder);

    Topology topology = builder.build();

    LOGGER.debug("Created topology: {}\n", topology.describe());

    Properties testDriverProperties = new Properties();
    testDriverProperties.setProperty(StreamsConfig.APPLICATION_ID_CONFIG, "test-app");
    testDriverProperties.setProperty(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9091");
    testDriverProperties.setProperty(StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG, Serdes.String().getClass().getName());
    testDriverProperties.setProperty(StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG, JsonSerde.class.getName());
    testDriverProperties.setProperty(StreamsConfig.STATE_DIR_CONFIG, "target/kafka-streams");
    testDriver = new TopologyTestDriver(topology, testDriverProperties);

    recordFactory = new ConsumerRecordFactory<>(SOURCE_TOPIC, KEY_SERIALIZER, VALUE_SERIALIZER);
  }

  @AfterEach
  void tearDown() throws IOException {
    try {
      testDriver.close();
    } catch (Exception e) {
      Files.deleteIfExists(Path.of("target/kafka-streams"));
      LOGGER.error("Error closing test driver", e);
    }
  }

  protected void pipeRecord(String record) {
    final JsonObject jsonObject = JsonParser.parseString(record).getAsJsonObject();
    final ConsumerRecord<byte[], byte[]> inputRecord = recordFactory.create(jsonObject);
    testDriver.pipeInput(inputRecord);
  }
}
