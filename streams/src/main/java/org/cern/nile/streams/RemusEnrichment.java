package org.cern.nile.streams;

import com.google.gson.JsonObject;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.kstream.Consumed;
import org.cern.nile.json.JsonSerde;

public class RemusEnrichment extends AbstractStream {

  @Override
  protected void createTopology(StreamsBuilder builder) {
    builder
        .stream(sourceTopic, Consumed.with(Serdes.String(), new JsonSerde()))
        .filter(this::filterRecord)
        .transformValues(InjectOffsetTransformer::new)
        .filter(((key, value) -> validateTimestamp(value)))
        .to(sinkTopic);
  }

  @Override
  protected boolean filterRecord(String key, JsonObject value) {
    return value != null && value.get("timestamp") != null && value.get("value") != null;
  }

  private boolean validateTimestamp(JsonObject value) {
    try {
      final long unix_seconds = Long.parseLong(value.get("timestamp").getAsString());
      final Date date = new Date(unix_seconds);
      final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss z");
      sdf.format(date);
    } catch (NumberFormatException e) {
      logStreamsException(e);
      return false;
    }
    lastReadOffset = value.get("offset").getAsLong();
    return true;
  }
}
