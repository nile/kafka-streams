package org.cern.nile.streams.kaitai;

import static org.junit.jupiter.api.Assertions.assertEquals;

import com.google.gson.JsonObject;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.cern.nile.streams.AbstractStream;
import org.cern.nile.streams.BaseStreamDecodeTest;
import org.junit.jupiter.api.Test;

public class LoraHumTempBatmonDecodeTest extends BaseStreamDecodeTest {

  private static final String DATA =
      "{\"applicationID\":\"25\",\"applicationName\":\"BE-cern-epr-Hum-Temp-batmon\",\"deviceName\":\"BE-cern-epr-Hum-Temp-batmon-0\",\"devEUI\":\"a0b0100000000000\",\"rxInfo\":[{\"gatewayID\":\"fcc23dfffe0f57a3\",\"uplinkID\":\"fb0477e9-43a9-4907-b943-5dcd4c18f933\",\"name\":\"lora-0866-gw\",\"time\":\"2023-04-21T15:20:50.505481844Z\",\"rssi\":-111,\"loRaSNR\":-4.8,\"location\":{\"latitude\":46.25694,\"longitude\":6.05851,\"altitude\":470}},{\"gatewayID\":\"fcc23dfffe20763c\",\"uplinkID\":\"7f02bf82-487b-4bb6-81cc-c93efab0bede\",\"name\":\"lora-763c-gw\",\"time\":\"2023-04-21T15:20:50.504458993Z\",\"rssi\":-33,\"loRaSNR\":9.5,\"location\":{\"latitude\":46.259707928145865,\"longitude\":6.062259078025818,\"altitude\":460}},{\"gatewayID\":\"fcc23dfffe20e31f\",\"uplinkID\":\"eefb0821-f6b6-48cf-a7b0-deaa9bce84e6\",\"name\":\"lora-0887-gw-2\",\"time\":\"2023-04-21T15:20:50.506611292Z\",\"rssi\":-101,\"loRaSNR\":-9.5,\"location\":{\"latitude\":46.26196,\"longitude\":6.05774,\"altitude\":486}},{\"gatewayID\":\"fcc23dfffe20e692\",\"uplinkID\":\"aa5bb61e-cc7b-4bf1-86cd-6adef2bc47bd\",\"name\":\"lora-0887-gw-1\",\"time\":\"2023-04-21T15:20:50.496608221Z\",\"rssi\":-109,\"loRaSNR\":-0.5,\"location\":{\"latitude\":46.26196,\"longitude\":6.05774,\"altitude\":486}}],\"txInfo\":{\"frequency\":867700000,\"dr\":2},\"adr\":false,\"fCnt\":12,\"fPort\":5,\"data\":\"DAD//wAA/wcXDEwGPAAThRUAgADxA4YHAEAAQjgKACCkBAAggy4AAAAAAAAAAAAAAAA=\"}";
  private static final String DATA_WITH_NAN_BRIDGE_VOLTAGE =
      "{\"applicationID\":\"25\",\"applicationName\":\"BE-cern-epr-Hum-Temp-batmon\",\"deviceName\":\"BE-cern-epr-Hum-Temp-batmon-0\",\"devEUI\":\"a0b0100000000000\",\"rxInfo\":[{\"gatewayID\":\"fcc23dfffe0f57a3\",\"uplinkID\":\"fb0477e9-43a9-4907-b943-5dcd4c18f933\",\"name\":\"lora-0866-gw\",\"time\":\"2023-04-21T15:20:50.505481844Z\",\"rssi\":-111,\"loRaSNR\":-4.8,\"location\":{\"latitude\":46.25694,\"longitude\":6.05851,\"altitude\":470}},{\"gatewayID\":\"fcc23dfffe20763c\",\"uplinkID\":\"7f02bf82-487b-4bb6-81cc-c93efab0bede\",\"name\":\"lora-763c-gw\",\"time\":\"2023-04-21T15:20:50.504458993Z\",\"rssi\":-33,\"loRaSNR\":9.5,\"location\":{\"latitude\":46.259707928145865,\"longitude\":6.062259078025818,\"altitude\":460}},{\"gatewayID\":\"fcc23dfffe20e31f\",\"uplinkID\":\"eefb0821-f6b6-48cf-a7b0-deaa9bce84e6\",\"name\":\"lora-0887-gw-2\",\"time\":\"2023-04-21T15:20:50.506611292Z\",\"rssi\":-101,\"loRaSNR\":-9.5,\"location\":{\"latitude\":46.26196,\"longitude\":6.05774,\"altitude\":486}},{\"gatewayID\":\"fcc23dfffe20e692\",\"uplinkID\":\"aa5bb61e-cc7b-4bf1-86cd-6adef2bc47bd\",\"name\":\"lora-0887-gw-1\",\"time\":\"2023-04-21T15:20:50.496608221Z\",\"rssi\":-109,\"loRaSNR\":-0.5,\"location\":{\"latitude\":46.26196,\"longitude\":6.05774,\"altitude\":486}}],\"txInfo\":{\"frequency\":867700000,\"dr\":2},\"adr\":false,\"fCnt\":12,\"fPort\":5,\"data\":\"AAD//wAABwg7DGQHAAAAAAAAAAAAAAAAAAAAAAAAAADfBQAg0ywAAAAAAAAAAAAAAAA=\"}";


  @Override
  protected AbstractStream createStreamDecodeInstance() {
    return new LoraHumTempBatmonDecode(BaseStreamDecodeTest.SOURCE_TOPIC, BaseStreamDecodeTest.SINK_TOPIC);
  }

  @Test
  void LoraBatmonHumTempDecodeTopology_CreatesOutputRecord_forCorrectData() {
    pipeRecord(DATA);
    final ProducerRecord<String, JsonObject> outputRecord = testDriver.readOutput(SINK_TOPIC, KEY_DESERIALIZER, VALUE_DESERIALIZER);

    assertEquals(255, outputRecord.value().get("swbuild").getAsInt());
    assertEquals(12, outputRecord.value().get("packetNumber").getAsInt());
    assertEquals(0, outputRecord.value().get("statusFlag").getAsInt());
    assertEquals(255, outputRecord.value().get("dummyByte").getAsInt());
    assertEquals(3.2983886718749997, outputRecord.value().get("mon33").getAsDouble());
    assertEquals(4.987060546875, outputRecord.value().get("mon5").getAsDouble());
    assertEquals(5.973921199402023, outputRecord.value().get("vBat").getAsDouble());
    assertEquals(60, outputRecord.value().get("extwtdCnt").getAsInt());
    assertEquals(1410323, outputRecord.value().get("humiSumPeriod").getAsInt());
    assertEquals(128, outputRecord.value().get("humiCap0").getAsInt());
    assertEquals(1009, outputRecord.value().get("tempGain").getAsInt());
    assertEquals(1926, outputRecord.value().get("tempRawValue").getAsInt());
    assertEquals(1.5517089843749998, outputRecord.value().get("temperatureAdcConv").getAsDouble());
    assertEquals(34338.43603550926, outputRecord.value().get("temperatureGainAdcConv").getAsDouble());
    assertEquals(1.9088206144697721, outputRecord.value().get("temperatureBridgeVoltage").getAsDouble());
  }

  @Test
  void LoraBatmonHumTempDecodeTopology_CreatesOuputRecord_forDataWithNanValue(){
    pipeRecord(DATA_WITH_NAN_BRIDGE_VOLTAGE);
    final ProducerRecord<String, JsonObject> outputRecord = testDriver.readOutput(SINK_TOPIC, KEY_DESERIALIZER, VALUE_DESERIALIZER);
    assertEquals(0, outputRecord.value().get("temperatureBridgeVoltage").getAsDouble());
  }

}
