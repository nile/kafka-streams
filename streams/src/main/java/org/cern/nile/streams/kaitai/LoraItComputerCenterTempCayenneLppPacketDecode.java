package org.cern.nile.streams.kaitai;

import org.cern.nile.decoders.generated.LoraItComputerCenterTempCayenneLppPacket;

public class LoraItComputerCenterTempCayenneLppPacketDecode extends LoraDecoderStream<LoraItComputerCenterTempCayenneLppPacket> {

  public LoraItComputerCenterTempCayenneLppPacketDecode() {
    super(LoraItComputerCenterTempCayenneLppPacket.class);
  }

  LoraItComputerCenterTempCayenneLppPacketDecode(String sourceTopic, String sinkTopic) {
    super(sourceTopic, sinkTopic, LoraItComputerCenterTempCayenneLppPacket.class);
  }
}
