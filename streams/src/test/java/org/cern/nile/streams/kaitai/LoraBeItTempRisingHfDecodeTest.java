package org.cern.nile.streams.kaitai;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import com.google.gson.JsonObject;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.cern.nile.streams.BaseStreamDecodeTest;
import org.junit.jupiter.api.Test;

public class LoraBeItTempRisingHfDecodeTest extends BaseStreamDecodeTest {

  private static final String RISING_HF_DATA =
      "{\"applicationID\":\"20\",\"applicationName\":\"BE-it-temp\",\"deviceName\":\"RHF-02\",\"devEUI\":\"8cf95740000008bc\",\"rxInfo\":[{\"gatewayID\":\"fcc23dfffe0f57a3\",\"uplinkID\":\"398e37bd-9774-46c1-9d72-4d0a79ae7914\",\"name\":\"lora-0866-gw\",\"time\":\"2022-04-01T07:19:24.00649741Z\",\"rssi\":-99,\"loRaSNR\":3,\"location\":{\"latitude\":46.25694,\"longitude\":6.05851,\"altitude\":470}},{\"gatewayID\":\"fcc23dfffe20e31f\",\"uplinkID\":\"6415116a-5a58-4c3a-8b0a-f9d099e444d8\",\"name\":\"lora-0887-gw-2\",\"time\":\"2022-04-01T07:19:24.029629274Z\",\"rssi\":-100,\"loRaSNR\":-16,\"location\":{\"latitude\":46.26196,\"longitude\":6.05774,\"altitude\":486}},{\"gatewayID\":\"fcc23dfffe2095ff\",\"uplinkID\":\"1c45ae91-5470-418e-b3bb-1e5a7ca02676\",\"name\":\"lora-0866-gw-2\",\"time\":\"2022-04-01T07:19:24.019087316Z\",\"rssi\":-105,\"loRaSNR\":-1.2,\"location\":{\"latitude\":46.25694,\"longitude\":6.05851,\"altitude\":470}}],\"txInfo\":{\"frequency\":867500000,\"dr\":0},\"adr\":true,\"fCnt\":644,\"fPort\":8,\"data\":\"AWxonTAAkCnI\"}";

  private static final String RISING_HF_FAILING_DATA =
      "{\"applicationID\":\"20\",\"applicationName\":\"BE-it-temp\",\"deviceName\":\"RHF-02\",\"devEUI\":\"8cf95740000008bc\",\"rxInfo\":[{\"gatewayID\":\"fcc23dfffe0f57a3\",\"uplinkID\":\"398e37bd-9774-46c1-9d72-4d0a79ae7914\",\"name\":\"lora-0866-gw\",\"rssi\":-99,\"loRaSNR\":3,\"location\":{\"latitude\":46.25694,\"longitude\":6.05851,\"altitude\":470}},{\"gatewayID\":\"fcc23dfffe20e31f\",\"uplinkID\":\"6415116a-5a58-4c3a-8b0a-f9d099e444d8\",\"name\":\"lora-0887-gw-2\",\"time\":\"2022-04-01T07:19:24.029629274Z\",\"rssi\":-100,\"loRaSNR\":-16,\"location\":{\"latitude\":46.26196,\"longitude\":6.05774,\"altitude\":486}},{\"gatewayID\":\"fcc23dfffe2095ff\",\"uplinkID\":\"1c45ae91-5470-418e-b3bb-1e5a7ca02676\",\"name\":\"lora-0866-gw-2\",\"time\":\"2022-04-01T07:19:24.019087316Z\",\"rssi\":-105,\"loRaSNR\":-1.2,\"location\":{\"latitude\":46.25694,\"longitude\":6.05851,\"altitude\":470}}],\"txInfo\":{\"frequency\":867500000,\"dr\":0},\"adr\":true,\"fCnt\":644,\"fPort\":8,\"data\":\"AWxonTAAkCnI\"}";

  @Override
  protected LoraBeItTempRisingHfDecode createStreamDecodeInstance() {
    return new LoraBeItTempRisingHfDecode(BaseStreamDecodeTest.SOURCE_TOPIC, BaseStreamDecodeTest.SINK_TOPIC);
  }

  @Test
  void LoraBeItTempRisingHfDecodeTopology_CreatesOutputRecord_forCorrectData() {
    pipeRecord(RISING_HF_DATA);
    final ProducerRecord<String, JsonObject> outputRecord = testDriver.readOutput(SINK_TOPIC, KEY_DESERIALIZER, VALUE_DESERIALIZER);

    assertEquals(1, outputRecord.value().get("header").getAsInt());
    assertEquals((float) 24.825829, outputRecord.value().get("temperature").getAsFloat());
    assertEquals((float) 70.66016, outputRecord.value().get("humidity").getAsFloat());
    assertEquals((float) 96.0, outputRecord.value().get("period").getAsFloat());
    assertEquals((float) -36.0, outputRecord.value().get("rssi").getAsFloat());
    assertEquals((float) 10.25, outputRecord.value().get("snr").getAsFloat());
    assertEquals((float) 3.5, outputRecord.value().get("battery").getAsFloat());
    assertNotNull(outputRecord.value().get("timestamp"), "Timestamp is null");
  }

  @Test
  void LoraBeItTempRisingHfDecodeTopology_LogsOffset_forIncorrectData() {
    pipeRecord(RISING_HF_DATA);
    pipeRecord(RISING_HF_DATA);
    pipeRecord(RISING_HF_DATA);
    pipeRecord(RISING_HF_FAILING_DATA);
  }

}
