package org.cern.nile.decoders.generated;

import static org.junit.jupiter.api.Assertions.assertEquals;

import com.google.gson.JsonParser;
import java.util.Map;
import org.cern.nile.decoders.KaitaiPacketDecoder;
import org.cern.nile.exceptions.DecodingException;
import org.junit.jupiter.api.Test;

public class LoraRisingHfDecoderTest {

  private static final String LORA_RISING_HF_DATA_FRAME = "{\"data\":\"AWxonTAAkCnI\"}";

  @Test
  void decode_DecodesMessageData_forLoraRisingHfPacket() throws DecodingException {
    Map<String, Object> packet =
        KaitaiPacketDecoder.decode(JsonParser.parseString(LORA_RISING_HF_DATA_FRAME).getAsJsonObject().get("data"), LoraRisingHfPacket.class);
    assertEquals(1, packet.get("header"));
    assertEquals(24.82582763671875, packet.get("temperature"));
    assertEquals(70.66015625, packet.get("humidity"));
    assertEquals(96.0, packet.get("period"));
    assertEquals(-36.0, packet.get("rssi"));
    assertEquals(10.25, packet.get("snr"));
    assertEquals(3.5, packet.get("battery"));
  }

}
