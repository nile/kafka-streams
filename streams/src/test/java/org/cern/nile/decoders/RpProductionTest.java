package org.cern.nile.decoders;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import com.google.gson.JsonParser;
import java.io.IOException;
import java.text.ParseException;
import java.util.Collection;
import java.util.Map;
import java.util.Optional;
import org.cern.nile.exceptions.DecodingException;
import org.junit.jupiter.api.Test;

public class RpProductionTest {

  private static final String message =
      "{\"applicationID\":\"5\",\"applicationName\":\"cern-rp-wmon-test\",\"deviceName\":\"rp-wmon-44\","
          + "\"devEUI\":\"00e839851c156ad8\",\"rxInfo\":[{\"gatewayID\":\"fcc23dfffe0f76fb\",\"uplinkID\":\"28084e96-8c1a-4255-b3b0-7f71ba815819\","
          + "\"name\":\"lora-0227-gw\",\"time\":\"2020-11-18T16:45:38.463126844Z\",\"rssi\":-108,\"loRaSNR\":5.8,\"location\":{\"latitude\":0,"
          + "\"longitude\":0,\"altitude\":0}},{\"gatewayID\":\"fcc23dfffe0f78a6\",\"uplinkID\":\"59ec4ab4-d040-458d-9700-dbfc397bcb32\","
          + "\"name\":\"lora-0060-gw\",\"time\":\"2020-11-18T16:45:38.452045474Z\",\"rssi\":-107,\"loRaSNR\":-2.2,\"location\":{\"latitude\":0,"
          + "\"longitude\":0,\"altitude\":0}}],\"txInfo\":{\"frequency\":867100000,\"dr\":4},\"adr\":true,\"fCnt\":0,\"fPort\":8,"
          + "\"data\":\"3QABAQAAADIAAAAyAAAAAQAAAAIOEA4QDeMAAAAQASIAD/8G7goMAAI=\"}";

  private static final String messageNoRssiNoSnr =
      "{\"applicationID\":\"5\",\"applicationName\":\"cern-rp-wmon-test\",\"deviceName\":\"rp-wmon-44\","
          + "\"devEUI\":\"00e839851c156ad8\",\"rxInfo\":[{\"gatewayID\":\"fcc23dfffe0f76fb\",\"uplinkID\":\"28084e96-8c1a-4255-b3b0-7f71ba815819\","
          + "\"name\":\"lora-0227-gw\",\"time\":\"2020-11-18T16:45:38.463126844Z\",\"location\":{\"latitude\":0,"
          + "\"longitude\":0,\"altitude\":0}},{\"gatewayID\":\"fcc23dfffe0f78a6\",\"uplinkID\":\"59ec4ab4-d040-458d-9700-dbfc397bcb32\","
          + "\"name\":\"lora-0060-gw\",\"time\":\"2020-11-18T16:45:38.452045474Z\",\"location\":{\"latitude\":0,"
          + "\"longitude\":0,\"altitude\":0}}],\"txInfo\":{\"frequency\":867100000,\"dr\":4},\"adr\":true,\"fCnt\":0,\"fPort\":8,"
          + "\"data\":\"3QABAQAAADIAAAAyAAAAAQAAAAIOEA4QDeMAAAAQASIAD/8G7goMAAI=\"}";

  private static final String messageNoTime =
      "{\"applicationID\":\"5\",\"applicationName\":\"cern-rp-wmon-test\",\"deviceName\":\"rp-wmon-44\","
          + "\"devEUI\":\"00e839851c156ad8\",\"rxInfo\":[{\"gatewayID\":\"fcc23dfffe0f76fb\",\"uplinkID\":\"28084e96-8c1a-4255-b3b0-7f71ba815819\","
          + "\"name\":\"lora-0227-gw\",\"rssi\":-108,\"loRaSNR\":5.8,\"location\":{\"latitude\":0,"
          + "\"longitude\":0,\"altitude\":0}},{\"gatewayID\":\"fcc23dfffe0f78a6\",\"uplinkID\":\"59ec4ab4-d040-458d-9700-dbfc397bcb32\","
          + "\"name\":\"lora-0060-gw\",\"rssi\":-107,\"loRaSNR\":-2.2,\"location\":{\"latitude\":0,"
          + "\"longitude\":0,\"altitude\":0}}],\"txInfo\":{\"frequency\":867100000,\"dr\":4},\"adr\":true,\"fCnt\":0,\"fPort\":8,"
          + "\"data\":\"3QABAQAAADIAAAAyAAAAAQAAAAIOEA4QDeMAAAAQASIAD/8G7goMAAI=\"}";

  // packet #1 => 1h loss; alarm = 0; counts_alarm = 13; count = 5; counts1h = 10; counts2h = 20;
  private static final String msg1 =
      "{\"applicationID\":\"5\",\"applicationName\":\"cern-rp-wmon-calibration\",\"deviceName\":\"cern-rp-wmon-calibration-01\","
          + "\"devEUI\":\"190b2ca38291b2bf\",\"rxInfo\":[{\"gatewayID\":\"fcc23dfffe2064e4\",\"uplinkID\":\"fce95233-ee82-49f0-b688-b17bb7b7c60f\","
          + "\"name\":\"lora-0060-gw-2\",\"time\":\"2021-01-14T09:32:03.733724076Z\",\"rssi\":-98,\"loRaSNR\":2.5,\"location\":{\"latitude\":46"
          + ".23144,\"longitude\":6.05452,\"altitude\":480}},{\"gatewayID\":\"fcc23dfffe0f3afd\","
          + "\"uplinkID\":\"c05491f4-b198-4a7b-a753-5a7736c4ac54\",\"name\":\"lora-0157-gw\",\"time\":\"2021-01-14T09:32:03.730387535Z\","
          + "\"rssi\":-101,\"loRaSNR\":0,\"location\":{\"latitude\":46.23207,\"longitude\":6.05053,\"altitude\":460}},"
          + "{\"gatewayID\":\"fcc23dfffe0f78a6\",\"uplinkID\":\"4f8c6e8f-ed48-4213-9b19-3b2d9bd8c224\",\"name\":\"lora-0060-gw\","
          + "\"time\":\"2021-01-14T09:32:03.738501395Z\",\"rssi\":-94,\"loRaSNR\":-2.2,\"location\":{\"latitude\":46.23144,\"longitude\":6.05452,"
          + "\"altitude\":480}},{\"gatewayID\":\"fcc23dfffe0f76fb\",\"uplinkID\":\"c8f87a1c-3559-41c1-b16e-1b06ebd29f4c\",\"name\":\"lora-0227-gw\","
          + "\"time\":\"2021-01-14T09:32:03.736185933Z\",\"rssi\":-93,\"loRaSNR\":-0.2,\"location\":{\"latitude\":46.23431,\"longitude\":6.04088,"
          + "\"altitude\":488}}],\"txInfo\":{\"frequency\":867500000,\"dr\":4},\"adr\":false,\"fCnt\":0,\"fPort\":3,"
          + "\"data\":\"3QABAAAAAA0AAAAFAAAACgAAABQAHgAAAAAAAAAAAAAAD/8J/gpiAAE=\"}";

  // packet #2 => 2h loss; alarm = 0; counts_alarm = 13; count = 5; counts1h = 10; counts2h = 20;
  private static final String msg2 =
      "{\"applicationID\":\"5\",\"applicationName\":\"cern-rp-wmon-calibration\",\"deviceName\":\"cern-rp-wmon-calibration-01\","
          + "\"devEUI\":\"190b2ca38291b2bf\",\"rxInfo\":[{\"gatewayID\":\"fcc23dfffe0f3afd\",\"uplinkID\":\"5e21bfdd-d6fa-414a-bb29-70b78ba7c990\","
          + "\"name\":\"lora-0157-gw\",\"time\":\"2021-01-14T09:33:03.736279042Z\",\"rssi\":-101,\"loRaSNR\":0.5,\"location\":{\"latitude\":46.23207,"
          + "\"longitude\":6.05053,\"altitude\":460}},{\"gatewayID\":\"fcc23dfffe2064e4\",\"uplinkID\":\"67b9609b-1b2b-4047-b39b-f632c7eff031\","
          + "\"name\":\"lora-0060-gw-2\",\"time\":\"2021-01-14T09:33:03.738209622Z\",\"rssi\":-95,\"loRaSNR\":8.2,\"location\":{\"latitude\":46"
          + ".23144,\"longitude\":6.05452,\"altitude\":480}},{\"gatewayID\":\"fcc23dfffe0f76fb\","
          + "\"uplinkID\":\"c168e903-808c-4cdb-94bb-08d4c6309fc4\",\"name\":\"lora-0227-gw\",\"time\":\"2021-01-14T09:33:03.723859586Z\","
          + "\"rssi\":-95,\"loRaSNR\":8.2,\"location\":{\"latitude\":46.23431,\"longitude\":6.04088,\"altitude\":488}},"
          + "{\"gatewayID\":\"fcc23dfffe21f4aa\",\"uplinkID\":\"fb328cd9-a06c-4a8f-9786-e381a33a5b83\",\"name\":\"lora-0227-gw-2\","
          + "\"time\":\"2021-01-14T09:33:03.727641324Z\",\"rssi\":-106,\"loRaSNR\":-8,\"location\":{\"latitude\":46.23431,\"longitude\":6.04088,"
          + "\"altitude\":488}},{\"gatewayID\":\"fcc23dfffe0f78a6\",\"uplinkID\":\"00f28393-82a5-4a99-a019-98730e85668c\",\"name\":\"lora-0060-gw\","
          + "\"time\":\"2021-01-14T09:33:03.736630002Z\",\"rssi\":-101,\"loRaSNR\":1.5,\"location\":{\"latitude\":46.23144,\"longitude\":6.05452,"
          + "\"altitude\":480}}],\"txInfo\":{\"frequency\":867700000,\"dr\":4},\"adr\":false,\"fCnt\":1,\"fPort\":3,"
          + "\"data\":\"3QACAAAAAA0AAAAFAAAACgAAABQAHgAeAAAAAAAAAAAAD/8J+gpiAAI=\"}";

  // 1h loss pkt number 4 exwdtc - 240
  private static final String msg3 =
      "{\"applicationID\":\"5\",\"applicationName\":\"cern-rp-wmon-calibration\",\"deviceName\":\"cern-rp-wmon-calibration-01\","
          + "\"devEUI\":\"190b2ca38291b2bf\",\"rxInfo\":[{\"gatewayID\":\"fcc23dfffe0f78a6\",\"uplinkID\":\"367aaa6f-2583-46de-803e-b52c487ee0f5\","
          + "\"name\":\"lora-0060-gw\",\"time\":\"2021-01-14T10:23:21.243303513Z\",\"rssi\":-96,\"loRaSNR\":-2,\"location\":{\"latitude\":46.23144,"
          + "\"longitude\":6.05452,\"altitude\":480}},{\"gatewayID\":\"fcc23dfffe2064e4\",\"uplinkID\":\"c445a909-cf8f-4fce-94d9-869892498502\","
          + "\"name\":\"lora-0060-gw-2\",\"time\":\"2021-01-14T10:23:21.23934582Z\",\"rssi\":-95,\"loRaSNR\":5,\"location\":{\"latitude\":46.23144,"
          + "\"longitude\":6.05452,\"altitude\":480}},{\"gatewayID\":\"fcc23dfffe0f3afd\",\"uplinkID\":\"a0715cbe-a3db-479d-a70c-9a450fc471e0\","
          + "\"name\":\"lora-0157-gw\",\"time\":\"2021-01-14T10:23:21.234174295Z\",\"rssi\":-100,\"loRaSNR\":-0.8,\"location\":{\"latitude\":46"
          + ".23207,\"longitude\":6.05053,\"altitude\":460}}],\"txInfo\":{\"frequency\":867500000,\"dr\":4},\"adr\":false,\"fCnt\":0,\"fPort\":3,"
          + "\"data\":\"3QAEAAAAAAEAAAAFAAAACgAAABQAHgAAAAAAAAAAAAAAD/8J/gpiAPA=\"}";

  // 2h loss pkt number 4 exwdtc - 240
  private static final String msg4 =
      "{\"applicationID\":\"5\",\"applicationName\":\"cern-rp-wmon-calibration\",\"deviceName\":\"cern-rp-wmon-calibration-01\","
          + "\"devEUI\":\"190b2ca38291b2bf\",\"rxInfo\":[{\"gatewayID\":\"fcc23dfffe0f78a6\",\"uplinkID\":\"367aaa6f-2583-46de-803e-b52c487ee0f5\","
          + "\"name\":\"lora-0060-gw\",\"time\":\"2021-01-14T10:23:21.243303513Z\",\"rssi\":-96,\"loRaSNR\":-2,\"location\":{\"latitude\":46.23144,"
          + "\"longitude\":6.05452,\"altitude\":480}},{\"gatewayID\":\"fcc23dfffe2064e4\",\"uplinkID\":\"c445a909-cf8f-4fce-94d9-869892498502\","
          + "\"name\":\"lora-0060-gw-2\",\"time\":\"2021-01-14T10:23:21.23934582Z\",\"rssi\":-95,\"loRaSNR\":5,\"location\":{\"latitude\":46.23144,"
          + "\"longitude\":6.05452,\"altitude\":480}},{\"gatewayID\":\"fcc23dfffe0f3afd\",\"uplinkID\":\"a0715cbe-a3db-479d-a70c-9a450fc471e0\","
          + "\"name\":\"lora-0157-gw\",\"time\":\"2021-01-14T10:23:21.234174295Z\",\"rssi\":-100,\"loRaSNR\":-0.8,\"location\":{\"latitude\":46"
          + ".23207,\"longitude\":6.05053,\"altitude\":460}}],\"txInfo\":{\"frequency\":867500000,\"dr\":4},\"adr\":false,\"fCnt\":0,\"fPort\":3,"
          + "\"data\":\"3QAEAAAAAAEAAAAFAAAACgAAABQAHgAAAAAAAAAAAAAAD/8J/gpiAPA=\"}";

  // 2h loss pkt number 4 exwdtc - 240 ALARM = 1
  private static final String msg5 =
      "{\"applicationID\":\"5\",\"applicationName\":\"cern-rp-wmon-calibration\",\"deviceName\":\"cern-rp-wmon-calibration-01\","
          + "\"devEUI\":\"190b2ca38291b2bf\",\"rxInfo\":[{\"gatewayID\":\"fcc23dfffe0f3afd\",\"uplinkID\":\"71194a44-6845-40f9-8a94-736b2cdd6749\","
          + "\"name\":\"lora-0157-gw\",\"time\":\"2021-01-14T10:23:21.243303513Z\",\"rssi\":-106,\"loRaSNR\":-8.8,\"location\":{\"latitude\":46"
          + ".23207,\"longitude\":6.05053,\"altitude\":460}},{\"gatewayID\":\"fcc23dfffe2064e4\","
          + "\"uplinkID\":\"ca455a63-8662-4f46-814e-fdd8ac20b5dc\",\"name\":\"lora-0060-gw-2\",\"time\":\"2021-01-14T10:32:45.624172466Z\","
          + "\"rssi\":-105,\"loRaSNR\":-3.2,\"location\":{\"latitude\":46.23144,\"longitude\":6.05452,\"altitude\":480}},"
          + "{\"gatewayID\":\"fcc23dfffe0f76fb\",\"uplinkID\":\"03a53130-ce58-47d3-80b4-a4494628a1e3\",\"name\":\"lora-0227-gw\","
          + "\"time\":\"2021-01-14T10:32:45.618829431Z\",\"rssi\":-104,\"loRaSNR\":3.8,\"location\":{\"latitude\":46.23431,\"longitude\":6.04088,"
          + "\"altitude\":488}},{\"gatewayID\":\"fcc23dfffe21f4aa\",\"uplinkID\":\"5141e116-e1d4-47b3-9234-52c27cb721f2\","
          + "\"name\":\"lora-0227-gw-2\",\"time\":\"2021-01-14T10:32:45.61907618Z\",\"rssi\":-107,\"loRaSNR\":-7.5,\"location\":{\"latitude\":46"
          + ".23431,\"longitude\":6.04088,\"altitude\":488}}],\"txInfo\":{\"frequency\":867100000,\"dr\":4},\"adr\":false,\"fCnt\":0,\"fPort\":3,"
          + "\"data\":\"3QAEAQAAAAEAAAAFAAAACgAAABQAHgAAAAAAAAAAAAAAD/8KAgpiAPA=\"}";

  // 2h loss pkt number 4 exwdtc - 240 ALARM = 2
  private static final String msg6 =
      "{\"applicationID\":\"5\",\"applicationName\":\"cern-rp-wmon-calibration\",\"deviceName\":\"cern-rp-wmon-calibration-01\","
          + "\"devEUI\":\"190b2ca38291b2bf\",\"rxInfo\":[{\"gatewayID\":\"fcc23dfffe21f4aa\",\"uplinkID\":\"9ab2abff-048c-4b17-b33a-263c3935265b\","
          + "\"name\":\"lora-0227-gw-2\",\"time\":\"2021-01-14T10:23:21.243303513Z\",\"rssi\":-107,\"loRaSNR\":-4.5,\"location\":{\"latitude\":46"
          + ".23431,\"longitude\":6.04088,\"altitude\":488}},{\"gatewayID\":\"fcc23dfffe0f78a6\","
          + "\"uplinkID\":\"bd8342d3-c7d2-492b-bc81-3745578a9881\",\"name\":\"lora-0060-gw\",\"time\":\"2021-01-14T10:36:41.916904504Z\","
          + "\"rssi\":-100,\"loRaSNR\":4.5,\"location\":{\"latitude\":46.23144,\"longitude\":6.05452,\"altitude\":480}},"
          + "{\"gatewayID\":\"fcc23dfffe0f3afd\",\"uplinkID\":\"18c9181b-e532-4b00-ac51-4a0a529874d4\",\"name\":\"lora-0157-gw\","
          + "\"time\":\"2021-01-14T10:36:41.915233848Z\",\"rssi\":-101,\"loRaSNR\":-2.8,\"location\":{\"latitude\":46.23207,\"longitude\":6.05053,"
          + "\"altitude\":460}},{\"gatewayID\":\"fcc23dfffe0f76fb\",\"uplinkID\":\"8056c850-02d5-4710-a03f-3b0fd26a2bea\",\"name\":\"lora-0227-gw\","
          + "\"time\":\"2021-01-14T10:36:41.906744507Z\",\"rssi\":-106,\"loRaSNR\":0,\"location\":{\"latitude\":46.23431,\"longitude\":6.04088,"
          + "\"altitude\":488}},{\"gatewayID\":\"fcc23dfffe2064e4\",\"uplinkID\":\"d06cc721-a453-4164-8ae3-aeb20920165f\","
          + "\"name\":\"lora-0060-gw-2\",\"time\":\"2021-01-14T10:36:41.910863261Z\",\"rssi\":-97,\"loRaSNR\":8.5,\"location\":{\"latitude\":46"
          + ".23144,\"longitude\":6.05452,\"altitude\":480}}],\"txInfo\":{\"frequency\":867300000,\"dr\":4},\"adr\":false,\"fCnt\":0,\"fPort\":3,"
          + "\"data\":\"3QAEAgAAAAEAAAAFAAAACgAAABQAHgAAAAAAAAAAAAAAD/8J/gpiAPA=\"}";

  // 2h loss pkt number 4 exwdtc - 240 ALARM = 3
  private static final String msg7 =
      "{\"applicationID\":\"5\",\"applicationName\":\"cern-rp-wmon-calibration\",\"deviceName\":\"cern-rp-wmon-calibration-01\","
          + "\"devEUI\":\"190b2ca38291b2bf\",\"rxInfo\":[{\"gatewayID\":\"fcc23dfffe0f78a6\",\"uplinkID\":\"47db6420-bc49-41cd-a3a7-db35e031c36d\","
          + "\"name\":\"lora-0060-gw\",\"time\":\"2021-01-14T10:23:21.243303513Z\",\"rssi\":-103,\"loRaSNR\":-1,\"location\":{\"latitude\":46.23144,"
          + "\"longitude\":6.05452,\"altitude\":480}},{\"gatewayID\":\"fcc23dfffe21f4aa\",\"uplinkID\":\"2e7f133b-7945-4ce8-9a5a-8401d554397b\","
          + "\"name\":\"lora-0227-gw-2\",\"time\":\"2021-01-14T10:41:28.160166866Z\",\"rssi\":-107,\"loRaSNR\":-6.2,\"location\":{\"latitude\":46"
          + ".23431,\"longitude\":6.04088,\"altitude\":488}},{\"gatewayID\":\"fcc23dfffe0f3afd\","
          + "\"uplinkID\":\"d3074401-3559-42b8-8a42-96c635e84542\",\"name\":\"lora-0157-gw\",\"time\":\"2021-01-14T10:41:28.160720122Z\","
          + "\"rssi\":-106,\"loRaSNR\":-9,\"location\":{\"latitude\":46.23207,\"longitude\":6.05053,\"altitude\":460}},"
          + "{\"gatewayID\":\"fcc23dfffe0f76fb\",\"uplinkID\":\"b67498c3-25f6-4b3f-80b7-4dc3efcef3dc\",\"name\":\"lora-0227-gw\","
          + "\"time\":\"2021-01-14T10:41:28.16149853Z\",\"rssi\":-104,\"loRaSNR\":2.5,\"location\":{\"latitude\":46.23431,\"longitude\":6.04088,"
          + "\"altitude\":488}},{\"gatewayID\":\"fcc23dfffe2064e4\",\"uplinkID\":\"05c56712-e154-4145-bda4-c6df43990652\","
          + "\"name\":\"lora-0060-gw-2\",\"time\":\"2021-01-14T10:41:28.163552026Z\",\"rssi\":-100,\"loRaSNR\":6.2,\"location\":{\"latitude\":46"
          + ".23144,\"longitude\":6.05452,\"altitude\":480}}],\"txInfo\":{\"frequency\":867100000,\"dr\":4},\"adr\":false,\"fCnt\":1,\"fPort\":3,"
          + "\"data\":\"3QAEAwAAAAEAAAAFAAAACgAAABQAHgAeAAAAAAAAAAAAD/8KAApiAPA=\"}\n";

  // 2h loss pkt number 4 exwdtc - 240 ALARM = 4
  private static final String msg8 =
      "{\"applicationID\":\"5\",\"applicationName\":\"cern-rp-wmon-calibration\",\"deviceName\":\"cern-rp-wmon-calibration-01\","
          + "\"devEUI\":\"190b2ca38291b2bf\",\"rxInfo\":[{\"gatewayID\":\"fcc23dfffe0f78a6\",\"uplinkID\":\"c95a1508-e57a-4c87-b088-5db5dd449351\","
          + "\"name\":\"lora-0060-gw\",\"time\":\"2021-01-14T10:23:21.243303513Z\",\"rssi\":-105,\"loRaSNR\":0.5,\"location\":{\"latitude\":46.23144,"
          + "\"longitude\":6.05452,\"altitude\":480}},{\"gatewayID\":\"fcc23dfffe0f3afd\",\"uplinkID\":\"e16dbf98-448d-454c-84e6-4aaf7083b6d5\","
          + "\"name\":\"lora-0157-gw\",\"time\":\"2021-01-14T10:46:00.172483007Z\",\"rssi\":-104,\"loRaSNR\":-8.5,\"location\":{\"latitude\":46"
          + ".23207,\"longitude\":6.05053,\"altitude\":460}},{\"gatewayID\":\"fcc23dfffe2064e4\","
          + "\"uplinkID\":\"1d14cb8c-444c-41f0-af2a-cd82e66012a7\",\"name\":\"lora-0060-gw-2\",\"time\":\"2021-01-14T10:46:00.179459949Z\","
          + "\"rssi\":-105,\"loRaSNR\":1,\"location\":{\"latitude\":46.23144,\"longitude\":6.05452,\"altitude\":480}}],"
          + "\"txInfo\":{\"frequency\":867100000,\"dr\":4},\"adr\":false,\"fCnt\":0,\"fPort\":3,"
          + "\"data\":\"3QAEBAAAAAEAAAAFAAAACgAAABQAHgAAAAAAAAAAAAAAD/8KAApiAPA=\"}\n";

  // 2h loss pkt number 4 exwdtc - 240 ALARM = 5
  private static final String msg9 =
      "{\"applicationID\":\"5\",\"applicationName\":\"cern-rp-wmon-calibration\",\"deviceName\":\"cern-rp-wmon-calibration-01\","
          + "\"devEUI\":\"190b2ca38291b2bf\",\"rxInfo\":[{\"gatewayID\":\"fcc23dfffe0f76fb\",\"uplinkID\":\"bc63976c-23bf-4256-a2a3-e2581aaf94c7\","
          + "\"name\":\"lora-0227-gw\",\"time\":\"2021-01-14T10:23:21.243303513Z\",\"rssi\":-103,\"loRaSNR\":5.2,\"location\":{\"latitude\":46.23431,"
          + "\"longitude\":6.04088,\"altitude\":488}},{\"gatewayID\":\"fcc23dfffe0f78a6\",\"uplinkID\":\"3fd3b5e2-f644-4538-aba1-66741017fdd5\","
          + "\"name\":\"lora-0060-gw\",\"time\":\"2021-01-14T10:50:31.756221159Z\",\"rssi\":-99,\"loRaSNR\":3.8,\"location\":{\"latitude\":46.23144,"
          + "\"longitude\":6.05452,\"altitude\":480}},{\"gatewayID\":\"fcc23dfffe0f3afd\",\"uplinkID\":\"6001f922-154a-4812-b1ac-96f492f30c86\","
          + "\"name\":\"lora-0157-gw\",\"time\":\"2021-01-14T10:50:31.749061838Z\",\"rssi\":-103,\"loRaSNR\":2,\"location\":{\"latitude\":46.23207,"
          + "\"longitude\":6.05053,\"altitude\":460}},{\"gatewayID\":\"fcc23dfffe2064e4\",\"uplinkID\":\"740ff44e-1843-4d3c-9b2b-79ab59c7d7e4\","
          + "\"name\":\"lora-0060-gw-2\",\"time\":\"2021-01-14T10:50:31.746863207Z\",\"rssi\":-94,\"loRaSNR\":10.2,\"location\":{\"latitude\":46"
          + ".23144,\"longitude\":6.05452,\"altitude\":480}},{\"gatewayID\":\"fcc23dfffe21f4aa\","
          + "\"uplinkID\":\"dbcb8ced-0996-4216-bf51-2cc5d2460535\",\"name\":\"lora-0227-gw-2\",\"time\":\"2021-01-14T10:50:31.759134783Z\","
          + "\"rssi\":-104,\"loRaSNR\":-2.2,\"location\":{\"latitude\":46.23431,\"longitude\":6.04088,\"altitude\":488}}],"
          + "\"txInfo\":{\"frequency\":867700000,\"dr\":4},\"adr\":false,\"fCnt\":0,\"fPort\":3,"
          + "\"data\":\"3QAEBQAAAAEAAAAFAAAACgAAABQAHgAAAAAAAAAAAAAAD/8J+ApiAPA=\"}\n";

  @Test
  void decode_DecodesMessage_forCorrectMessage() throws IOException, ParseException, DecodingException {
    Optional<Map<String, Object>> decode = RpProduction.decode(
        JsonParser.parseString(message).getAsJsonObject(), null).stream().findFirst();
    assertTrue(decode.isPresent());

    assertEquals(2572, decode.get().get("monVin"));
    assertEquals(4.765986328125, decode.get().get("monVin_voltage"));
    assertEquals(5, decode.get().get("snr_gw_0"));
    assertEquals(-2, decode.get().get("snr_gw_1"));
    assertEquals(3.2991943359375, decode.get().get("mon3v3_voltage"));
    assertEquals(2, decode.get().get("exwdtc"));
    assertEquals("8", decode.get().get("fPort"));
    assertEquals(50, decode.get().get("counts"));
    assertEquals(4095, decode.get().get("mon3v3"));
    assertEquals(221, decode.get().get("dev_ID"));
    assertEquals(1.429248046875, decode.get().get("mon5_voltage"));
    assertEquals("rp-wmon-44", decode.get().get("device_name"));
    assertEquals(16, decode.get().get("shock_counts"));
    assertEquals(1, decode.get().get("alarm"));
    assertEquals(290, decode.get().get("temperature"));
    assertEquals(50, decode.get().get("alarm_counts"));
    assertEquals(1774, decode.get().get("mon5"));
    assertEquals(3600, decode.get().get("checking_time"));
    assertEquals(1, decode.get().get("package_num"));
    assertEquals(1605717938463L, decode.get().get("timestamp"));
    assertEquals(-108, decode.get().get("rssi_gw_0"));
    assertEquals(-107, decode.get().get("rssi_gw_1"));
    assertEquals(5, decode.get().get("snr_gw_0"));
    assertEquals(-2, decode.get().get("snr_gw_1"));
  }

  @Test
  void decode_DecodesMessage_forCorrectMessageWithNoRssi()
      throws IOException, ParseException, DecodingException {
    Optional<Map<String, Object>> decode = RpProduction.decode(
        JsonParser.parseString(messageNoRssiNoSnr).getAsJsonObject(), null).stream().findFirst();
    assertTrue(decode.isPresent());

    assertNull(decode.get().get("rssi_gw_0"));
    assertNull(decode.get().get("rssi_gw_1"));
    assertNull(decode.get().get("snr_gw_0"));
    assertNull(decode.get().get("snr_gw_1"));
  }

  @Test
  void decode_ThrowsException_forMessageWithNoTime() {
    NullPointerException ex = assertThrows(NullPointerException.class,
        () -> RpProduction.decode(JsonParser.parseString(messageNoTime).getAsJsonObject(), 1610615001000L));
    assertTrue(ex.getMessage().contains("Current timestamp was not present in the message"));
  }

  @Test
  void decode_ThrowsException_for1hRedundantMessage() {
    DecodingException ex = assertThrows(DecodingException.class,
        () -> RpProduction.decode(JsonParser.parseString(msg1).getAsJsonObject(), 1610611803000L));
    assertTrue(ex.getMessage().contains("had data loss of 1 hour but packageNum was 1"));
  }

  @Test
  void decode_DecodesMessage_for1hRedundantMessage()
      throws IOException, ParseException, DecodingException {
    Collection<Map<String, Object>> decode =
        RpProduction.decode(JsonParser.parseString(msg3).getAsJsonObject(), 1610615001000L);
    assertEquals(2, decode.size());
  }

  @Test
  void decode_DecodesMessage_for2hRedundantMessageWithPackageSize2()
      throws DecodingException, IOException, ParseException {
      Collection<Map<String, Object>> decode = RpProduction.decode(
          JsonParser.parseString(msg2).getAsJsonObject(), 1610604603000L);
      assertEquals(2, decode.size());
  }

  @Test
  void decode_DecodesMessage_for2hRedundantMessageWithPackageSize3()
      throws IOException, ParseException, DecodingException {
    Collection<Map<String, Object>> decode = RpProduction.decode(
        JsonParser.parseString(msg4).getAsJsonObject(), 1610607801000L);
    assertEquals(3, decode.size());
  }

  @Test
  void decode_DecodesMessage_for2hRedundantMessageWithAlarm()
      throws IOException, ParseException, DecodingException {
    Collection<Map<String, Object>> decode = RpProduction.decode(
        JsonParser.parseString(msg5).getAsJsonObject(), 1610607801000L);
    assertEquals(3, decode.size());
  }

  @Test
  void decode_DecodesMessage_for2hRedundantMessageWithAlarm2()
      throws IOException, ParseException, DecodingException {
    Collection<Map<String, Object>> decode = RpProduction.decode(
        JsonParser.parseString(msg6).getAsJsonObject(), 1610607801000L);
    assertEquals(3, decode.size());

  }

  @Test
  void decode_DecodesMessage_for2hRedundantMessageWithAlarm3()
      throws IOException, ParseException, DecodingException {
    Collection<Map<String, Object>> decode = RpProduction.decode(
        JsonParser.parseString(msg7).getAsJsonObject(), 1610607801000L);
    assertEquals(3, decode.size());
  }

  @Test
  void decode_DecodesMessage_for2hRedundantMessageWithAlarm4()
      throws IOException, ParseException, DecodingException {
    Collection<Map<String, Object>> decode = RpProduction.decode(
        JsonParser.parseString(msg8).getAsJsonObject(), 1610607801000L);
    assertEquals(3, decode.size());
  }

  @Test
  void decode_DecodesMessage_for2hRedundantMessageWithAlarm5()
      throws IOException, ParseException, DecodingException {
    Collection<Map<String, Object>> decode = RpProduction.decode(
        JsonParser.parseString(msg9).getAsJsonObject(), 1610607801000L);
    assertEquals(3, decode.size());
  }

}
