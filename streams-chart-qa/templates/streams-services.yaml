apiVersion: apps/v1
kind: Deployment
metadata:
  name: {{ .Values.config.name }}
  namespace: {{ .Values.config.namespace }}
  labels:
    app.kubernetes.io/name: {{ .Values.config.name }}
    app.kubernets.io/version: {{ .Values.config.version }}
spec:
  replicas: 1
  selector:
    matchLabels:
      app.kubernetes.io/name: {{ .Values.config.name }}
      app.kubernets.io/version: {{ .Values.config.version }}
  template:
    metadata:
      annotations:
        rollme: {{ randAlphaNum 5 | quote }}
      labels:
        app.kubernetes.io/name: {{ .Values.config.name }}
        app.kubernets.io/version: {{ .Values.config.version }}
    spec:
      containers:
      - image: {{ .Values.common.image }}:{{ .Values.config.image_tag }}
        name: {{ .Values.config.name }}
        imagePullPolicy: Always
        livenessProbe:
          httpGet:
            path: /health
            port: 8899
        env:
        - name: INFLUX_DB
          value: {{ .Values.influx.db }}
        - name: INFLUX_URL
          value: {{ .Values.influx.url }}
        - name: INFLUX_USER
          value: {{ .Values.influx.user }}
        - name: INFLUX_PW
          valueFrom:
            secretKeyRef:
              name: influxpw
              key: influxpw
        - name: JAVA_JVM_OPTS
          value: {{ .Values.common.jvm_opts }}
        volumeMounts:
        - mountPath: /data
          name: data
        - mountPath: /kafka/streams.properties
          name: {{ .Values.config.cm_name }}
          subPath: streams.properties
        - mountPath: /kafka/nile.keytab
          subPath: nile.keytab
          name: cert
          readOnly: true
        - mountPath: /kafka/cacerts
          subPath: cacerts
          name: cert
          readOnly: true
        - mountPath: /etc/krb5.conf
          subPath: krb5.conf
          name: cert
          readOnly: true
        - mountPath: /kafka/jaas.conf
          subPath: jaas.conf
          name: cert
          readOnly: true
        {{ if .Values.config.routing_config_path }}
        - mountPath: {{ .Values.config.routing_config_path }}
          subPath: config.json
          name: {{ .Values.config.cm_name }}
          readOnly: true
        {{ end }}
        {{ if .Values.config.enrichment_config_path }}
        - mountPath: {{ .Values.config.enrichment_config_path }}
          subPath: spec.json
          name: {{ .Values.config.cm_name }}
          readOnly: true
        {{ end }}
      volumes:
        - name: data
          emptyDir: {}
        - name: {{ .Values.config.cm_name }}
          configMap:
            name: {{ .Values.config.cm_name }}
        - name: cert
          secret:
            secretName: cert
      restartPolicy: Always
---
apiVersion: v1
kind: ConfigMap
metadata:
  name: {{ .Values.config.cm_name }}
  namespace: {{ .Values.config.namespace }}
  labels:
    app.kubernetes.io/name: {{ .Values.config.name }}
    app.kubernets.io/version: {{ .Values.config.version }}
data:
  streams.properties: |-
    stream.type={{ .Values.config.stream_type }}
    stream.class={{ .Values.config.stream_class }}
    kafka.cluster={{ .Values.config.kafka_cluster }}
    source.topic={{ .Values.config.source_topic }}
    {{ if .Values.config.sink_topic }}
    sink.topic={{ .Values.config.sink_topic }}
    {{ end }}
    {{ if .Values.config.dlq_topic }}
    dlq.topic={{ .Values.config.dlq_topic }}
    {{ end }}
    truststore.location={{ .Values.common.truststore_location }}
    client.id={{ .Values.config.client_id }}
    {{ if .Values.config.routing_config_path }}
    routing.config.path={{ .Values.config.routing_config_path }}
    {{ end }}
    {{ if .Values.config.enrichment_config_path }}
    enrichment.config.path={{ .Values.config.enrichment_config_path }}
    {{ end }}

  {{ if .Values.config.routing_config_path }}
  config.json: |- 
    {{ .Files.Get "config/lora-routing/config.json" }}
  {{ end }}

  {{ if .Values.config.enrichment_config_path }}
  spec.json: |-
    {{ .Files.Get "config/remus/spec.json" }}
  {{ end }}