package org.cern.nile.streams.kaitai;

import com.google.gson.JsonObject;
import java.util.HashMap;
import java.util.Map;
import org.cern.nile.decoders.generated.LoraEnAccessControlPacket;
import org.cern.nile.schema.connect.SchemaInjector;

public class LoraEnAccessControlDecode extends LoraDecoderStream<LoraEnAccessControlPacket> {

  private static final Map<String, String> CUSTOM_METADATA_FIELD_MAP = new HashMap<>();

  static {
    CUSTOM_METADATA_FIELD_MAP.put("devEUI", "DEVEUI");
    CUSTOM_METADATA_FIELD_MAP.put("timestamp", "TIMESTAMP");
    CUSTOM_METADATA_FIELD_MAP.put("data", "DATA");
  }

  /**
   * Constructor for LoraEnAccessControlDecode.
   */
  public LoraEnAccessControlDecode() {
    super(LoraEnAccessControlPacket.class);
    super.setCustomMetadataFields(CUSTOM_METADATA_FIELD_MAP);
    super.setAddFrameType(true);
  }

  LoraEnAccessControlDecode(String sourceTopic, String sinkTopic) {
    super(sourceTopic, sinkTopic, LoraEnAccessControlPacket.class);
    super.setCustomMetadataFields(CUSTOM_METADATA_FIELD_MAP);
    super.setAddFrameType(true);
  }

  @Override
  public Map<String, Object> enrichCustomFunction(Map<String, Object> map, JsonObject value) {
    return SchemaInjector.inject(map);
  }

}