package org.cern.nile.schema.connect;

import java.util.Date;

enum JsonType {
  BYTE(Byte.class, "int8"),
  SHORT(Short.class, "int16"),
  INTEGER(Integer.class, "int32"),
  LONG(Long.class, "int64"),
  FLOAT(Float.class, "float"),
  DOUBLE(Double.class, "double"),
  BOOLEAN(Boolean.class, "boolean"),
  STRING(String.class, "string"),
  DATE(Date.class, "int64"),
  BYTE_ARRAY(byte[].class, "bytes");

  private final Class<?> clazz;
  private final String type;

  JsonType(Class<?> clazz, String type) {
    this.clazz = clazz;
    this.type = type;
  }

  public Class<?> getClazz() {
    return clazz;
  }

  public String getType() {
    return type;
  }

  public static JsonType fromClass(Class<?> clazz) {
    for (JsonType jsonType : JsonType.values()) {
      if (jsonType.getClazz().equals(clazz)) {
        return jsonType;
      }
    }
    throw new IllegalArgumentException("Unsupported class: " + clazz.getSimpleName());
  }
}
