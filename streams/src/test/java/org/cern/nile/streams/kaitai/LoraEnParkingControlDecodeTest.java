package org.cern.nile.streams.kaitai;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

import com.google.gson.JsonObject;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.cern.nile.streams.BaseStreamDecodeTest;
import org.junit.jupiter.api.Test;

public class LoraEnParkingControlDecodeTest extends BaseStreamDecodeTest {

  private static final String LORA_EN_PARKING_CONTROL_STATUS_REPORT_FRAME =
      "{\"applicationID\":\"26\",\"applicationName\":\"lora-EN-access-control-testing\",\"deviceName\":\"test-cicicom\",\"devEUI\":\"0004a30b00ffa4ac\",\"rxInfo\":[{\"gatewayID\":\"fcc23dfffe20d13b\",\"uplinkID\":\"27bbee0c-8a2c-4b0f-a45f-a634fd4c6860\",\"name\":\"lora-0024-gw-2\",\"time\":\"2023-03-27T09:41:15.232962518Z\",\"rssi\":-111,\"loRaSNR\":-10,\"location\":{\"latitude\":46.22932,\"longitude\":6.05161,\"altitude\":480}},{\"gatewayID\":\"fcc23dfffe21f4aa\",\"uplinkID\":\"2fd0f6d8-9f5f-4893-baa8-a686ebe0d33b\",\"name\":\"lora-0227-gw-2\",\"time\":\"2023-03-27T09:41:15.238108733Z\",\"rssi\":-106,\"loRaSNR\":-16.8,\"location\":{\"latitude\":46.23431,\"longitude\":6.04088,\"altitude\":488}},{\"gatewayID\":\"fcc23dfffe20c913\",\"uplinkID\":\"2ffdd35b-946e-4691-95f2-ff623ae30ceb\",\"name\":\"lora-0024-gw-1\",\"time\":\"2023-03-27T09:41:15.233796178Z\",\"rssi\":-109,\"loRaSNR\":-13.8,\"location\":{\"latitude\":46.22932,\"longitude\":6.05161,\"altitude\":480}},{\"gatewayID\":\"fcc23dfffe20d444\",\"uplinkID\":\"6067ec9e-cdc4-4d87-a599-27e5076e7438\",\"name\":\"lora-3182-gw-2\",\"time\":\"2023-03-27T09:41:15.236088539Z\",\"rssi\":-109,\"loRaSNR\":-4,\"location\":{\"latitude\":46.23539,\"longitude\":6.05547,\"altitude\":480}},{\"gatewayID\":\"fcc23dfffe2095aa\",\"uplinkID\":\"407966fb-b716-4f37-bb79-e63962ed5eb2\",\"name\":\"lora-0361-gw-1\",\"time\":\"2023-03-27T09:41:15.232626011Z\",\"rssi\":-110,\"loRaSNR\":-10.5,\"location\":{\"latitude\":46.23283,\"longitude\":6.0472,\"altitude\":480}},{\"gatewayID\":\"fcc23dfffe207e29\",\"uplinkID\":\"b8ee9af0-5930-4e12-b83f-f37fb9d26cb8\",\"name\":\"lora-3182-gw-1\",\"time\":\"2023-03-27T09:41:15.230423021Z\",\"rssi\":-107,\"loRaSNR\":-15.2,\"location\":{\"latitude\":46.23539,\"longitude\":6.05547,\"altitude\":480}},{\"gatewayID\":\"fcc23dfffe20ef66\",\"uplinkID\":\"6377a34a-7ebb-46dc-b410-3d891ea53984\",\"name\":\"lora-0361-gw-2\",\"time\":\"2023-03-27T09:41:15.230769004Z\",\"rssi\":-106,\"loRaSNR\":-10.8,\"location\":{\"latitude\":46.23283,\"longitude\":6.0472,\"altitude\":480}}],\"txInfo\":{\"frequency\":867300000,\"dr\":1},\"adr\":true,\"fCnt\":69,\"fPort\":1,\"data\":\"NzMuMzAwAAAAFisxOS4w\",\"object\":{\"batery_voltage\":3.3,\"car_status\":false,\"temperature\":19}}";
  private static final String LORA_EN_PARKING_CONTROL_OCCUPATION_STATUS_CHANGE_FRAME =
      "{\"applicationID\":\"26\",\"applicationName\":\"lora-EN-access-control-testing\",\"deviceName\":\"test-cicicom\",\"devEUI\":\"0004a30b00ffa4ac\",\"rxInfo\":[{\"gatewayID\":\"fcc23dfffe2095aa\",\"uplinkID\":\"db897fd8-ff8b-4437-939b-aa7d888d4777\",\"name\":\"lora-0361-gw-1\",\"time\":\"2023-03-30T12:36:11.910536774Z\",\"rssi\":-105,\"loRaSNR\":4,\"location\":{\"latitude\":46.23283,\"longitude\":6.0472,\"altitude\":480}},{\"gatewayID\":\"fcc23dfffe20d444\",\"uplinkID\":\"3d9f62d6-1247-4662-9028-9180709bc57c\",\"name\":\"lora-3182-gw-2\",\"time\":\"2023-03-30T12:36:11.907003989Z\",\"rssi\":-105,\"loRaSNR\":2.5,\"location\":{\"latitude\":46.23539,\"longitude\":6.05547,\"altitude\":480}},{\"gatewayID\":\"fcc23dfffe207e29\",\"uplinkID\":\"16f4cf50-faac-4ce2-a775-6d0b4258c3ce\",\"name\":\"lora-3182-gw-1\",\"time\":\"2023-03-30T12:36:11.908381266Z\",\"rssi\":-106,\"loRaSNR\":-5.2,\"location\":{\"latitude\":46.23539,\"longitude\":6.05547,\"altitude\":480}},{\"gatewayID\":\"fcc23dfffe0f76fb\",\"uplinkID\":\"2d2f67d9-04b6-48ee-8cfa-4115f44518f3\",\"name\":\"lora-0227-gw\",\"time\":\"2023-03-30T12:36:11.915053413Z\",\"rssi\":-106,\"loRaSNR\":-2.2,\"location\":{\"latitude\":46.23431,\"longitude\":6.04088,\"altitude\":488}},{\"gatewayID\":\"fcc23dfffe21f4aa\",\"uplinkID\":\"2710257f-3977-48de-9853-85bf77688a76\",\"name\":\"lora-0227-gw-2\",\"time\":\"2023-03-30T12:36:11.91412595Z\",\"rssi\":-107,\"loRaSNR\":-6.8,\"location\":{\"latitude\":46.23431,\"longitude\":6.04088,\"altitude\":488}}],\"txInfo\":{\"frequency\":867300000,\"dr\":1},\"adr\":true,\"fCnt\":1,\"fPort\":1,\"data\":\"FTMuMzAxAAE3FCsyMi4w\",\"object\":{\"batery_voltage\":3.3,\"car_status\":true,\"id_tag_beacon_rssi\":\"\",\"id_tag_uid\":\"\",\"temperature\":22}}";

  @Override
  protected LoraEnParkingControlDecode createStreamDecodeInstance() {
    return new LoraEnParkingControlDecode(BaseStreamDecodeTest.SOURCE_TOPIC, BaseStreamDecodeTest.SINK_TOPIC);
  }

  @Test
  void LoraEnAccessControlDecodeTopology_CreatesOutputRecord_forStatusReportFrame() {
    pipeRecord(LORA_EN_PARKING_CONTROL_STATUS_REPORT_FRAME);
    final ProducerRecord<String, JsonObject> outputRecord = testDriver.readOutput(SINK_TOPIC, KEY_DESERIALIZER, VALUE_DESERIALIZER);
    assertEquals("vacant", outputRecord.value().get("carStatus").getAsString());
    assertEquals(3.3, outputRecord.value().get("batteryVoltage").getAsDouble());
    assertEquals(19.0, outputRecord.value().get("temperature").getAsDouble());
    assertNotNull(outputRecord.value().get("timestamp"), "Timestamp is null");
  }

  @Test
  void LoraEnAccessControlDecodeTopology_CreatesOutputRecord_forOccupationStatusChangeFrame() {
    pipeRecord(LORA_EN_PARKING_CONTROL_OCCUPATION_STATUS_CHANGE_FRAME);
    final ProducerRecord<String, JsonObject> outputRecord = testDriver.readOutput(SINK_TOPIC, KEY_DESERIALIZER, VALUE_DESERIALIZER);
    assertEquals("occupied", outputRecord.value().get("carStatus").getAsString());
    assertEquals(3.3, outputRecord.value().get("batteryVoltage").getAsDouble());
    assertEquals(22.0, outputRecord.value().get("temperature").getAsDouble());
    assertNotNull(outputRecord.value().get("timestamp"), "Timestamp is null");
    assertNull(outputRecord.value().get("idTagBeaconRssi"));
    assertNull(outputRecord.value().get("idTagUid"));
  }
}
