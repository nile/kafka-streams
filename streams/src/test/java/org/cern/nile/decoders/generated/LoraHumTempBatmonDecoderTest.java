package org.cern.nile.decoders.generated;

import static org.junit.jupiter.api.Assertions.assertEquals;

import com.google.gson.JsonParser;
import java.util.Map;
import org.cern.nile.decoders.KaitaiPacketDecoder;
import org.cern.nile.exceptions.DecodingException;
import org.junit.jupiter.api.Test;

public class LoraHumTempBatmonDecoderTest {

  private static final String LORA_HUM_TEMP_BATMON_MESSAGE_DATA_FRAME =
      "{\"data\":\"DAD//wAA/wcXDEwGPAAThRUAgADxA4YHAEAAQjgKACCkBAAggy4AAAAAAAAAAAAAAAA=\"}";

  @Test
  void decode_DecodesMessageData() throws DecodingException {
    Map<String, Object> packet =
        KaitaiPacketDecoder.decode(JsonParser.parseString(LORA_HUM_TEMP_BATMON_MESSAGE_DATA_FRAME).getAsJsonObject().get("data"),
            LoraHumTempBatmonPacket.class);

    assertEquals(12, packet.get("packetNumber"));
    assertEquals(0, packet.get("statusFlag"));
    assertEquals(255, packet.get("swbuild"));
    assertEquals(255, packet.get("dummyByte"));
    assertEquals(3.2983886718749997, packet.get("mon33"));
    assertEquals(4.987060546875, packet.get("mon5"));
    assertEquals(5.973921199402023, packet.get("vBat"));
    assertEquals(60, packet.get("extwtdCnt"));
    assertEquals(1410323, packet.get("humiSumPeriod"));
    assertEquals(128, packet.get("humiCap0"));
    assertEquals(1009, packet.get("tempGain"));
    assertEquals(1926, packet.get("tempRawValue"));
    assertEquals(1.5517089843749998, packet.get("temperatureAdcConv"));
    assertEquals(34338.43603550926, packet.get("temperatureGainAdcConv"));
    assertEquals(1.9088206144697721, packet.get("temperatureBridgeVoltage"));
  }
}
