package org.cern.nile.streams.kaitai;

import org.cern.nile.decoders.generated.LoraCrackSensorsPacket;

public class LoraCrackSensorsDecode extends LoraDecoderStream<LoraCrackSensorsPacket> {

  public LoraCrackSensorsDecode() {
    super(LoraCrackSensorsPacket.class);
  }

  LoraCrackSensorsDecode(String sourceTopic, String sinkTopic) {
    super(sourceTopic, sinkTopic, LoraCrackSensorsPacket.class);
  }
}
