package org.cern.nile.streams.kaitai;

import java.util.HashMap;
import java.util.Map;
import org.cern.nile.decoders.generated.LoraBatmonPacket;

public class LoraBatmonDecode extends LoraDecoderStream<LoraBatmonPacket> {

  private static final Map<String, String> CUSTOM_METADATA_FIELD_MAP = new HashMap<>();

  static {
    CUSTOM_METADATA_FIELD_MAP.put("adr", "adr");
    CUSTOM_METADATA_FIELD_MAP.put("fCnt", "fCnt");
    CUSTOM_METADATA_FIELD_MAP.put("fPort", "fPort");
  }

  /**
   * Creates a new LoraBatmonDecode.
   */
  public LoraBatmonDecode() {
    super(LoraBatmonPacket.class);
    super.setCustomMetadataFields(CUSTOM_METADATA_FIELD_MAP);
    super.setAddGatewayDetails(true);
    super.setAddTxDetails(true);
  }

  LoraBatmonDecode(String sourceTopic, String sinkTopic) {
    super(sourceTopic, sinkTopic, LoraBatmonPacket.class);
    super.setCustomMetadataFields(CUSTOM_METADATA_FIELD_MAP);
    super.setAddGatewayDetails(true);
    super.setAddTxDetails(true);
  }

}