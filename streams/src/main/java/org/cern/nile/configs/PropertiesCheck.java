package org.cern.nile.configs;

import java.util.Objects;
import java.util.Properties;
import java.util.Set;
import javax.validation.constraints.NotNull;
import org.cern.nile.exceptions.MissingPropertyException;
import org.cern.nile.exceptions.UnknownStreamTypeException;

/**
 * A utility class to validate properties based on the type of stream.
 */
public final class PropertiesCheck {

  private PropertiesCheck(){}

  private static final Set<String> CLIENT_PROPERTIES = StreamConfig.ClientProperties.getValues();
  private static  final Set<String> COMMON_PROPERTIES = StreamConfig.CommonProperties.getValues();
  private static  final Set<String> DECODING_PROPERTIES = StreamConfig.DecodingProperties.getValues();
  private static  final Set<String> ROUTING_PROPERTIES = StreamConfig.RoutingProperties.getValues();
  private static final Set<String> ENRICHMENT_PROPERTIES = StreamConfig.EnrichmentProperties.getValues();

  /**
   * Validates the properties file based on the type of stream.
   *
   * @param properties - properties already loaded from file into java.util.Properties object.
   * @param streamType - type of stream defined in the properties file.
   * @throws MissingPropertyException   if a required property is missing from the properties object.
   * @throws UnknownStreamTypeException if the stream type is unknown.
   */
  public static void validateProperties(@NotNull Properties properties, @NotNull StreamType streamType) {
    Objects.requireNonNull(properties, "Properties object cannot be null");
    Objects.requireNonNull(streamType, "Properties file is missing stream.type property");

    validateRequiredProperties(properties, CLIENT_PROPERTIES);
    validateRequiredProperties(properties, COMMON_PROPERTIES);

    switch (streamType) {
      case DECODING:
        validateRequiredProperties(properties, DECODING_PROPERTIES);
        break;
      case ROUTING:
        validateRequiredProperties(properties, ROUTING_PROPERTIES);
        break;
      case ENRICHMENT:
        validateRequiredProperties(properties, ENRICHMENT_PROPERTIES);
        break;
      default:
        throw new UnknownStreamTypeException(String.format("Stream type unknown: %s.", streamType));
    }
  }

  /**
   * Validates the required properties within the given properties object.
   *
   * @param props       - properties object to check for required properties.
   * @param propsToCheck - set of required property keys.
   * @throws MissingPropertyException if a required property is missing from the properties object.
   */
  private static void validateRequiredProperties(@NotNull Properties props, @NotNull Set<String> propsToCheck) {
    Objects.requireNonNull(props, "Properties object cannot be null");

    for (String prop : propsToCheck) {
      if (!props.containsKey(prop)) {
        throw new MissingPropertyException(String.format("Properties file is missing: %s property.", prop));
      }
    }
  }

}
