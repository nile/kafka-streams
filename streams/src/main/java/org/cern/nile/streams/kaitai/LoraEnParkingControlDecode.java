package org.cern.nile.streams.kaitai;

import org.cern.nile.decoders.generated.LoraEnParkingControlPacket;

public class LoraEnParkingControlDecode extends LoraDecoderStream<LoraEnParkingControlPacket> {

  public LoraEnParkingControlDecode() {
    super(LoraEnParkingControlPacket.class);
  }

  LoraEnParkingControlDecode(String sourceTopic, String sinkTopic) {
    super(sourceTopic, sinkTopic, LoraEnParkingControlPacket.class);
  }
}
