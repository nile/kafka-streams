package org.cern.nile.streams;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import java.io.IOException;
import java.lang.reflect.Type;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Collection;
import java.util.Optional;
import java.util.Properties;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.kstream.Consumed;
import org.cern.nile.configs.StreamConfig;
import org.cern.nile.exceptions.RoutingConfigurationException;
import org.cern.nile.json.JsonSerde;
import org.cern.nile.models.Application;

public class LoraRouting extends AbstractStream {

  private Collection<Application> applications;

  public LoraRouting() {
  }

  LoraRouting(String sourceTopic, String sinkTopic){
    this.sourceTopic = sourceTopic;
    this.sinkTopic = sinkTopic;
  }

  @Override
  public void configure(Properties configs) {
    this.configs = configs;
    String routingConfigPath = configs.getProperty(StreamConfig.RoutingProperties.ROUTING_CONFIG_PATH.getValue());
    try {
      byte[] bytes = Files.readAllBytes(Paths.get(routingConfigPath));
      final Type type = new TypeToken<Collection<Application>>() {}.getType();
      applications = new Gson().fromJson(new String(bytes), type);
    } catch (IOException e) {
      throw new RoutingConfigurationException("Failed to read routing configuration file: " + routingConfigPath, e);
    }
  }

  @Override
  protected void createTopology(StreamsBuilder builder) {
    builder
        .stream(sourceTopic, Consumed.with(Serdes.String(), new JsonSerde()))
        .filter((key, value) -> value != null && value.get("applicationName") != null)
        .to((key, value, recordContext) -> {
          Optional<Application> application = getFirstMatchingApplication(applications, value.get("applicationName").getAsString());
          if (application.isPresent()) {
            return application.get().getTopic().getName();
          } else {
            return configs.getProperty(StreamConfig.RoutingProperties.DLQ_TOPIC.getValue());
          }
        });
  }

  /**
   * Gets the first Application object whose name matches the given applicationName.
   *
   * @param applications    - Collection of Applications
   * @param applicationName - The name of the Application
   * @return - Optional of {@link org.cern.nile.models.Application}
   * @throws IllegalArgumentException If the expression's syntax is invalid
   */
  static Optional<Application> getFirstMatchingApplication(Collection<Application> applications,
                                                                  String applicationName) throws IllegalArgumentException {
    return applications.stream().filter(application -> {
      try {
        return Pattern.compile(application.getName()).matcher(applicationName).matches();
      } catch (PatternSyntaxException e) {
        throw new IllegalArgumentException(e);
      }
    }).findFirst();
  }
}
