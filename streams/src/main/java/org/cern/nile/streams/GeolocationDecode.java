package org.cern.nile.streams;

import com.google.gson.JsonObject;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.time.DateTimeException;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import javax.xml.bind.DatatypeConverter;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.kstream.Consumed;
import org.cern.nile.json.JsonSerde;

public class GeolocationDecode extends AbstractStream {

  protected void createTopology(StreamsBuilder builder) {
    builder
        .stream(sourceTopic, Consumed.with(Serdes.String(), new JsonSerde()))
        .filter(this::filterRecord)
        .transformValues(InjectOffsetTransformer::new)
        .mapValues(this::mapValues)
        .filter(AbstractStream::filterNull)
        .to(sinkTopic);
  }

  @Override
  protected boolean filterRecord(String k, JsonObject v) {
    return v != null && v.get("data") != null && v.get("fPort") != null && v.get("deviceName") != null && v.get("rxInfo") != null;
  }

  private Map<String, Object> mapValues(JsonObject value) {
    final Map<String, Object> map = new HashMap<>();

    decodePayload(value.get("data").getAsString(), map);
    decorateId(value.get("deviceName").getAsString(), map);
    try {
      addTimestamp(value.get("rxInfo").getAsJsonArray(), map);
    } catch (DateTimeException e) {
      logStreamsException(e);
      return Collections.emptyMap();
    }
    lastReadOffset = value.get("offset").getAsLong();
    return map;
  }

  private void decorateId(String devId, Map<String, Object> dict) {
    dict.put("devId", devId);
  }

  private void decodePayload(String payload, Map<String, Object> dict) {
    try {
      final byte[] decode = DatatypeConverter.parseBase64Binary(payload);
      ByteBuffer bb = ByteBuffer.wrap(decode).order(ByteOrder.LITTLE_ENDIAN);
      while (bb.hasRemaining()) {
        final int latitudeRaw = bb.getInt();
        final int longitudeRaw = bb.getInt();
        final int headingRaw = bb.get() & 0xFF;
        final int speed = bb.get() & 0xFF;
        final int voltageRaw = bb.get() & 0xFF;
        dict.put("type", "position");
        dict.put("latitudeDeg", latitudeRaw * 1e-7);
        dict.put("longitudeDeg", longitudeRaw * 1e-7);
        dict.put("inTrip", ((headingRaw & 1) != 0));
        dict.put("fixFailed", ((headingRaw & 2) != 0));
        dict.put("speedKmph", speed);
        dict.put("headingDeg", (headingRaw >> 2) * 5.625);
        dict.put("batV", voltageRaw * 0.025);
        dict.put("manDown", false);
      }
    } catch (IllegalArgumentException e) {
      logStreamsException(e);
    }
  }

}
