package org.cern.nile.configs;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.Set;
import org.junit.jupiter.api.Test;

class StreamConfigTest {

  @Test
  void testClientProperties() {
    Set<String> expectedConfigs = Set.of("source.topic", "kafka.cluster", "client.id", "truststore.location");
    assertEquals(expectedConfigs, StreamConfig.ClientProperties.getValues());
    assertThrows(IllegalArgumentException.class, () -> StreamConfig.ClientProperties.valueOf("unknown.property"));
  }

  @Test
  void testCommonProperties() {
    Set<String> expectedConfigs = Set.of("stream.type", "stream.class");
    assertEquals(expectedConfigs, StreamConfig.CommonProperties.getValues());
    assertThrows(IllegalArgumentException.class, () -> StreamConfig.CommonProperties.valueOf("unknown.property"));
  }

  @Test
  void testDecodingProperties() {
    Set<String> expectedConfigs = Set.of("sink.topic");
    assertEquals(expectedConfigs, StreamConfig.DecodingProperties.getValues());
    assertThrows(IllegalArgumentException.class, () -> StreamConfig.DecodingProperties.valueOf("unknown.property"));
  }

  @Test
  void testRoutingProperties() {
    Set<String> expectedConfigs = Set.of("routing.config.path", "dlq.topic");
    assertEquals(expectedConfigs, StreamConfig.RoutingProperties.getValues());
    assertThrows(IllegalArgumentException.class, () -> StreamConfig.RoutingProperties.valueOf("unknown.property"));
  }

  @Test
  void testEnrichmentProperties() {
    Set<String> expectedConfigs = Set.of("enrichment.config.path", "sink.topic");
    assertEquals(expectedConfigs, StreamConfig.EnrichmentProperties.getValues());
    assertThrows(IllegalArgumentException.class, () -> StreamConfig.EnrichmentProperties.valueOf("unknown.property"));
  }

}
