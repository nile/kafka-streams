package org.cern.nile.streams.kaitai;

import org.cern.nile.decoders.generated.LoraRisingHfPacket;

public class LoraBeItTempRisingHfDecode extends LoraDecoderStream<LoraRisingHfPacket> {

  public LoraBeItTempRisingHfDecode() {
    super(LoraRisingHfPacket.class);
  }

  LoraBeItTempRisingHfDecode(String sourceTopic, String sinkTopic) {
    super(sourceTopic, sinkTopic, LoraRisingHfPacket.class);
  }
}
