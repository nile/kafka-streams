package org.cern.nile.exceptions;

public class RoutingConfigurationException extends RuntimeException {

  public RoutingConfigurationException(String message, Throwable err) {
    super(message, err);
  }

}