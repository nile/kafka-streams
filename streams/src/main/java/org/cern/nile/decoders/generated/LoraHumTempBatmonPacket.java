// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

package org.cern.nile.decoders.generated;

import io.kaitai.struct.ByteBufferKaitaiStream;
import io.kaitai.struct.KaitaiStruct;
import io.kaitai.struct.KaitaiStream;
import java.io.IOException;

public class LoraHumTempBatmonPacket extends KaitaiStruct {
    public static LoraHumTempBatmonPacket fromFile(String fileName) throws IOException {
        return new LoraHumTempBatmonPacket(new ByteBufferKaitaiStream(fileName));
    }

    public LoraHumTempBatmonPacket(KaitaiStream _io) {
        this(_io, null, null);
    }

    public LoraHumTempBatmonPacket(KaitaiStream _io, KaitaiStruct _parent) {
        this(_io, _parent, null);
    }

    public LoraHumTempBatmonPacket(KaitaiStream _io, KaitaiStruct _parent, LoraHumTempBatmonPacket _root) {
        super(_io);
        this._parent = _parent;
        this._root = _root == null ? this : _root;
        _read();
    }
    private void _read() {
        this.packetNumberLowHidden = this._io.readU1();
        this.packetNumberHighHidden = this._io.readU1();
        this.swbuild = this._io.readU1();
        this.dummyByte = this._io.readU1();
        this.statusFlagLowHidden = this._io.readU1();
        this.statusFlagHighHidden = this._io.readU1();
        this.mon33LowHidden = this._io.readU1();
        this.mon33HighHidden = this._io.readU1();
        this.mon5LowHidden = this._io.readU1();
        this.mon5HighHidden = this._io.readU1();
        this.vBatLowHidden = this._io.readU1();
        this.vBatHighHidden = this._io.readU1();
        this.extwtdCntLowHidden = this._io.readU1();
        this.extwtdCntHighHidden = this._io.readU1();
        this.humiSumPeriodRawHidden = this._io.readU4le();
        this.humiCap0LowHidden = this._io.readU1();
        this.humiCap0HighHidden = this._io.readU1();
        this.tempGainLowHidden = this._io.readU1();
        this.tempGainHighHidden = this._io.readU1();
        this.tempRawValueLowHidden = this._io.readU1();
        this.tempRawValueHighHidden = this._io.readU1();
    }
    private Double mon33;
    public Double mon33() {
        if (this.mon33 != null)
            return this.mon33;
        double _tmp = (double) ((((mon33LowHidden() + (mon33HighHidden() * 256)) * 6.6) / 4096));
        this.mon33 = _tmp;
        return this.mon33;
    }
    private Integer humiSumPeriod;
    public Integer humiSumPeriod() {
        if (this.humiSumPeriod != null)
            return this.humiSumPeriod;
        int _tmp = (int) ((((((humiSumPeriodRawHidden() >> 24) * 16777216) + (((humiSumPeriodRawHidden() >> 16) & 255) * 65536)) + (((humiSumPeriodRawHidden() >> 8) & 255) * 256)) + (humiSumPeriodRawHidden() & 255)));
        this.humiSumPeriod = _tmp;
        return this.humiSumPeriod;
    }
    private Double temperatureGainAdcConv;
    public Double temperatureGainAdcConv() {
        if (this.temperatureGainAdcConv != null)
            return this.temperatureGainAdcConv;
        double _tmp = (double) ((tempGain() / 0.029383982396769513));
        this.temperatureGainAdcConv = _tmp;
        return this.temperatureGainAdcConv;
    }
    private Integer extwtdCnt;
    public Integer extwtdCnt() {
        if (this.extwtdCnt != null)
            return this.extwtdCnt;
        int _tmp = (int) ((extwtdCntLowHidden() + (extwtdCntHighHidden() * 256)));
        this.extwtdCnt = _tmp;
        return this.extwtdCnt;
    }
    private Double mon5;
    public Double mon5() {
        if (this.mon5 != null)
            return this.mon5;
        double _tmp = (double) ((((mon5LowHidden() + (mon5HighHidden() * 256)) * 6.6) / 4096));
        this.mon5 = _tmp;
        return this.mon5;
    }
    private Integer statusFlag;
    public Integer statusFlag() {
        if (this.statusFlag != null)
            return this.statusFlag;
        int _tmp = (int) ((statusFlagLowHidden() + (statusFlagHighHidden() * 256)));
        this.statusFlag = _tmp;
        return this.statusFlag;
    }
    private Double temperatureBridgeVoltage;
    public Double temperatureBridgeVoltage() {
        if (this.temperatureBridgeVoltage != null)
            return this.temperatureBridgeVoltage;
        double _tmp = (double) ((tempGainIsZeroHidden() ? 0.0 : ((tempRawValue() * 1.0) / tempGain())));
        this.temperatureBridgeVoltage = _tmp;
        return this.temperatureBridgeVoltage;
    }
    private Integer tempRawValue;
    public Integer tempRawValue() {
        if (this.tempRawValue != null)
            return this.tempRawValue;
        int _tmp = (int) ((tempRawValueLowHidden() + (tempRawValueHighHidden() * 256)));
        this.tempRawValue = _tmp;
        return this.tempRawValue;
    }
    private Double vBat;
    public Double vBat() {
        if (this.vBat != null)
            return this.vBat;
        double _tmp = (double) ((((vBatLowHidden() + (vBatHighHidden() * 256)) * 3.3) / (0.2174 * 4096)));
        this.vBat = _tmp;
        return this.vBat;
    }
    private Integer packetNumber;
    public Integer packetNumber() {
        if (this.packetNumber != null)
            return this.packetNumber;
        int _tmp = (int) ((packetNumberLowHidden() + (packetNumberHighHidden() * 256)));
        this.packetNumber = _tmp;
        return this.packetNumber;
    }
    private Double temperatureAdcConv;
    public Double temperatureAdcConv() {
        if (this.temperatureAdcConv != null)
            return this.temperatureAdcConv;
        double _tmp = (double) (((tempRawValue() * 3.3) / 4096));
        this.temperatureAdcConv = _tmp;
        return this.temperatureAdcConv;
    }
    private Integer tempGain;
    public Integer tempGain() {
        if (this.tempGain != null)
            return this.tempGain;
        int _tmp = (int) ((tempGainLowHidden() + (tempGainHighHidden() * 256)));
        this.tempGain = _tmp;
        return this.tempGain;
    }
    private Boolean tempGainIsZeroHidden;
    public Boolean tempGainIsZeroHidden() {
        if (this.tempGainIsZeroHidden != null)
            return this.tempGainIsZeroHidden;
        boolean _tmp = (boolean) (tempGain() == 0);
        this.tempGainIsZeroHidden = _tmp;
        return this.tempGainIsZeroHidden;
    }
    private Integer humiCap0;
    public Integer humiCap0() {
        if (this.humiCap0 != null)
            return this.humiCap0;
        int _tmp = (int) ((humiCap0LowHidden() + (humiCap0HighHidden() * 256)));
        this.humiCap0 = _tmp;
        return this.humiCap0;
    }
    private int packetNumberLowHidden;
    private int packetNumberHighHidden;
    private int swbuild;
    private int dummyByte;
    private int statusFlagLowHidden;
    private int statusFlagHighHidden;
    private int mon33LowHidden;
    private int mon33HighHidden;
    private int mon5LowHidden;
    private int mon5HighHidden;
    private int vBatLowHidden;
    private int vBatHighHidden;
    private int extwtdCntLowHidden;
    private int extwtdCntHighHidden;
    private long humiSumPeriodRawHidden;
    private int humiCap0LowHidden;
    private int humiCap0HighHidden;
    private int tempGainLowHidden;
    private int tempGainHighHidden;
    private int tempRawValueLowHidden;
    private int tempRawValueHighHidden;
    private LoraHumTempBatmonPacket _root;
    private KaitaiStruct _parent;
    public int packetNumberLowHidden() { return packetNumberLowHidden; }
    public int packetNumberHighHidden() { return packetNumberHighHidden; }
    public int swbuild() { return swbuild; }
    public int dummyByte() { return dummyByte; }
    public int statusFlagLowHidden() { return statusFlagLowHidden; }
    public int statusFlagHighHidden() { return statusFlagHighHidden; }
    public int mon33LowHidden() { return mon33LowHidden; }
    public int mon33HighHidden() { return mon33HighHidden; }
    public int mon5LowHidden() { return mon5LowHidden; }
    public int mon5HighHidden() { return mon5HighHidden; }
    public int vBatLowHidden() { return vBatLowHidden; }
    public int vBatHighHidden() { return vBatHighHidden; }
    public int extwtdCntLowHidden() { return extwtdCntLowHidden; }
    public int extwtdCntHighHidden() { return extwtdCntHighHidden; }
    public long humiSumPeriodRawHidden() { return humiSumPeriodRawHidden; }
    public int humiCap0LowHidden() { return humiCap0LowHidden; }
    public int humiCap0HighHidden() { return humiCap0HighHidden; }
    public int tempGainLowHidden() { return tempGainLowHidden; }
    public int tempGainHighHidden() { return tempGainHighHidden; }
    public int tempRawValueLowHidden() { return tempRawValueLowHidden; }
    public int tempRawValueHighHidden() { return tempRawValueHighHidden; }
    public LoraHumTempBatmonPacket _root() { return _root; }
    public KaitaiStruct _parent() { return _parent; }
}
