package org.cern.nile.streams.kaitai;

import com.google.gson.JsonObject;
import io.kaitai.struct.ByteBufferKaitaiStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.util.*;
import java.util.stream.Collectors;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.kstream.Consumed;
import org.cern.nile.json.JsonSerde;
import org.cern.nile.decoders.generated.LoraContactPacket;
import org.cern.nile.streams.AbstractStream;

// TODO: Refactor if decided to keep in the future
public class LoraContact extends AbstractStream {

  @Override
  protected void createTopology(StreamsBuilder builder) {
    builder
        .stream(sourceTopic, Consumed.with(Serdes.String(), new JsonSerde()))
        .filter(this::filterRecord)
        .transformValues(InjectOffsetTransformer::new)
        .flatMapValues(this::mapValues)
        .filter(LoraContact::filterEmpty)
        .to(sinkTopic);
  }

  @Override
  protected boolean filterRecord(String key, JsonObject value) {
    return value != null && value.get("data") != null;
  }

  private List<Object> mapValues(JsonObject value) {
    List<Object> loraEvents;
    try {
      loraEvents = createLoraEvents(value.get("data").getAsString());
    } catch (RuntimeException e) {
      logStreamsException(e);
      return Collections.emptyList();
    }
    lastReadOffset = value.get("offset").getAsLong();
    return loraEvents;
  }

  private List<Object> createLoraEvents(String payload) throws RuntimeException {
    final LoraContactPacket loraContactPacket = decode(payload);
    final List<Object> encounters = loraContactPacket.encounters().stream().map(er -> {
      try {
        final String timezone = "Europe/Zurich";
        final HashMap<String, Object> records = new HashMap<>();
        final Instant instant = Instant.now();
        final int monthValue = instant.atZone(ZoneId.of(timezone)).getMonthValue();
        final int yearValue = instant.atZone(ZoneId.of(timezone)).getYear();
        final SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy'T'HH:mm");
        sdf.setTimeZone(TimeZone.getTimeZone(timezone));
        final Calendar calendar = Calendar.getInstance();
        calendar.setTimeZone(TimeZone.getTimeZone(timezone));
        // Month is the previous one
        // In case a message comes with the start day bigger than the current day, we know this had to be the previous month
        // E.g message day = 23 current day = 22 => message occurred last month
        if (er.startedAt().day() > instant.atOffset(ZoneOffset.UTC).getDayOfMonth()) {
          final Date parse = sdf.parse(String.format("%s/%s/%sT%d:%d", er.startedAt().day(), monthValue, yearValue, er.startedAt().hour(),
              er.startedAt().minute()));
          calendar.setTime(parse);
          calendar.add(Calendar.MONTH, -1);
        } else { // Month is the current one
          final Date parse = sdf.parse(String.format("%s/%s/%sT%d:%d", er.startedAt().day(), monthValue, yearValue, er.startedAt().hour(),
              er.startedAt().minute()));
          calendar.setTime(parse);
        }

        records.put("my_tag_id", loraContactPacket.myTagId());
        records.put("rf_tag_id", er.rfTagId());
        records.put("num_contacts", er.numContacts());
        records.put("timestamp", calendar.getTimeInMillis());
        return records;
      } catch (ParseException ex) {
        throw new RuntimeException(ex);
      }
    }).collect(Collectors.toList());
    return injectSchema(encounters);
  }

  private List<Object> injectSchema(List<Object> message) {
    final HashMap<String, Object> schema = new HashMap<>();
    final Collection<Map<String, Object>> fields = new ArrayList<>();

    fields.add(Map.of("field", "rf_tag_id", "type", "int64", "optional", "false"));
    fields.add(Map.of("field", "my_tag_id", "type", "int64", "optional", "false"));
    fields.add(Map.of("field", "num_contacts", "type", "int64", "optional", "false"));
    fields.add(Map.of("field", "timestamp", "type", "int64", "optional", "false", "name", "org.apache.kafka.connect.data.Timestamp"));

    schema.put("type", "struct");
    schema.put("fields", fields);

    return message.stream().map(m -> {
      final HashMap<String, Object> obj = new HashMap<>();
      obj.put("schema", schema);
      obj.put("payload", m);
      return obj;
    }).collect(Collectors.toList());
  }

  private LoraContactPacket decode(String payload) throws RuntimeException {
    final byte[] decodedBytes = Base64.getDecoder().decode(payload);
    return new LoraContactPacket(new ByteBufferKaitaiStream(decodedBytes));
  }

}
