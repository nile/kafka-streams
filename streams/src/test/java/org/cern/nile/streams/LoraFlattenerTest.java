package org.cern.nile.streams;

import static org.junit.jupiter.api.Assertions.assertEquals;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.junit.jupiter.api.Test;

public class LoraFlattenerTest {

  private static final String MESSAGE =
      "{\"applicationID\":\"2\",\"applicationName\":\"covid-distancing\","
          + "\"deviceName\":\"covid-distancing75\","
          + "\"devEUI\":\"00a9a20e000001db\",\"rxInfo\":[{\"gatewayID\":\"fcc23dfffe128a6\",\"uplinkID\":\"bda6-631a-43ac-b92f-abca0556\","
          + "\"name\":\"lora-0-gw\",\"time\":\"2020-12-14T08:54:12.377063977Z\",\"rssi\":-106,\"loRaSNR\":-9,\"location\":{\"latitude\":46.23144,"
          + "\"longitude\":6.02,\"altitude\":40}},{\"gatewayID\":\"fcc276fb\",\"uplinkID\":\"e259b-8ca-4d31-800a-9e6be1\","
          + "\"name\":\"lora-7-gw\",\"time\":\"2020-12-14T08:54:12.373357386Z\",\"rssi\":-111,\"loRaSNR\":-14.8,\"location\":{\"latitude\":46.23431,"
          + "\"longitude\":6.88,\"altitude\":48}}],\"txInfo\":{\"frequency\":868100,\"dr\":2},\"adr\":false,\"fCnt\":3,\"fPort\":5,"
          + "\"data\":\"A8943JAHJmcEQBuag==\"}";

  private static final String EXPECTED_REMOVED_DATA_FIELD_OUTPUT =
      "{\"applicationID\":\"2\",\"applicationName\":\"covid-distancing\","
          + "\"deviceName\":\"covid-distancing75\","
          + "\"devEUI\":\"00a9a20e000001db\",\"rxInfo\":[{\"gatewayID\":\"fcc23dfffe128a6\",\"uplinkID\":\"bda6-631a-43ac-b92f-abca0556\","
          + "\"name\":\"lora-0-gw\",\"time\":\"2020-12-14T08:54:12.377063977Z\",\"rssi\":-106,\"loRaSNR\":-9,\"location\":{\"latitude\":46.23144,"
          + "\"longitude\":6.02,\"altitude\":40}},{\"gatewayID\":\"fcc276fb\",\"uplinkID\":\"e259b-8ca-4d31-800a-9e6be1\","
          + "\"name\":\"lora-7-gw\",\"time\":\"2020-12-14T08:54:12.373357386Z\",\"rssi\":-111,\"loRaSNR\":-14.8,\"location\":{\"latitude\":46.23431,"
          + "\"longitude\":6.88,\"altitude\":48}}],\"txInfo\":{\"frequency\":868100,\"dr\":2},\"adr\":false,\"fCnt\":3,\"fPort\":5}";

  private static final String EXPECTED_FLATTENED_OUTPUT =
      "{rxInfo_0_location_latitude=46.23144, rxInfo_0_rssi=-106, fPort=5, "
          + "data=A8943JAHJmcEQBuag==, rxInfo_0_location_altitude=40, fCnt=3, "
          + "deviceName=covid-distancing75, devEUI=00a9a20e000001db, "
          + "rxInfo_0_gatewayID=fcc23dfffe128a6, rxInfo_0_time=2020-12-14T08:54:12.377063977Z, "
          + "rxInfo_1_uplinkID=e259b-8ca-4d31-800a-9e6be1, rxInfo_0_name=lora-0-gw, "
          + "rxInfo_1_location_latitude=46.23431, applicationID=2, "
          + "applicationName=covid-distancing, rxInfo_1_name=lora-7-gw, "
          + "rxInfo_1_location_longitude=6.88, rxInfo_0_loRaSNR=-9, txInfo_dr=2, "
          + "rxInfo_0_location_longitude=6.02, txInfo_frequency=868100, "
          + "rxInfo_1_location_altitude=48, adr=false, rxInfo_1_time=2020-12-14T08:54:12.373357386Z, "
          + "rxInfo_1_rssi=-111, rxInfo_0_uplinkID=bda6-631a-43ac-b92f-abca0556, "
          + "rxInfo_1_gatewayID=fcc276fb, rxInfo_1_loRaSNR=-14.8}";

  // TODO: write unit-tests as written in LoraRoutingTest

  @Test
  void removeDataField_RemovesDataField_ForJsonObject() {
    final LoraFlattener flattener = new LoraFlattener();
    final JsonObject msgAsJson = JsonParser.parseString(MESSAGE).getAsJsonObject();
    final JsonObject output = flattener.removeDataField(msgAsJson);
    final JsonObject expectedOutput =
        JsonParser.parseString(EXPECTED_REMOVED_DATA_FIELD_OUTPUT).getAsJsonObject();
    assertEquals(expectedOutput, output);
  }

  @Test
  void flatten_FlattensData_ForJsonObject() {
    final LoraFlattener flattener = new LoraFlattener();
    final JsonObject msgAsJson = JsonParser.parseString(MESSAGE).getAsJsonObject();
    final Object output = flattener.flatten(msgAsJson);
    assertEquals(EXPECTED_FLATTENED_OUTPUT, output.toString());
  }
}
