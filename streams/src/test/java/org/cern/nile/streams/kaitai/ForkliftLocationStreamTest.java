package org.cern.nile.streams.kaitai;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import com.google.gson.JsonObject;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.cern.nile.streams.AbstractStream;
import org.cern.nile.streams.BaseStreamDecodeTest;
import org.junit.jupiter.api.Test;

public class ForkliftLocationStreamTest extends BaseStreamDecodeTest {

  private static final String START_FRAME_LAT_LON_0 =
      "{\"applicationID\":\"22\",\"applicationName\":\"lora-EN-forklift-location\",\"deviceName\":\"CRLV-00298\",\"deviceProfileName\":\"LoRaWAN_102_A\",\"deviceProfileID\":\"6c516d64-f4ce-4890-a022-4607854d2b8c\",\"devEUI\":\"a0c03f0298f10000\",\"rxInfo\":[{\"gatewayID\":\"fcc23dfffe207e29\",\"uplinkID\":\"1a04f444-5847-46df-9ef8-42f243701453\",\"name\":\"lora-3182-gw-1\",\"time\":\"2024-02-12T06:16:08.370438143Z\",\"rssi\":-109,\"loRaSNR\":-12.5,\"location\":{\"latitude\":46.23539,\"longitude\":6.05547,\"altitude\":480}},{\"gatewayID\":\"fcc23dfffe20eedb\",\"uplinkID\":\"37499642-226b-4d8f-a81b-82342c7b5d4f\",\"name\":\"lora-0188-gw-2\",\"time\":\"2024-02-12T06:16:08.364859087Z\",\"rssi\":-110,\"loRaSNR\":-12.2,\"location\":{\"latitude\":46.2382,\"longitude\":6.03488,\"altitude\":470}},{\"gatewayID\":\"fcc23dfffe0b6b55\",\"uplinkID\":\"fc5183d2-6520-490f-9c38-c77af591c2a5\",\"name\":\"lora-0188-gw-1\",\"time\":\"2024-02-12T06:16:08.36339031Z\",\"rssi\":-112,\"loRaSNR\":-9,\"location\":{\"latitude\":46.2382,\"longitude\":6.03488,\"altitude\":470}},{\"gatewayID\":\"fcc23dfffe20ef66\",\"uplinkID\":\"7ad92811-1869-4f2d-9092-cdba6b9a5d0b\",\"name\":\"lora-0361-gw-2\",\"time\":\"2024-02-12T06:16:08.357216653Z\",\"rssi\":-105,\"loRaSNR\":-6,\"location\":{\"latitude\":46.23283,\"longitude\":6.0472,\"altitude\":480}},{\"gatewayID\":\"0016c001ff10c83d\",\"uplinkID\":\"03323449-f5b5-4bac-be0f-81de1a6c7270\",\"name\":\"LORA-SM18-GW\",\"rssi\":-85,\"loRaSNR\":7.25,\"location\":{\"latitude\":46.23824769448428,\"longitude\":6.048585232776174,\"altitude\":0}},{\"gatewayID\":\"fcc23dfffe21f4aa\",\"uplinkID\":\"8a77f7e2-b0e0-469d-aeb5-cc4687b9cba9\",\"name\":\"lora-0227-gw-2\",\"time\":\"2024-02-12T06:16:08.363801465Z\",\"rssi\":-106,\"loRaSNR\":-11,\"location\":{\"latitude\":46.23431,\"longitude\":6.04088,\"altitude\":488}},{\"gatewayID\":\"fcc23dfffe2095aa\",\"uplinkID\":\"3fac163c-1411-442c-9a8b-b602507c8064\",\"name\":\"lora-0361-gw-1\",\"time\":\"2024-02-12T06:16:08.358720133Z\",\"rssi\":-110,\"loRaSNR\":0.2,\"location\":{\"latitude\":46.23283,\"longitude\":6.0472,\"altitude\":480}},{\"gatewayID\":\"fcc23dfffe20d444\",\"uplinkID\":\"4861028b-6ad5-4295-a372-fbf7503f3cc8\",\"name\":\"lora-3182-gw-2\",\"time\":\"2024-02-12T06:16:08.355865774Z\",\"rssi\":-108,\"loRaSNR\":-2,\"location\":{\"latitude\":46.23539,\"longitude\":6.05547,\"altitude\":480}},{\"gatewayID\":\"fcc23dfffe0f76fb\",\"uplinkID\":\"29574678-d296-41df-b9f5-69048861fb1a\",\"name\":\"lora-0227-gw\",\"time\":\"2024-02-12T06:16:08.363981108Z\",\"rssi\":-106,\"loRaSNR\":-1.8,\"location\":{\"latitude\":46.23431,\"longitude\":6.04088,\"altitude\":488}}],\"txInfo\":{\"frequency\":867300000,\"dr\":0},\"adr\":true,\"fCnt\":183,\"fPort\":1,\"data\":\"hAAAAAAAAAAAnD0P\"}";
  private static final String START_FRAME =
      "{\"applicationID\":\"22\",\"applicationName\":\"lora-EN-forklift-location\",\"deviceName\":\"CH-01237\",\"deviceProfileName\":\"LoRaWAN_102_A\",\"deviceProfileID\":\"6c516d64-f4ce-4890-a022-4607854d2b8c\",\"devEUI\":\"a0c02f1237f10000\",\"rxInfo\":[{\"gatewayID\":\"fcc23dfffe20d13b\",\"uplinkID\":\"9bcfa6d9-4c72-418f-92b0-20f76761c771\",\"name\":\"lora-0024-gw-2\",\"time\":\"2024-02-12T08:30:51.705918612Z\",\"rssi\":-96,\"loRaSNR\":4.5,\"location\":{\"latitude\":46.22932,\"longitude\":6.05161,\"altitude\":480}},{\"gatewayID\":\"fcc23dfffe20def5\",\"uplinkID\":\"77980d57-86f1-48cc-8d75-b0027f9aaeef\",\"name\":\"lora-0501-gw-1\",\"time\":\"2024-02-12T08:30:51.710083655Z\",\"rssi\":-91,\"loRaSNR\":-1.8,\"location\":{\"latitude\":46.23109,\"longitude\":6.05477,\"altitude\":460}},{\"gatewayID\":\"fcc23dfffe21f4aa\",\"uplinkID\":\"39aa4088-4104-426d-9e62-8a3f7ef5ea4d\",\"name\":\"lora-0227-gw-2\",\"time\":\"2024-02-12T08:30:51.719630434Z\",\"rssi\":-100,\"loRaSNR\":-9,\"location\":{\"latitude\":46.23431,\"longitude\":6.04088,\"altitude\":488}},{\"gatewayID\":\"fcc23dfffe20d444\",\"uplinkID\":\"c286259e-267e-4d85-8e70-80dca27a51aa\",\"name\":\"lora-3182-gw-2\",\"time\":\"2024-02-12T08:30:51.709094963Z\",\"rssi\":-111,\"loRaSNR\":1.5,\"location\":{\"latitude\":46.23539,\"longitude\":6.05547,\"altitude\":480}},{\"gatewayID\":\"fcc23dfffe2095aa\",\"uplinkID\":\"4a72c442-25df-4e5c-84c4-e58d207615db\",\"name\":\"lora-0361-gw-1\",\"time\":\"2024-02-12T08:30:51.701737063Z\",\"rssi\":-103,\"loRaSNR\":-0.5,\"location\":{\"latitude\":46.23283,\"longitude\":6.0472,\"altitude\":480}},{\"gatewayID\":\"fcc23dfffe20ef66\",\"uplinkID\":\"eb1441e7-d65f-49d8-b9ea-7480a79d281f\",\"name\":\"lora-0361-gw-2\",\"time\":\"2024-02-12T08:30:51.702155729Z\",\"rssi\":-106,\"loRaSNR\":-5,\"location\":{\"latitude\":46.23283,\"longitude\":6.0472,\"altitude\":480}},{\"gatewayID\":\"fcc23dfffe20c913\",\"uplinkID\":\"ac5e2dee-c712-4801-a7ba-15582a1021fc\",\"name\":\"lora-0024-gw-1\",\"time\":\"2024-02-12T08:30:51.707525565Z\",\"rssi\":-104,\"loRaSNR\":2.8,\"location\":{\"latitude\":46.22932,\"longitude\":6.05161,\"altitude\":480}},{\"gatewayID\":\"fcc23dfffe20cdca\",\"uplinkID\":\"03122f5b-a6e1-4f50-b1eb-c8fba2355b31\",\"name\":\"lora-0501-gw-2\",\"time\":\"2024-02-12T08:30:51.704748681Z\",\"rssi\":-106,\"loRaSNR\":3.2,\"location\":{\"latitude\":46.23109,\"longitude\":6.05477,\"altitude\":460}},{\"gatewayID\":\"fcc23dfffe207e29\",\"uplinkID\":\"e26fa747-18cd-4a31-b7ea-d6979e98433c\",\"name\":\"lora-3182-gw-1\",\"time\":\"2024-02-12T08:30:51.706345616Z\",\"rssi\":-104,\"loRaSNR\":-0.2,\"location\":{\"latitude\":46.23539,\"longitude\":6.05547,\"altitude\":480}},{\"gatewayID\":\"fcc23dfffe0f76fb\",\"uplinkID\":\"bd25602b-47ee-4b28-aebe-6cba98e9dfbc\",\"name\":\"lora-0227-gw\",\"time\":\"2024-02-12T08:30:51.698031903Z\",\"rssi\":-97,\"loRaSNR\":-5.5,\"location\":{\"latitude\":46.23431,\"longitude\":6.04088,\"altitude\":488}}],\"txInfo\":{\"frequency\":868500000,\"dr\":0},\"adr\":true,\"fCnt\":2386,\"fPort\":1,\"data\":\"hABGil8ACT1IlLoF\"}";
  private static final String STOP_FRAME =
      "{\"applicationID\":\"22\",\"applicationName\":\"lora-EN-forklift-location\",\"deviceName\":\"CRLV-00298\",\"deviceProfileName\":\"LoRaWAN_102_A\",\"deviceProfileID\":\"6c516d64-f4ce-4890-a022-4607854d2b8c\",\"devEUI\":\"a0c03f0298f10000\",\"rxInfo\":[{\"gatewayID\":\"fcc23dfffe20ef66\",\"uplinkID\":\"2dc58622-61a1-42a1-aafb-df22aad5d1c3\",\"name\":\"lora-0361-gw-2\",\"time\":\"2024-02-12T06:41:20.817425996Z\",\"rssi\":-107,\"loRaSNR\":-13,\"location\":{\"latitude\":46.23283,\"longitude\":6.0472,\"altitude\":480}},{\"gatewayID\":\"fcc23dfffe21f4aa\",\"uplinkID\":\"cb8dbf2c-8f4d-42c8-9ed0-cd4267f8ba99\",\"name\":\"lora-0227-gw-2\",\"time\":\"2024-02-12T06:41:20.817373339Z\",\"rssi\":-106,\"loRaSNR\":-8.2,\"location\":{\"latitude\":46.23431,\"longitude\":6.04088,\"altitude\":488}},{\"gatewayID\":\"fcc23dfffe20d444\",\"uplinkID\":\"7704576d-e179-4cbb-af31-f907d0741c87\",\"name\":\"lora-3182-gw-2\",\"time\":\"2024-02-12T06:41:20.822602082Z\",\"rssi\":-112,\"loRaSNR\":-9.5,\"location\":{\"latitude\":46.23539,\"longitude\":6.05547,\"altitude\":480}},{\"gatewayID\":\"0016c001ff10c83d\",\"uplinkID\":\"cebf3082-bce3-46f3-8c3e-86b8e4dfc94f\",\"name\":\"LORA-SM18-GW\",\"rssi\":-82,\"loRaSNR\":8.5,\"location\":{\"latitude\":46.23824769448428,\"longitude\":6.048585232776174,\"altitude\":0}},{\"gatewayID\":\"fcc23dfffe0f76fb\",\"uplinkID\":\"533835d0-30fb-4b46-947d-8e1413909cea\",\"name\":\"lora-0227-gw\",\"time\":\"2024-02-12T06:41:20.813498111Z\",\"rssi\":-106,\"loRaSNR\":-8.8,\"location\":{\"latitude\":46.23431,\"longitude\":6.04088,\"altitude\":488}},{\"gatewayID\":\"fcc23dfffe2095aa\",\"uplinkID\":\"a2bc9d72-a84c-4b7b-bd18-f70fb5bac0f6\",\"name\":\"lora-0361-gw-1\",\"time\":\"2024-02-12T06:41:20.812437656Z\",\"rssi\":-112,\"loRaSNR\":-4,\"location\":{\"latitude\":46.23283,\"longitude\":6.0472,\"altitude\":480}}],\"txInfo\":{\"frequency\":868100000,\"dr\":0},\"adr\":true,\"fCnt\":184,\"fPort\":1,\"data\":\"gABGlOUACTxEnLUD\"}";
  private static final String CYCLIC_FRAME =
      "{\"applicationID\":\"22\",\"applicationName\":\"lora-EN-forklift-location\",\"deviceName\":\"CRLV-00298\",\"deviceProfileName\":\"LoRaWAN_102_A\",\"deviceProfileID\":\"6c516d64-f4ce-4890-a022-4607854d2b8c\",\"devEUI\":\"a0c03f0298f10000\",\"rxInfo\":[{\"gatewayID\":\"fcc23dfffe20ef66\",\"uplinkID\":\"2dc58622-61a1-42a1-aafb-df22aad5d1c3\",\"name\":\"lora-0361-gw-2\",\"time\":\"2024-02-12T06:41:20.817425996Z\",\"rssi\":-107,\"loRaSNR\":-13,\"location\":{\"latitude\":46.23283,\"longitude\":6.0472,\"altitude\":480}},{\"gatewayID\":\"fcc23dfffe21f4aa\",\"uplinkID\":\"cb8dbf2c-8f4d-42c8-9ed0-cd4267f8ba99\",\"name\":\"lora-0227-gw-2\",\"time\":\"2024-02-12T06:41:20.817373339Z\",\"rssi\":-106,\"loRaSNR\":-8.2,\"location\":{\"latitude\":46.23431,\"longitude\":6.04088,\"altitude\":488}},{\"gatewayID\":\"fcc23dfffe20d444\",\"uplinkID\":\"7704576d-e179-4cbb-af31-f907d0741c87\",\"name\":\"lora-3182-gw-2\",\"time\":\"2024-02-12T06:41:20.822602082Z\",\"rssi\":-112,\"loRaSNR\":-9.5,\"location\":{\"latitude\":46.23539,\"longitude\":6.05547,\"altitude\":480}},{\"gatewayID\":\"0016c001ff10c83d\",\"uplinkID\":\"cebf3082-bce3-46f3-8c3e-86b8e4dfc94f\",\"name\":\"LORA-SM18-GW\",\"rssi\":-82,\"loRaSNR\":8.5,\"location\":{\"latitude\":46.23824769448428,\"longitude\":6.048585232776174,\"altitude\":0}},{\"gatewayID\":\"fcc23dfffe0f76fb\",\"uplinkID\":\"533835d0-30fb-4b46-947d-8e1413909cea\",\"name\":\"lora-0227-gw\",\"time\":\"2024-02-12T06:41:20.813498111Z\",\"rssi\":-106,\"loRaSNR\":-8.8,\"location\":{\"latitude\":46.23431,\"longitude\":6.04088,\"altitude\":488}},{\"gatewayID\":\"fcc23dfffe2095aa\",\"uplinkID\":\"a2bc9d72-a84c-4b7b-bd18-f70fb5bac0f6\",\"name\":\"lora-0361-gw-1\",\"time\":\"2024-02-12T06:41:20.812437656Z\",\"rssi\":-112,\"loRaSNR\":-4,\"location\":{\"latitude\":46.23283,\"longitude\":6.0472,\"altitude\":480}}],\"txInfo\":{\"frequency\":868100000,\"dr\":0},\"adr\":true,\"fCnt\":184,\"fPort\":1,\"data\":\"EABGjGsACTppnr8H\"}";

  @Override
  protected AbstractStream createStreamDecodeInstance() {
    return new ForkliftLocationStream(BaseStreamDecodeTest.SOURCE_TOPIC, BaseStreamDecodeTest.SINK_TOPIC);
  }

  @Test
  void ForkliftLocationStreamTopology_DoesNotCreateOutputRecord_forStartFrameWithLatLon0() {
    pipeRecord(START_FRAME_LAT_LON_0);
    final ProducerRecord<String, JsonObject> outputRecord = testDriver.readOutput(SINK_TOPIC, KEY_DESERIALIZER, VALUE_DESERIALIZER);
    assertNull(outputRecord, "Output record is not null");
  }

  @Test
  void ForkliftLocationStreamTopology_CreatesOutputRecord_forStartFrame() {
    pipeRecord(START_FRAME);
    final ProducerRecord<String, JsonObject> outputRecord = testDriver.readOutput(SINK_TOPIC, KEY_DESERIALIZER, VALUE_DESERIALIZER);
    assertEquals(3.48, outputRecord.value().get("batteryVoltage").getAsDouble());
    assertEquals(46.22943, outputRecord.value().get("latitude").getAsDouble());
    assertEquals(6.05512, outputRecord.value().get("longitude").getAsDouble());
    assertEquals(18, outputRecord.value().get("temperature").getAsInt());
    assertEquals(1, outputRecord.value().get("accuracy").getAsInt());
    assertEquals(0, outputRecord.value().get("speed").getAsInt());
    assertEquals("CH-01237", outputRecord.value().get("deviceName").getAsString());
    assertEquals("a0c02f1237f10000", outputRecord.value().get("devEUI").getAsString());
    assertEquals("22", outputRecord.value().get("applicationID").getAsString());
    assertEquals("lora-EN-forklift-location", outputRecord.value().get("applicationName").getAsString());
    assertNotNull(outputRecord.value().get("timestamp"), "timestamp is null");
  }

  @Test
  void ForkliftLocationStreamTopology_CreatesOutputRecord_forStopFrame() {
    pipeRecord(STOP_FRAME);
    final ProducerRecord<String, JsonObject> outputRecord = testDriver.readOutput(SINK_TOPIC, KEY_DESERIALIZER, VALUE_DESERIALIZER);
    assertEquals(3.56, outputRecord.value().get("batteryVoltage").getAsDouble());
    assertEquals(46.25637, outputRecord.value().get("latitude").getAsDouble());
    assertEquals(6.05252, outputRecord.value().get("longitude").getAsDouble());
    assertEquals(0, outputRecord.value().get("accuracy").getAsInt());
    assertEquals("CRLV-00298", outputRecord.value().get("deviceName").getAsString());
    assertTrue(outputRecord.value().get("gnssFix").getAsBoolean());
    assertEquals(0, outputRecord.value().get("speed").getAsInt());
    assertEquals("a0c03f0298f10000", outputRecord.value().get("devEUI").getAsString());
    assertTrue(outputRecord.value().get("inputState2").getAsBoolean());
    assertTrue(outputRecord.value().get("inputState1").getAsBoolean());
    assertEquals(13, outputRecord.value().get("temperature").getAsInt());
    assertEquals("StopFrame", outputRecord.value().get("frameType").getAsString());
    assertEquals("22", outputRecord.value().get("applicationID").getAsString());
    assertEquals("lora-EN-forklift-location", outputRecord.value().get("applicationName").getAsString());
    assertNotNull(outputRecord.value().get("timestamp"), "timestamp is null");
  }

  @Test
  void ForkliftLocationStreamTopology_CreatesOutputRecord_forCyclicFrame() {
    pipeRecord(CYCLIC_FRAME);
    final ProducerRecord<String, JsonObject> outputRecord = testDriver.readOutput(SINK_TOPIC, KEY_DESERIALIZER, VALUE_DESERIALIZER);
    assertEquals(3.58, outputRecord.value().get("batteryVoltage").getAsDouble());
    assertEquals(46.23467, outputRecord.value().get("latitude").getAsDouble());
    assertEquals(6.04777, outputRecord.value().get("longitude").getAsDouble());
    assertEquals(1, outputRecord.value().get("accuracy").getAsInt());
    assertEquals("CRLV-00298", outputRecord.value().get("deviceName").getAsString());
    assertTrue(outputRecord.value().get("gnssFix").getAsBoolean());
    assertEquals(0, outputRecord.value().get("speed").getAsInt());
    assertEquals("a0c03f0298f10000", outputRecord.value().get("devEUI").getAsString());
    assertTrue(outputRecord.value().get("inputState2").getAsBoolean());
    assertTrue(outputRecord.value().get("inputState1").getAsBoolean());
    assertEquals(23, outputRecord.value().get("temperature").getAsInt());
    assertEquals("CyclicFrame", outputRecord.value().get("frameType").getAsString());
    assertEquals("22", outputRecord.value().get("applicationID").getAsString());
    assertEquals("lora-EN-forklift-location", outputRecord.value().get("applicationName").getAsString());
    assertNotNull(outputRecord.value().get("timestamp"), "timestamp is null");
  }

}
