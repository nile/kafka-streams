package org.cern.nile;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.*;
import java.time.Duration;
import java.util.Date;
import java.util.*;
import org.apache.kafka.clients.admin.AdminClient;
import org.apache.kafka.clients.admin.AdminClientConfig;
import org.apache.kafka.clients.admin.NewTopic;
import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.apache.kafka.common.serialization.StringSerializer;
import org.cern.nile.schema.connect.SchemaInjector;
import org.cern.nile.schema.db.ConnectSchemaToTableCreationCommandGenerator;
import org.cern.nile.schema.db.DbType;
import org.junit.jupiter.api.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testcontainers.containers.GenericContainer;
import org.testcontainers.containers.KafkaContainer;
import org.testcontainers.containers.Network;
import org.testcontainers.containers.OracleContainer;
import org.testcontainers.containers.wait.strategy.Wait;
import org.testcontainers.shaded.com.fasterxml.jackson.databind.ObjectMapper;
import org.testcontainers.utility.DockerImageName;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class OraclePipelineTestIT {
  private static final Logger LOGGER = LoggerFactory.getLogger(OraclePipelineTestIT.class);

  private static final String CONNECTOR_NAME = "jdbc-source-connector";
  private static final String TOPIC_NAME = "source-topic";
  private static final String TEST_TABLE = "TEST_TABLE";
  private static final int ORACLE_TIMEOUT = 10000;
  private static final int CONSUMER_TIMEOUT = 1000;

  /**
   * Confluent Kafka v6.2.1 is Kafka v2.8.1 with scala 2.13
   */
  private static final String CP_KAFKA_VERSION = "6.2.1";

  private static String jdbcUrl;
  private static Network network;
  private static OracleContainer oracle;
  private static KafkaContainer kafka;
  private static GenericContainer<?> connect;

  private Map<String, Object> data = new HashMap<>();
  private Map<String, Object> dataWithDifferentType = new HashMap<>();

  @BeforeEach
  void setUp() {
    data = new HashMap<>();
    data.put("byte_col", (byte) 1);
    data.put("short_col", (short) 2);
    data.put("int_col", 3);
    data.put("long_col", (long) 4);
    data.put("float_col", 5.0f);
    data.put("double_col", 6.0);
    data.put("boolean_col", true);
    data.put("string_col", "test");
    data.put("timestamp_col", 1501834166000L);
    data.put("date_col", new Date());
    data.put("bytes_col", new byte[]{1, 2, 3});
    dataWithDifferentType = new HashMap<>();
    dataWithDifferentType.put("int_col", 6.5);
  }


  @BeforeAll
  public static void setup() {
    network = Network.newNetwork();
    setUpOracle();
    setUpKafka();
    setUpKafkaConnect();
    createConnector();
  }

  @AfterAll
  public static void tearDown() {
    connect.stop();
    kafka.stop();
    oracle.stop();
  }

  private static void setUpOracle() {
    oracle = new OracleContainer("gvenzl/oracle-xe:latest-faststart")
        .withNetwork(network)
        .withNetworkAliases("oracle");
    oracle.start();
    jdbcUrl = String.format("jdbc:oracle:thin:@oracle:%d/%s", 1521, oracle.getDatabaseName());
  }

  private static void setUpKafka() {
    DockerImageName imageName = DockerImageName.parse(String.format("confluentinc/cp-kafka:%s", CP_KAFKA_VERSION));
    kafka = new KafkaContainer(imageName)
        .withNetwork(network)
        .withNetworkAliases("kafka")
        .withExposedPorts(9092, 9093)
        .waitingFor(Wait.forLogMessage(".*Kafka version.*", 1));
    kafka.start();
  }

  private static void setUpKafkaConnect() {
    DockerImageName imageName = DockerImageName.parse(String.format("confluentinc/cp-kafka-connect:%s", CP_KAFKA_VERSION));
    Path connectorPath = Paths.get("src/test/resources/connectors");
    connect = new GenericContainer<>(imageName)
        .withNetwork(network)
        .dependsOn(kafka, oracle)
        .withEnv("CONNECT_BOOTSTRAP_SERVERS", "kafka:9092")
        .withEnv("CONNECT_REST_ADVERTISED_HOST_NAME", "kafka-connect")
        .withEnv("CONNECT_REST_PORT", "28082")
        .withEnv("CONNECT_GROUP_ID", "quickstart")
        .withEnv("CONNECT_CONFIG_STORAGE_TOPIC", "connect-configs")
        .withEnv("CONNECT_OFFSET_STORAGE_TOPIC", "connect-offsets")
        .withEnv("CONNECT_STATUS_STORAGE_TOPIC", "connect-status")
        .withEnv("SQL_QUOTE_IDENTIFIERS_CONFIG", "false")
        .withEnv("CONNECT_KEY_CONVERTER", "org.apache.kafka.connect.json.JsonConverter")
        .withEnv("CONNECT_VALUE_CONVERTER", "org.apache.kafka.connect.json.JsonConverter")
        .withEnv("CONNECT_VALUE_CONVERTER_SCHEMAS_ENABLE", "true")
        .withEnv("CONNECT_PLUGIN_PATH", "/usr/share/java/kafka")
        .withEnv("KAFKA_JMX_OPTS",
            "-Dcom.sun.management.jmxremote -Dcom.sun.management.jmxremote.authenticate=false -Dcom.sun.management.jmxremote.ssl=false -Dcom.sun.management.jmxremote.rmi.port=1099 -Djava.rmi.server.hostname=localhost")
        .withFileSystemBind(connectorPath.toString(), "/usr/share/java/kafka/jdbc-connector")
        .withExposedPorts(28082, 1099)  // Expose the JMX port
        .waitingFor(Wait.forLogMessage(".*Kafka Connect started.*", 1));

    KafkaTopicCreator.createTopic(kafka.getBootstrapServers(), "connect-offsets", 1, (short) 1);
    KafkaTopicCreator.createTopic(kafka.getBootstrapServers(), "connect-configs", 1, (short) 1);
    KafkaTopicCreator.createTopic(kafka.getBootstrapServers(), "connect-status", 1, (short) 1);

    connect.start();
  }


  private static void createConnector() {
    final int mappedPort = connect.getMappedPort(28082);

    Map<String, String> config = new HashMap<>();
    config.put("connector.class", "io.aiven.connect.jdbc.JdbcSinkConnector");
    config.put("table.name.format", TEST_TABLE);
    config.put("connection.password", oracle.getPassword());
    config.put("tasks.max", "1");
    config.put("auto.create", "false");
    config.put("auto.evolve", "false");
    config.put("topics", TOPIC_NAME);
    config.put("connection.user", oracle.getUsername());
    config.put("sql.quote.identifiers", "false");
    config.put("value.converter.schemas.enable", "true");
    config.put("connection.url", jdbcUrl);
    config.put("value.converter", "org.apache.kafka.connect.json.JsonConverter");
    config.put("key.converter", "org.apache.kafka.connect.storage.StringConverter");

    Map<String, Object> requestBody = new HashMap<>();
    requestBody.put("name", CONNECTOR_NAME);
    requestBody.put("config", config);

    ObjectMapper objectMapper = new ObjectMapper();

    try {
      String json = objectMapper.writeValueAsString(requestBody);
      HttpClient httpClient = HttpClient.newHttpClient();
      HttpRequest request = HttpRequest.newBuilder()
          .uri(URI.create("http://localhost:" + mappedPort + "/connectors"))
          .header("Content-Type", "application/json")
          .POST(HttpRequest.BodyPublishers.ofString(json))
          .build();

      HttpResponse<String> response = httpClient.send(request, HttpResponse.BodyHandlers.ofString());

      if (response.statusCode() != 201) {
        String logs = connect.getLogs();
        LOGGER.error(logs);
        throw new RuntimeException("Failed to create the connector, response code: " + response.statusCode());
      }
    } catch (Exception e) {
      String logs = connect.getLogs();
      LOGGER.error(logs, e);
      throw new RuntimeException("Failed to create the connector", e);
    }
  }

  private void produceRecord(String message) {
    Properties properties = new Properties();
    LOGGER.info(String.format("Kafka bootstrapServers: %s", kafka.getBootstrapServers()));
    properties.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, kafka.getBootstrapServers());
    properties.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
    properties.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
    KafkaProducer<String, String> producer = new KafkaProducer<>(properties);

    final ProducerRecord<String, String> record = new ProducerRecord<>(TOPIC_NAME, message);

    producer.send(record, (metadata, exception) -> {
      LOGGER.info("Trying to write to topic " + record.topic());
      if (exception != null) {
        LOGGER.error("Error producing to topic", exception);
        fail("Failed to produce message");
      } else {
        LOGGER.info("Message produced successfully");
        LOGGER.info(String.valueOf(metadata.offset()));
      }
    });
    producer.flush();
    producer.close();
  }

  private boolean hasMessagesInTopic() {
    Properties props = new Properties();
    props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, kafka.getBootstrapServers());
    props.put(ConsumerConfig.GROUP_ID_CONFIG, "test-group");
    props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());
    props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());
    props.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");
    props.put(ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG, "false");

    Consumer<String, String> consumer = new KafkaConsumer<>(props);
    consumer.subscribe(Collections.singletonList(TOPIC_NAME));

    ConsumerRecords<String, String> records = consumer.poll(Duration.ofMillis(CONSUMER_TIMEOUT));
    consumer.close();

    return !records.isEmpty();
  }

  private static class KafkaTopicCreator {

    public static void createTopic(String bootstrapServers, String topicName, int partitions, short replicationFactor) {
      Properties config = new Properties();
      config.put(AdminClientConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapServers);

      try (AdminClient admin = AdminClient.create(config)) {
        NewTopic newTopic = new NewTopic(topicName, partitions, replicationFactor);
        Map<String, String> configs = new HashMap<>();
        configs.put("cleanup.policy", "compact");
        newTopic.configs(configs);
        admin.createTopics(Collections.singletonList(newTopic)).all().get();
      } catch (Exception e) {
        String logs = kafka.getLogs();
        LOGGER.error(logs, e);
        throw new RuntimeException("Failed to create topic " + topicName, e);
      }
    }
  }

  private static boolean tableExists() throws SQLException {
    try (Connection connection = DriverManager.getConnection(oracle.getJdbcUrl(), oracle.getUsername(), oracle.getPassword())) {
      ResultSet tableExists = connection.getMetaData().getTables(null, null, TEST_TABLE, null);
      if (tableExists.next()) {
        return true;
      }
    }
    return false;
  }


  private static List<Map<String, Object>> getRecordsFromOracle() throws SQLException, InterruptedException {
    List<Map<String, Object>> records = new ArrayList<>();
    long endTimeMillis = System.currentTimeMillis() + ORACLE_TIMEOUT;

    while (System.currentTimeMillis() < endTimeMillis && records.isEmpty()) {
      try (Connection connection = DriverManager.getConnection(oracle.getJdbcUrl(), oracle.getUsername(), oracle.getPassword());
           Statement statement = connection.createStatement();
           ResultSet resultSet = statement.executeQuery("SELECT * FROM " + TEST_TABLE)) {

        ResultSetMetaData metaData = resultSet.getMetaData();
        int columnCount = metaData.getColumnCount();

        while (resultSet.next()) {
          Map<String, Object> record = new HashMap<>();
          for (int i = 1; i <= columnCount; i++) {
            String columnName = metaData.getColumnName(i);
            Object columnValue = resultSet.getObject(i);
            record.put(columnName, columnValue);
          }
          records.add(record);
        }
      }

      if (records.isEmpty()) {
        Thread.sleep(500);
      } else {
        return records;
      }
    }
    return records;
  }

  private void runTableCreationCommand(String command) throws SQLException {
    try (Connection connection = DriverManager.getConnection(oracle.getJdbcUrl(), oracle.getUsername(), oracle.getPassword());
         Statement statement = connection.createStatement()) {
      statement.execute(command);
    }
  }

  @Test
  @Order(1)
  public void connectorPipeline_ValidProcessing_WhenTableExistsAndRecordsProduced() throws Exception {
    String oracleTableCreationCommand = ConnectSchemaToTableCreationCommandGenerator.getTableCreationCommand(data, DbType.ORACLE, TEST_TABLE);
    runTableCreationCommand(oracleTableCreationCommand);
    if (!tableExists()) {
      fail("Table was not created");
    }

    final Map<String, Object> messageWithSchema = SchemaInjector.inject(data);

    ObjectMapper objectMapper = new ObjectMapper();

    produceRecord(objectMapper.writeValueAsString(messageWithSchema));
    assertTrue(hasMessagesInTopic(), "No messages in topic");

    List<Map<String, Object>> records = getRecordsFromOracle();
    if (records.size() > 0) {
      return;
    }

    fail("Expected at least one row in the table");

    Map<String, Object> record = records.get(0);

    assertEquals(data.get("byte_col"), record.get("byte_col"));
    assertEquals(data.get("short_col"), record.get("short_col"));
    assertEquals(data.get("int_col"), record.get("int_col"));
    assertEquals(data.get("long_col"), record.get("long_col"));
    assertEquals(data.get("float_col"), record.get("float_col"));
    assertEquals(data.get("double_col"), record.get("double_col"));
    assertEquals(data.get("boolean_col"), record.get("boolean_col"));
    assertEquals(data.get("string_col"), record.get("string_col"));
    assertEquals(data.get("timestamp_col"), record.get("timestamp_col"));
    assertEquals(data.get("date_col"), record.get("date_col"));
    Assertions.assertArrayEquals((byte[]) data.get("bytes_col"), (byte[]) record.get("bytes_col"));

  }

  @Test
  @Order(2)
  public void connectorPipeline_InvalidProcessing_WhenTableExistsAndRecordsWithDifferentSchemasProduced() throws Exception {
    final Map<String, Object> messageWithSchema = SchemaInjector.inject(data);
    final Map<String, Object> messageWithDifferentSchema = SchemaInjector.inject(dataWithDifferentType);

    ObjectMapper objectMapper = new ObjectMapper();
    produceRecord(objectMapper.writeValueAsString(messageWithSchema));

    assertTrue(hasMessagesInTopic(), "No messages in topic");

    Thread.sleep(5000);

    List<Map<String, Object>> records = getRecordsFromOracle();
    if (records.size() == 0) {
      fail("Expected at least one row in the table");
    }

    produceRecord(objectMapper.writeValueAsString(messageWithDifferentSchema));
    assertTrue(hasMessagesInTopic(), "No messages in topic");

    Thread.sleep(5000);
    records = getRecordsFromOracle();
    System.out.println(records);
    if (records.size() != 1) {
      fail("Expected for Oracle to fail.");
    }
  }

}
