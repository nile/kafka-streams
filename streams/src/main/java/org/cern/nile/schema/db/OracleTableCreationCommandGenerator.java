package org.cern.nile.schema.db;

import java.util.List;
import java.util.Map;

/**
 * Generates an Oracle database schema creation command from a Connect schema.
 */
public class OracleTableCreationCommandGenerator {

  static String generateOracleCreationCommand(List<Map<String, Object>> connectSchemaFields, String tableName) {
    StringBuilder oracleCreationCommand = new StringBuilder();
    appendTableCreationStart(oracleCreationCommand, tableName);

    for (Map<String, Object> field : connectSchemaFields) {
      appendField(oracleCreationCommand, field);
    }

    removeLastComma(oracleCreationCommand);
    appendTableCreationEnd(oracleCreationCommand);

    return oracleCreationCommand.toString();
  }

  private static void appendTableCreationStart(StringBuilder sb, String tableName) {
    sb.append("CREATE TABLE ").append(tableName).append(" (\n");
  }

  private static void appendField(StringBuilder sb, Map<String, Object> field) {
    String fieldName = (String) field.get("field");
    String fieldType = (String) field.get("type");
    String name = (String) field.getOrDefault("name", "");
    String oracleDataType = mapFieldTypeToOracleType(fieldType, name);
    sb.append(fieldName).append(" ").append(oracleDataType).append(",\n");
  }

  private static void removeLastComma(StringBuilder sb) {
    sb.setLength(sb.length() - 2);
  }

  private static void appendTableCreationEnd(StringBuilder sb) {
    sb.append("\n);");
  }

  private static String mapFieldTypeToOracleType(String fieldType, String name) {
    if (name.equals("org.apache.kafka.connect.data.Timestamp")) {
      return "TIMESTAMP";
    } else if (name.equals("org.apache.kafka.connect.data.Date")) {
      return "DATE";
    } else {
      switch (fieldType) {
        case "int8":
          return "NUMBER(3)";
        case "int16":
          return "NUMBER(5)";
        case "int32":
          return "NUMBER(10)";
        case "int64":
          return "NUMBER(19)";
        case "float":
          return "FLOAT";
        case "double":
          return "FLOAT(126)";
        case "string":
          return "VARCHAR2(510)";
        case "boolean":
          return "NUMBER(1)";
        case "date":
          return "DATE";
        case "timestamp":
          return "TIMESTAMP";
        case "bytes":
          return "BLOB";
        default:
          throw new IllegalArgumentException("Unsupported field type: " + fieldType);
      }
    }
  }
}
