package org.cern.nile.streams;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import java.time.DateTimeException;
import java.time.Instant;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.CountDownLatch;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.Topology;
import org.apache.kafka.streams.kstream.ValueTransformer;
import org.apache.kafka.streams.processor.ProcessorContext;
import org.cern.nile.clients.KafkaStreamsClient;
import org.cern.nile.configs.StreamConfig;
import org.cern.nile.probes.Health;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class AbstractStream implements Streaming {

  protected static final Logger LOGGER = LoggerFactory.getLogger(AbstractStream.class);

  Properties configs;
  KafkaStreams streams;
  protected String sourceTopic;
  protected String sinkTopic;
  protected long lastReadOffset = -2;
  private Health health;
  private CountDownLatch latch;

  @Override
  public void configure(Properties configs) {
    this.configs = configs;
  }

  @Override
  public void stream(KafkaStreamsClient kafkaStreamsClient) {
    init(kafkaStreamsClient);
    Runtime.getRuntime().addShutdownHook(new Thread(this::shutDown, "streams-shutdown-hook"));
    start();
    System.exit(0);
  }

  protected static void addTimestamp(JsonArray gatewayInfo, Map<String, Object> map) throws DateTimeException {
    final String timestampKey = "timestamp";
    final String timeKey = "time";

    Instant mostRecentTimestamp = null;
    for (JsonElement element : gatewayInfo) {
      if (element.isJsonObject()) {
        JsonObject entry = element.getAsJsonObject();
        if (entry.has(timeKey)) {
          Instant currentTimestamp = Instant.parse(entry.get(timeKey).getAsString());
          if (mostRecentTimestamp == null || currentTimestamp.isAfter(mostRecentTimestamp)) {
            mostRecentTimestamp = currentTimestamp;
          }
        }
      }
    }

    if (mostRecentTimestamp != null) {
      map.put(timestampKey, mostRecentTimestamp.toEpochMilli());
    } else {
      LOGGER.warn(String.format("No '%s' field found in gateway info, adding current timestamp.", timeKey));
      map.put(timestampKey, Instant.now().toEpochMilli());
    }
  }

  protected static boolean filterNull(String k, Object v) {
    return v != null;
  }

  protected static boolean filterEmpty(String k, Object v) {
    return !(v instanceof List && ((List<?>) v).isEmpty());
  }

  protected boolean filterRecord(String k, JsonObject v) {
    return v != null && v.get("applicationID") != null && v.get("applicationName") != null && v.get("deviceName") != null && v.get("devEUI") != null
        && v.get("data") != null;
  }

  protected void logStreamsException(Exception e) {
    LOGGER.warn(String.format("Error reading from topic %s. Last read offset %s:", sourceTopic, lastReadOffset), e);
    if (streams != null) {
      LOGGER.info(String.format("Streams state is: %s", streams.state().toString()));
    }
  }

  protected abstract void createTopology(StreamsBuilder builder);

  private void init(KafkaStreamsClient kafkaStreamsClient) {
    final StreamsBuilder builder = new StreamsBuilder();
    sourceTopic = configs.getProperty(StreamConfig.ClientProperties.SOURCE_TOPIC.getValue());
    sinkTopic = configs.getProperty(StreamConfig.DecodingProperties.SINK_TOPIC.getValue());
    createTopology(builder);
    final Topology topology = builder.build();
    streams = kafkaStreamsClient.create(topology);
    health = new Health(streams);
    latch = new CountDownLatch(1);
  }

  private void start() {
    LOGGER.info("Starting streams...");
    try {
      streams.start();
      health.start();
      latch.await();
    } catch (Exception e) {
      LOGGER.error("Could not start streams.", e);
      System.exit(1);
    }
  }

  private void shutDown() {
    LOGGER.info("Shutting down streams...");
    streams.close();
    health.stop();
    latch.countDown();
  }

  public static class InjectOffsetTransformer implements ValueTransformer<JsonObject, JsonObject> {

    private ProcessorContext context;

    @Override
    public void init(ProcessorContext context) {
      this.context = context;
    }

    @Override
    public JsonObject transform(JsonObject value) {
      value.addProperty("offset", context.offset());
      return value;
    }

    @Override
    public void close() {
    }

  }

}
