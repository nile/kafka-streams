package org.cern.nile.configs;

import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.Properties;
import org.cern.nile.configs.StreamConfig.ClientProperties;
import org.cern.nile.configs.StreamConfig.CommonProperties;
import org.cern.nile.configs.StreamConfig.DecodingProperties;
import org.cern.nile.configs.StreamConfig.EnrichmentProperties;
import org.cern.nile.configs.StreamConfig.RoutingProperties;
import org.junit.jupiter.api.Test;

class PropertiesCheckTest {

  @Test
  void validateProperties_ThrowsRuntimeException_forIllegalArguments() {
    final Properties properties = new Properties();

    assertThrows(RuntimeException.class,
        () -> PropertiesCheck.validateProperties(null, StreamType.ROUTING),
        "Properties object cannot be null");

    assertThrows(RuntimeException.class,
        () -> PropertiesCheck.validateProperties(properties, null),
        "Properties file is missing stream.type property");
  }

  @Test
  void validateProperties_PassesValidation_forDecoding() {
    final Properties properties = new Properties();
    initClientAndCommonProperties(properties);
    properties.put(DecodingProperties.SINK_TOPIC.getValue(), "");
    PropertiesCheck.validateProperties(properties, StreamType.DECODING);
  }

  @Test
  void validateProperties_PassesValidation_forRouting() {
    final Properties properties = new Properties();
    initClientAndCommonProperties(properties);
    properties.put(RoutingProperties.ROUTING_CONFIG_PATH.getValue(), "");
    properties.put(RoutingProperties.DLQ_TOPIC.getValue(), "");
    PropertiesCheck.validateProperties(properties, StreamType.ROUTING);
  }

  @Test
  void validateProperties_PassesValidation_forEnrichment() {
    final Properties properties = new Properties();
    initClientAndCommonProperties(properties);
    properties.put(EnrichmentProperties.ENRICHMENT_CONFIG_PATH.getValue(), "");
    properties.put(EnrichmentProperties.SINK_TOPIC.getValue(), "");
    PropertiesCheck.validateProperties(properties, StreamType.ENRICHMENT);
  }

  private void initClientAndCommonProperties(Properties properties) {
    properties.put(ClientProperties.CLIENT_ID.getValue(), "");
    properties.put(ClientProperties.KAFKA_CLUSTER.getValue(), "");
    properties.put(ClientProperties.SOURCE_TOPIC.getValue(), "");
    properties.put(ClientProperties.TRUSTSTORE_LOCATION.getValue(), "");
    properties.put(CommonProperties.STREAM_CLASS.getValue(), "");
    properties.put(CommonProperties.STREAM_TYPE.getValue(), "");
  }
}
