meta:
  id: lora_en_parking_control_packet
  title: Lora EN Parking Control Packet

doc-ref: https://cernbox.cern.ch/s/XCEjcjpMMtGcvGR

seq:
  - id: frame_code
    type: u1
    doc: Frame code
  - id: payload
    type:
      switch-on: frame_code
      cases:
        55: status_report
        21: occupation_status_change

types:
  car_status:
    seq:
      - id: car_status_char_hidden
        type: str
        size: 1
        encoding: ASCII
    instances:
      car_status:
        value: 'car_status_char_hidden == "0" ? "vacant" : "occupied"'

  temperature:
    seq:
      - id: temperature_sign_hidden
        type: str
        size: 1
        encoding: ASCII
      - id: temperature_value_hidden
        type: str
        size: 4
        encoding: ASCII
    instances:
      temperature:
        value: temperature_sign_hidden + temperature_value_hidden

  status_report:
    seq:
      - id: battery_voltage
        type: str
        size: 4
        encoding: ASCII
        doc: Battery voltage as a string (e.g., "3.55")
      - id: car_status_data
        type: car_status
      - id: factory_used_hidden
        size: 4
        doc: Factory used bytes
      - id: temperature_data
        type: temperature

  occupation_status_change:
    seq:
      - id: battery_voltage
        type: str
        size: 4
        encoding: ASCII
        doc: Battery voltage as a string (e.g., "3.55")
      - id: car_status_data
        type: car_status
      - id: factory_used_hidden
        size: 4
        doc: Factory used bytes
      - id: temperature_data
        type: temperature
      - id: id_tag_uid
        type: str
        size: 12
        encoding: ASCII
        doc: ID Tag UID as a string (e.g., "AC233F6E224DD25")
        if: "_io.size - _io.pos >= 12"
      - id: id_tag_beacon_rssi
        type: str
        size: 2
        encoding: ASCII
        doc: ID Tag Beacon RSSI as a string (e.g., "54")
        if: "_io.size - _io.pos >= 2"
