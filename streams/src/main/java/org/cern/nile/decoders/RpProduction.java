package org.cern.nile.decoders;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.text.ParseException;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import org.cern.nile.exceptions.DecodingException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

// TODO: Check how to convert this into Kaitai
public class RpProduction {
  private static final Logger LOGGER = LoggerFactory.getLogger(RpProduction.class.getName());
  private static final double VOLTAGE_CONSTANT = 0.0008056640625;
  private static final double VOLTAGE_CONSTANT2 = 0.00185302734375;

  /**
   * Decodes payload and adds properties to the returning  objects.
   *
   * @param message             - incoming message
   * @param lastDeviceTimestamp - devices' last timestamp
   * @return - Map of properties of the decoded payload
   */
  public static Collection<Map<String, Object>> decode(JsonObject message, Long lastDeviceTimestamp)
      throws IOException, ParseException, DecodingException {
    // Payload vars
    final String payloadRaw = message.get("data").getAsString();
    final byte[] payloadDecoded = Base64.getDecoder().decode(payloadRaw);
    final DataInputStream inputStream = new DataInputStream(new ByteArrayInputStream(payloadDecoded));

    // Timestamp
    final long currTimestamp = findTimestamp(message);
    if (lastDeviceTimestamp == null) {
      lastDeviceTimestamp = currTimestamp;
    }

    final Collection<Map<String, Object>> output = new ArrayList<>();
    // LoRa transmission configuration parameters
    int transmissionPeriod = 3600; // 1 hour
    float transmissionMargin = 0.15f;  // 540s Due to different factors LoRa transmission timing can vary with some tolerance.

    if (inputStream.available() > 0) {
      final Map<String, Object> payload = decodePayload(inputStream);
      final int packageNum = (int) payload.get("package_num");
      if (Math.abs(currTimestamp - lastDeviceTimestamp) > ((2 * transmissionPeriod + transmissionMargin * transmissionPeriod) * 1000)) {
        // 2h + 540s
        if (packageNum >= 3) {
          output.add(getSpecificPayloadData(0, payload, message, transmissionPeriod, currTimestamp));
          output.add(getSpecificPayloadData(1, payload, message, transmissionPeriod, currTimestamp));
          output.add(getSpecificPayloadData(2, payload, message, transmissionPeriod, currTimestamp));
        } else if (packageNum == 2) {
          // Account for cases when the device was having problems for more than 2h15min
          output.add(getSpecificPayloadData(0, payload, message, transmissionPeriod, currTimestamp));
          output.add(getSpecificPayloadData(1, payload, message, transmissionPeriod, currTimestamp));
        } else if (packageNum == 1) {
          // Account for cases when the device was having problems for more than 2h15min
          output.add(getSpecificPayloadData(0, payload, message, transmissionPeriod, currTimestamp));
        } else {
          throw new DecodingException(String.format("Message %s had data loss of %d hours but packageNum was %d", message, 2, packageNum));
        }
      } else if (Math.abs(currTimestamp - lastDeviceTimestamp) > ((transmissionPeriod + transmissionMargin * transmissionPeriod) * 1000)) {
        // 1h + 540s
        if (packageNum >= 2) {
          output.add(getSpecificPayloadData(0, payload, message, transmissionPeriod, currTimestamp));
          output.add(getSpecificPayloadData(1, payload, message, transmissionPeriod, currTimestamp));
        } else {
          throw new DecodingException(String.format("Message %s had data loss of %d hour but packageNum was %d", message, 1, packageNum));
        }
      } else {
        if (packageNum >= 1) {
          output.add(getSpecificPayloadData(0, payload, message, transmissionPeriod, currTimestamp));
        } else {
          throw new DecodingException(String.format("Message %s is without data loss but packageNum was %d", message, packageNum));
        }
      }
    }
    return output;
  }

  private static long findTimestamp(JsonObject message) throws NullPointerException {
    final JsonArray metadata = message.get("rxInfo").getAsJsonArray();
    boolean timestampFound = false;
    long timestamp = 0;
    for (int i = 0; i < metadata.size(); i++) {
      final JsonObject entry = metadata.get(i).getAsJsonObject();
      if (entry.get("time") != null) {
        timestamp = Instant.parse(entry.get("time").getAsString()).toEpochMilli();
        timestampFound = true;
        break;
      }
    }
    if (!timestampFound) {
      throw new NullPointerException(String.format("Current timestamp was not present in the message: %s; Dropping the message", metadata));
    }
    return timestamp;
  }

  private static Map<String, Object> getSpecificPayloadData(int dataLossCase, Map<String, Object> payloadData, JsonObject message,
                                                            int transmissionPeriod, long currTimestamp) {
    final Map<String, Object> toReturn = new HashMap<>();
    final int alarm = (int) payloadData.get("alarm");
    toReturn.put("dev_ID", payloadData.get("dev_ID"));
    toReturn.put("package_num", payloadData.get("package_num"));

    switch (dataLossCase) {
      case 0:
        toReturn.put("redundant_alg", 0);
        toReturn.put("package_num", payloadData.get("package_num"));
        toReturn.put("alarm", payloadData.get("alarm"));
        toReturn.put("alarm_counts", payloadData.get("alarm_counts"));
        toReturn.put("counts", payloadData.get("counts"));
        toReturn.put("counts_normalized", normalizeCounts((int) payloadData.get("counts"), (int) payloadData.get("checkingTime")));
        toReturn.put("checking_time", payloadData.get("checkingTime"));
        toReturn.put("shock_counts", payloadData.get("shockCounts"));
        toReturn.put("timestamp", currTimestamp);
        toReturn.put("exwdtc", payloadData.get("exwdtc"));
        break;
      case 1:
        toReturn.put("redundant_alg", 1);
        toReturn.put("package_num", (int) payloadData.get("package_num") - 1);
        toReturn.put("alarm", 0);
        toReturn.put("counts", payloadData.get("counts1hAgo"));
        toReturn.put("counts_normalized", normalizeCounts((int) payloadData.get("counts1hAgo"), (int) payloadData.get("checkingTime1hAgo")));
        toReturn.put("checking_time", payloadData.get("checkingTime1hAgo"));
        toReturn.put("shock_counts", 0);
        toReturn.put("exwdtc", (int) payloadData.get("exwdtc") - 60);
        if (alarm != 0 && alarm != 1) {
          switch (alarm) {
            case 2:
            case 5:
              // Insert 1 hour ago because alarm has not been triggered or alarm has been triggered in last period.
              toReturn.put("timestamp", currTimestamp - (transmissionPeriod * 1000L));
              toReturn.put("exwdtc", (int) payloadData.get("exwdtc") - 60);
              toReturn.put("alarm", 2);
              break;
            case 3:
              // Insert 20 minutes ago because alarm has been triggered now in the first period of checking.
              toReturn.put("timestamp", currTimestamp - (transmissionPeriod / 3 * 1000L));
              toReturn.put("exwdtc", (int) payloadData.get("exwdtc") - 20);
              toReturn.put("alarm", 2);
              break;
            case 4:
              // Insert 40 minutes ago because alarm has been triggered now in the first period of checking.
              toReturn.put("timestamp", currTimestamp - (((long) transmissionPeriod / 3) * 2 * 1000));
              toReturn.put("exwdtc", (int) payloadData.get("exwdtc") - 40);
              toReturn.put("alarm", 2);
              break;
            default:
              // We should abort this message insertion here because alarm should never be higher than 4
              toReturn.clear();
              return toReturn;
          }
        } else {
          toReturn.put("timestamp", currTimestamp - (transmissionPeriod * 1000L));
        }
        break;
      case 2:
        toReturn.put("redundant_alg", 2);
        toReturn.put("package_num", (int) payloadData.get("package_num") - 2);
        toReturn.put("alarm", 0);
        toReturn.put("alarm_counts", 0);
        toReturn.put("counts", payloadData.get("counts2hAgo"));
        toReturn.put("counts_normalized", normalizeCounts((int) payloadData.get("counts2hAgo"), (int) payloadData.get("checkingTime2hAgo")));
        toReturn.put("checking_time", payloadData.get("checkingTime2hAgo"));
        toReturn.put("shock_counts", 0);
        toReturn.put("exwdtc", (int) payloadData.get("exwdtc") - 120);
        if (alarm != 0 && alarm != 1) {
          switch (alarm) {
            case 2:
            case 5:
              // Insert 2 hour ago because alarm has not been triggered or alarm has been triggered in last period.
              toReturn.put("timestamp", currTimestamp - (2L * transmissionPeriod * 1000));
              toReturn.put("exwdtc", (int) payloadData.get("exwdtc") - 120);
              toReturn.put("alarm", 2);
              break;
            case 3:
              // Insert 80 minutes ago because alarm has been triggered now in the first period of checking.
              toReturn.put("timestamp", currTimestamp - (2L * transmissionPeriod - transmissionPeriod / 3 * 2) * 1000);
              toReturn.put("exwdtc", (int) payloadData.get("exwdtc") - 80);
              toReturn.put("alarm", 2);
              break;
            case 4:
              // Insert 100 minutes ago because alarm has been triggered now in the first period of checking.
              toReturn.put("timestamp", currTimestamp - (2L * transmissionPeriod - transmissionPeriod / 3) * 1000);
              toReturn.put("exwdtc", (int) payloadData.get("exwdtc") - 100);
              toReturn.put("alarm", 2);
              break;
            default:
              // We should abort this message insertion here because alarm should never be higher than 4
              toReturn.clear();
              return toReturn;
          }
        } else {
          toReturn.put("timestamp", currTimestamp - (2L * transmissionPeriod * 1000));
        }
        break;
      default:
        throw new IllegalStateException("Unexpected value: " + dataLossCase);
    }
    toReturn.put("temperature", payloadData.get("temperature"));
    toReturn.put("mon3v3", payloadData.get("mon3v3"));
    toReturn.put("mon3v3_voltage", payloadData.get("mon3v3_voltage"));
    toReturn.put("mon5", payloadData.get("mon5"));
    toReturn.put("mon5_voltage", payloadData.get("mon5_voltage"));
    toReturn.put("monVin", payloadData.get("monVin"));
    toReturn.put("monVin_voltage", payloadData.get("monVin_voltage"));

    appendMetadata(message, toReturn);
    return toReturn;
  }

  private static Map<String, Object> decodePayload(DataInputStream inputStream) throws IOException {
    final Map<String, Object> output = new HashMap<>();

    // Common bytes
    final int devId = inputStream.readUnsignedByte();
    final int pNum = inputStream.readUnsignedShort();

    //30 bytes of sensor payload data
    final int alarm = inputStream.readUnsignedByte();
    final int alarmCounts = inputStream.readInt();
    final int counts = inputStream.readInt();
    final int counts1hAgo = inputStream.readInt();
    final int counts2hAgo = inputStream.readInt();
    final int checkingTime = inputStream.readUnsignedShort();
    final int checkingTime1hAgo = inputStream.readUnsignedShort();
    final int checkingTime2hAgo = inputStream.readUnsignedShort();
    final int shockCounts = inputStream.readInt();
    final int temperature = inputStream.readUnsignedShort();

    inputStream.skipBytes(1); // unused

    // Common bytes
    final int mon3v3 = inputStream.readUnsignedShort();
    final int mon5 = inputStream.readUnsignedShort();
    final int monVin = inputStream.readUnsignedShort();
    final int exwdtc = inputStream.readUnsignedShort();
    final double mon3v3_voltage = mon3v3 * VOLTAGE_CONSTANT;
    final double mon5_voltage = mon5 * VOLTAGE_CONSTANT;
    final double monVin_voltage = monVin * VOLTAGE_CONSTANT2;

    output.put("dev_ID", devId);
    output.put("package_num", pNum);
    output.put("alarm", alarm);
    output.put("alarm_counts", alarmCounts);
    output.put("counts", counts);
    output.put("counts1hAgo", counts1hAgo);
    output.put("counts2hAgo", counts2hAgo);
    output.put("checkingTime", checkingTime);
    output.put("checkingTime1hAgo", checkingTime1hAgo);
    output.put("checkingTime2hAgo", checkingTime2hAgo);
    output.put("shockCounts", shockCounts);
    output.put("temperature", temperature);

    // Voltages and extWDT counter
    output.put("mon3v3", mon3v3);
    output.put("mon3v3_voltage", mon3v3_voltage);
    output.put("mon5", mon5);
    output.put("mon5_voltage", mon5_voltage);
    output.put("monVin", monVin);
    output.put("monVin_voltage", monVin_voltage);
    output.put("exwdtc", exwdtc);

    return output;
  }

  private static double normalizeCounts(int counts, int checkingTime) {
    return (checkingTime == 0) ? 0.0 : (float) counts / checkingTime * 3600;
  }

  private static void appendMetadata(JsonObject message, Map<String, Object> obj) {
    // Add device name
    addJsonPropertyIfExists(message, "deviceName", "device_name", obj);

    // Add port as property
    addJsonPropertyIfExists(message, "fPort", "fPort", obj);

    // Add RSSI(s) and SNR(s) as properties
    if (message.has("rxInfo")) {
      JsonArray metadata = message.get("rxInfo").getAsJsonArray();
      int index = 0;
      for (JsonElement element : metadata) {
        JsonObject entry = element.getAsJsonObject();
        addGatewayProperty(entry, "rssi", "rssi_gw_%d", index, obj);
        addGatewayProperty(entry, "loRaSNR", "snr_gw_%d", index, obj);
        index++;
      }
    } else {
      LOGGER.warn("Message does not contain rxInfo. Not appending RSSI and SNR values.");
    }
  }

  private static void addJsonPropertyIfExists(JsonObject jsonObject, String propertyName, String formattedName, Map<String, Object> obj) {
    if (jsonObject.has(propertyName)) {
      obj.put(formattedName, jsonObject.get(propertyName).getAsString());
    } else {
      LOGGER.warn(String.format("Message does not contain %s. Not appending value.", propertyName));
    }
  }

  private static void addGatewayProperty(JsonObject entry, String propertyName, String formattedName, int index, Map<String, Object> obj) {
    if (entry.has(propertyName)) {
      obj.put(String.format(formattedName, index), entry.get(propertyName).getAsInt());
    } else {
      LOGGER.warn(String.format("Gateway %d does not contain %s. Not appending value.", index, propertyName));
    }
  }


}
