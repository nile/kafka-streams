package org.cern.nile.schema.db;

import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Arrays;
import java.util.List;
import org.cern.nile.schema.SchemaTestBase;
import org.junit.jupiter.api.Test;

public class ConnectSchemaToTableCreationCommandGeneratorTest extends SchemaTestBase {

  @Test
  public void generateOracleCreationCommand_GeneratesCommand_ForCorrectFields() {
    String tableName = "test_table";
    List<String> expectedFields = Arrays.asList(
        "byte_col NUMBER(3)",
        "short_col NUMBER(5)",
        "int_col NUMBER(10)",
        "long_col NUMBER(19)",
        "float_col FLOAT",
        "double_col FLOAT(126)",
        "boolean_col NUMBER(1)",
        "string_col VARCHAR2(510)",
        "timestamp_col TIMESTAMP",
        "date_col DATE",
        "bytes_col BLOB"
    );

    String actualCommand = ConnectSchemaToTableCreationCommandGenerator.getTableCreationCommand(data, DbType.ORACLE, tableName);

    for (String expectedField : expectedFields) {
      assertTrue(actualCommand.contains(expectedField), "Field " + expectedField + " is missing in the creation command");
    }
  }
}
