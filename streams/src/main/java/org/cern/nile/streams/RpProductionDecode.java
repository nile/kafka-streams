package org.cern.nile.streams;

import com.google.gson.JsonObject;
import java.io.IOException;
import java.text.ParseException;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.kstream.Consumed;
import org.cern.nile.decoders.RpProduction;
import org.cern.nile.exceptions.DecodingException;
import org.cern.nile.json.JsonSerde;

public class RpProductionDecode extends AbstractStream {

  private final Map<String, Long> timestamps = new HashMap<>();

  @Override
  protected void createTopology(StreamsBuilder builder) {
    builder
        .stream(sourceTopic, Consumed.with(Serdes.String(), new JsonSerde()))
        .filter(this::filterRecord)
        .transformValues(InjectOffsetTransformer::new)
        .flatMapValues(this::mapValues)
        .filter(AbstractStream::filterNull)
        .to(sinkTopic);
  }

  @Override
  protected boolean filterRecord(String key, JsonObject value) {
    return value.get("fPort") != null
        && value.get("data") != null
        && value.get("rxInfo") != null
        && value.get("deviceName") != null;
  }

  private Collection<Map<String, Object>> mapValues(JsonObject value) {
    Collection<Map<String, Object>> collection;
    try {
      final String deviceName = value.get("deviceName").getAsString();
      final Long lastDeviceTimestamp = timestamps.getOrDefault(deviceName, null);
      collection = RpProduction.decode(value, lastDeviceTimestamp);
      timestamps.put(deviceName, System.currentTimeMillis());
    } catch (IOException | ParseException | DecodingException e) {
      logStreamsException(e);
      return Collections.emptyList();
    }
    lastReadOffset = value.get("offset").getAsLong();
    return collection;
  }

}
