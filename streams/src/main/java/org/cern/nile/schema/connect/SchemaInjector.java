package org.cern.nile.schema.connect;

import com.google.gson.JsonPrimitive;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

public final class SchemaInjector {

  private SchemaInjector() {
  }

  /**
   * Injects a Connect schema into the given data.
   *
   * @param data Data to inject the schema into.
   * @return Data with the schema injected.
   */
  public static Map<String, Object> inject(Map<String, Object> data) {
    Map<String, Object> dataCopy = new HashMap<>(data);
    Map<String, Object> schemaMap = generateSchemaMap(dataCopy);

    Map<String, Object> result = new HashMap<>();
    result.put("schema", schemaMap);
    result.put("payload", dataCopy);

    return result;
  }

  private static Map<String, Object> generateSchemaMap(Map<String, Object> data) {
    Map<String, Object> schemaMap = new HashMap<>();
    schemaMap.put("type", "struct");
    schemaMap.put("fields", generateFieldMaps(data));

    return schemaMap;
  }

  private static Iterable<Map<String, Object>> generateFieldMaps(Map<String, Object> data) {
    return data.entrySet().stream().map(SchemaInjector::generateFieldMap).collect(Collectors.toList());
  }

  private static Map<String, Object> generateFieldMap(Map.Entry<String, Object> entry) {
    Map<String, Object> fieldMap = new HashMap<>();
    String key = entry.getKey();
    Object value = entry.getValue();

    validateValue(value);
    if (value instanceof JsonPrimitive) {
      // TODO: test this quick bugfix further
      value = ((JsonPrimitive) value).getAsString();
    }

    JsonType type = JsonType.fromClass(value.getClass());

    fieldMap.put("field", key);
    fieldMap.put("type", type.getType());
    fieldMap.put("optional", !key.toLowerCase().contains("timestamp"));

    addTimestampAndDateFields(fieldMap, key, type);

    return fieldMap;
  }

  private static void validateValue(Object value) {
    if (value == null) {
      throw new IllegalArgumentException("Null values are not allowed in the data map.");
    }
  }

  private static void addTimestampAndDateFields(Map<String, Object> fieldMap, String key, JsonType type) {
    boolean isTimestampField = key.toLowerCase().contains("timestamp");
    boolean isDateType = type.getClazz().equals(Date.class);

    if (isTimestampField) {
      fieldMap.put("name", "org.apache.kafka.connect.data.Timestamp");
      fieldMap.put("version", 1);
    } else if (isDateType) {
      fieldMap.put("name", "org.apache.kafka.connect.data.Date");
      fieldMap.put("version", 1);
    }
  }
}
