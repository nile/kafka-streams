package org.cern.nile.probes;

import com.sun.net.httpserver.HttpServer;
import java.io.IOException;
import java.net.InetSocketAddress;
import org.apache.kafka.streams.KafkaStreams;

public class Health {

  private static final int OK = 200;
  private static final int ERROR = 500;
  private static final int PORT = 8899;

  private final KafkaStreams streams;
  private HttpServer server;

  public Health(KafkaStreams streams) {
    this.streams = streams;
  }

  /**
   * Start the Health http server.
   */
  public void start() {
    try {
      server = HttpServer.create(new InetSocketAddress(PORT), 0);
    } catch (IOException ioe) {
      throw new RuntimeException("Could not setup http server: ", ioe);
    }
    server.createContext("/health", exchange -> {
      int responseCode = streams.state().isRunning() ? OK : ERROR;
      exchange.sendResponseHeaders(responseCode, 0);
      exchange.close();
    });
    server.start();
  }

  /**
   * Stops the Health HTTP server.
   */
  public void stop() {
    server.stop(0);
  }

}
