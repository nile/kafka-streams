package org.cern.nile.decoders.generated;

import static org.junit.jupiter.api.Assertions.assertThrows;

import com.google.gson.JsonParser;
import org.cern.nile.decoders.KaitaiPacketDecoder;
import org.cern.nile.exceptions.DecodingException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class KaitaiPacketDecoderTest {
  private static final String EMPTY_DATA_FRAME = "{\"data\":\"\"}";

  @Test
  void parseNumericValue_invalidValue_returnsOriginalString() {
    String invalidValue = "foo";
    Object result = KaitaiPacketDecoder.parseNumericValue(invalidValue);
    Assertions.assertEquals(invalidValue, result);
  }

  @Test
  void testDecodeWithInvalidData() {
    assertThrows(DecodingException.class,
        () -> KaitaiPacketDecoder.decode(JsonParser.parseString(EMPTY_DATA_FRAME).getAsJsonObject().get("data"), LoraCrackSensorsPacket.class));
  }

}
