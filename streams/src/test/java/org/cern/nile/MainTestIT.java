package org.cern.nile;

import static net.mguenther.kafka.junit.EmbeddedKafkaCluster.provisionWith;
import static net.mguenther.kafka.junit.ObserveKeyValues.on;
import static net.mguenther.kafka.junit.SendValues.to;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import javax.security.auth.login.AppConfigurationEntry;
import javax.security.auth.login.Configuration;
import net.mguenther.kafka.junit.EmbeddedKafkaCluster;
import net.mguenther.kafka.junit.EmbeddedKafkaClusterConfig;
import net.mguenther.kafka.junit.EmbeddedKafkaConfig;
import net.mguenther.kafka.junit.ReadKeyValues;
import net.mguenther.kafka.junit.TopicConfig;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;

@Disabled
public class MainTestIT {
  protected static final String MAIN_TEST_PROPERTIES = "main-test-config.properties";

  private static final String VALUE =
      "{\"applicationID\":\"26\",\"applicationName\":\"lora-EN-access-control-testing\",\"deviceName\":\"test-adeunis\",\"devEUI\":\"0018b21000006a53\",\"rxInfo\":[{\"gatewayID\":\"fcc23dfffe20d444\",\"uplinkID\":\"4efe2660-8894-4ffa-a3e4-0b3ed5819de2\",\"name\":\"lora-3182-gw-2\",\"time\":\"2023-03-16T14:52:36.637416804Z\",\"rssi\":-107,\"loRaSNR\":0,\"location\":{\"latitude\":46.23539,\"longitude\":6.05547,\"altitude\":480}},{\"gatewayID\":\"fcc23dfffe20c913\",\"uplinkID\":\"3aea7dc3-68aa-4788-a29b-6a5f163af44b\",\"name\":\"lora-0024-gw-1\",\"time\":\"2023-03-16T14:52:36.637121089Z\",\"rssi\":-109,\"loRaSNR\":-12.8,\"location\":{\"latitude\":46.22932,\"longitude\":6.05161,\"altitude\":480}},{\"gatewayID\":\"fcc23dfffe2064e4\",\"uplinkID\":\"46b7656c-97c8-4816-8e03-88a35f479c40\",\"name\":\"lora-0060-gw-2\",\"time\":\"2023-03-16T14:52:36.636894509Z\",\"rssi\":-106,\"loRaSNR\":-0.5,\"location\":{\"latitude\":46.23144,\"longitude\":6.05452,\"altitude\":480}},{\"gatewayID\":\"fcc23dfffe2095aa\",\"uplinkID\":\"fbd2e72c-9b0a-4406-ba3a-136f62b07730\",\"name\":\"lora-0361-gw-1\",\"time\":\"2023-03-16T14:52:36.64034413Z\",\"rssi\":-106,\"loRaSNR\":3.8,\"location\":{\"latitude\":46.23283,\"longitude\":6.0472,\"altitude\":480}},{\"gatewayID\":\"fcc23dfffe0f78a6\",\"uplinkID\":\"00da7b17-d221-4288-a523-82cd047e2550\",\"name\":\"lora-0060-gw\",\"time\":\"2023-03-16T14:52:36.640756418Z\",\"rssi\":-107,\"loRaSNR\":0,\"location\":{\"latitude\":46.23144,\"longitude\":6.05452,\"altitude\":480}},{\"gatewayID\":\"fcc23dfffe20ef66\",\"uplinkID\":\"4fa291de-7179-443a-9b28-e230faaac05f\",\"name\":\"lora-0361-gw-2\",\"time\":\"2023-03-16T14:52:36.634109748Z\",\"rssi\":-107,\"loRaSNR\":-3,\"location\":{\"latitude\":46.23283,\"longitude\":6.0472,\"altitude\":480}},{\"gatewayID\":\"fcc23dfffe207e29\",\"uplinkID\":\"57fea069-dd11-481b-886f-0e477b4ec36f\",\"name\":\"lora-3182-gw-1\",\"time\":\"2023-03-16T14:52:36.63994713Z\",\"rssi\":-107,\"loRaSNR\":-3.8,\"location\":{\"latitude\":46.23539,\"longitude\":6.05547,\"altitude\":480}},{\"gatewayID\":\"fcc23dfffe0f76fb\",\"uplinkID\":\"d5a3f8a2-aecc-420e-91eb-b16a494e48bc\",\"name\":\"lora-0227-gw\",\"time\":\"2023-03-16T14:52:36.636738592Z\",\"rssi\":-99,\"loRaSNR\":-8.2,\"location\":{\"latitude\":46.23431,\"longitude\":6.04088,\"altitude\":488}},{\"gatewayID\":\"fcc23dfffe20d13b\",\"uplinkID\":\"016d97e3-425a-4193-a5c6-3b572c565346\",\"name\":\"lora-0024-gw-2\",\"time\":\"2023-03-16T14:52:36.638382612Z\",\"rssi\":-112,\"loRaSNR\":-7.2,\"location\":{\"latitude\":46.22932,\"longitude\":6.05161,\"altitude\":480}}],\"txInfo\":{\"frequency\":868500000,\"dr\":1},\"adr\":true,\"fCnt\":11672,\"fPort\":1,\"data\":\"QAAAAgAAAAAAAA8=\",\"object\":{\"channel_1_info\":2,\"channel_1_open\":true,\"channel_1_prev\":true,\"channel_2_info\":0,\"channel_2_open\":true,\"channel_2_prev\":true,\"channel_3_info\":0,\"channel_3_open\":false,\"channel_3_prev\":false,\"channel_4_info\":0,\"channel_4_open\":false,\"channel_4_prev\":false,\"configuration\":false,\"flag_1\":false,\"flag_2\":false,\"low_batery\":false,\"status\":0,\"timestamp\":0}}";

  private static final String EXPECTED_DECODED_VALUE =
      "{\"schema\":{\"type\":\"struct\",\"fields\":[{\"type\":\"string\",\"optional\":true,\"field\":\"DEVEUI\"},{\"type\":\"string\",\"optional\":true,\"field\":\"CHANNEL1INFO\"},{\"type\":\"string\",\"optional\":true,\"field\":\"CHANNEL2INFO\"},{\"type\":\"string\",\"optional\":true,\"field\":\"CHANNEL3INFO\"},{\"type\":\"string\",\"optional\":true,\"field\":\"CHANNEL4INFO\"},{\"type\":\"string\",\"optional\":true,\"field\":\"FRAMECOUNTER\"},{\"type\":\"string\",\"optional\":true,\"field\":\"CONFIG\"},{\"type\":\"string\",\"optional\":true,\"field\":\"LOWBAT\"},{\"type\":\"string\",\"optional\":true,\"field\":\"TIMESTAMP\"},{\"type\":\"string\",\"optional\":true,\"field\":\"CHANNEL1CURRENTSTATE\"},{\"type\":\"string\",\"optional\":true,\"field\":\"CHANNEL1PREVIOUSSTATE\"},{\"type\":\"string\",\"optional\":true,\"field\":\"CHANNEL2CURRENTSTATE\"},{\"type\":\"string\",\"optional\":true,\"field\":\"CHANNEL2PREVIOUSSTATE\"},{\"type\":\"string\",\"optional\":true,\"field\":\"CHANNEL3CURRENTSTATE\"},{\"type\":\"string\",\"optional\":true,\"field\":\"CHANNEL3PREVIOUSSTATE\"},{\"type\":\"string\",\"optional\":true,\"field\":\"CHANNEL4CURRENTSTATE\"},{\"type\":\"string\",\"optional\":true,\"field\":\"CHANNEL4PREVIOUSSTATE\"}]},\"payload\":{\"CHANNEL1INFO\":2,\"DEVICENAME\":\"test-adeunis\",\"CHANNEL3INFO\":0,\"CHANNEL3CURRENTSTATE\":false,\"CHANNEL4CURRENTSTATE\":true,\"CHANNEL2PREVIOUSSTATE\":false,\"FRAMECOUNTER\":0,\"CHANNEL1PREVIOUSSTATE\":false,\"CHANNEL2INFO\":0,\"CONFIG\":false,\"CHANNEL4PREVIOUSSTATE\":true,\"LOWBAT\":false,\"CHANNEL4INFO\":0,\"TIMESTAMP\":false,\"CHANNEL3PREVIOUSSTATE\":false,\"CHANNEL2CURRENTSTATE\":false,\"DEVEUI\":\"0018b21000006a53\",\"CHANNEL1CURRENTSTATE\":false,\"applicationID\":\"26\",\"applicationName\":\"lora-EN-access-control-testing\",\"timestamp\":1678978356637}}";

  private static EmbeddedKafkaCluster kafka;

  @BeforeEach
  void setupKafka() {
    EmbeddedKafkaConfig kafkaConfig = EmbeddedKafkaConfig.create()
        .with("zookeeper.connection.timeout.ms", "10000")
        .with("zookeeper.session.timeout.ms", "10000")
        .with("auto.create.topics.enable", "true")
        .with("log.flush.interval.messages", "1")
        .with("listeners", "PLAINTEXT://:0")
        .with("port", "0")
        .with("log.dir", "/tmp/kafka-logs")
        .with("security.protocol", "SASL_PLAINTEXT")
        .with("sasl.mechanism", "PLAIN")
        .with("sasl.jaas.config", "org.apache.kafka.common.security.plain.PlainLoginModule required username=\"test\" password=\"test123\";")
        .build();

    EmbeddedKafkaClusterConfig clusterConfig = EmbeddedKafkaClusterConfig.create().provisionWith(kafkaConfig).build();

    kafka = provisionWith(clusterConfig);
    kafka.start();
  }


  @AfterAll
  public static void tearDownKafkaCluster() {
    kafka.stop();
  }

  @Test
  public void testMain(@TempDir Path tempDir) throws IOException, InterruptedException, ExecutionException {
    InputStream testConfigStream = getClass().getClassLoader().getResourceAsStream(MAIN_TEST_PROPERTIES);
    assert testConfigStream != null;
    String content = new String(testConfigStream.readAllBytes());

    content = content.replace("<mock-kafka-bootstrap-servers>", kafka.getBrokerList());

    Path testConfigPath = tempDir.resolve("main-test-config-with-brokers.properties");

    Files.write(testConfigPath, content.getBytes());

    configureJaas("test", "test123");
    kafka.send(to("lora-EN-access-control", VALUE).build());
    kafka.observe(on("lora-EN-access-control", 1).build());
    kafka.createTopic(TopicConfig.forTopic("lora-EN-access-control-decoded").build());

    CompletableFuture<Void> future = CompletableFuture.runAsync(() -> {
      try {
        Main.main(new String[]{testConfigPath.toString(), String.valueOf(true)});
      } catch (Exception e) {
        fail("Main should run");
      }
    });

    try {
      future.get(2, TimeUnit.SECONDS);
      kafka.observe(on("lora-EN-access-control-decoded", 1).build());
      List<String> values = kafka.readValues(ReadKeyValues.from("lora-EN-access-control-decoded").build());
      assertEquals(EXPECTED_DECODED_VALUE, values.get(0));
    } catch (AssertionError e) {
      fail("Expected 1 decoded record, received none.", e);
    } catch (TimeoutException ignored) {
    }

  }

  /**
   * Used for testing.
   *
   * @param username the username
   * @param password the password
   */
  private void configureJaas(String username, String password) {
    String jaasConfig =
        String.format("org.apache.kafka.common.security.plain.PlainLoginModule required username=\"%s\" password=\"%s\";", username, password);
    System.setProperty("java.security.auth.login.config", "");
    Configuration.setConfiguration(new Configuration() {
      @Override
      public AppConfigurationEntry[] getAppConfigurationEntry(String name) {
        if ("KafkaClient".equals(name)) {
          Map<String, String> options = new HashMap<>();
          options.put("username", username);
          options.put("password", password);

          return new AppConfigurationEntry[]{
              new AppConfigurationEntry("org.apache.kafka.common.security.plain.PlainLoginModule",
                  AppConfigurationEntry.LoginModuleControlFlag.REQUIRED, options)
          };
        }
        return new AppConfigurationEntry[0];
      }
    });
  }
}
