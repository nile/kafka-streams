package org.cern.nile;

import java.io.FileInputStream;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.Properties;
import org.cern.nile.clients.KafkaStreamsClient;
import org.cern.nile.configs.PropertiesCheck;
import org.cern.nile.configs.StreamConfig;
import org.cern.nile.configs.StreamType;
import org.cern.nile.streams.Streaming;

public class Main {

  /**
   * Main method.
   *
   * @param args the properties files
   */
  public static void main(String[] args) {
    // Check if properties file was passed
    if (args.length < 1) {
      throw new RuntimeException("Expecting args[0] to be the path to the configuration file");
    }

    // Loading properties file
    String configsPath = args[0];
    final Properties configs = new Properties();
    try {
      configs.load(new FileInputStream(configsPath));
    } catch (IOException e) {
      e.printStackTrace();
      throw new RuntimeException(e);
    }

    StreamType sType = StreamType.valueOf(configs.getProperty(StreamConfig.CommonProperties.STREAM_TYPE.getValue(), null));

    PropertiesCheck.validateProperties(configs, sType);

    // Initialize Kafka Client
    final KafkaStreamsClient client = new KafkaStreamsClient();
    client.configure(configs);

    // Start Streaming
    try {
      Class<?> clazz = Class.forName(configs.getProperty(StreamConfig.CommonProperties.STREAM_CLASS.getValue()));
      final Streaming streaming;
      streaming = (Streaming) clazz.getDeclaredConstructor().newInstance();
      streaming.configure(configs);
      streaming.stream(client);
    } catch (ClassNotFoundException | IllegalAccessException | InstantiationException | ClassCastException
             | InvocationTargetException | NoSuchMethodException e) {
      e.printStackTrace();
    }
  }
}
