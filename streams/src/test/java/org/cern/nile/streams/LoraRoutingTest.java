package org.cern.nile.streams;

import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

import com.google.gson.JsonObject;
import java.util.Properties;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.cern.nile.configs.StreamConfig;
import org.cern.nile.exceptions.RoutingConfigurationException;
import org.junit.jupiter.api.Test;

public class LoraRoutingTest extends BaseStreamDecodeTest {
  private static final String TEST_TOPIC_1 = "test-topic-1";
  private static final String TEST_TOPIC_2 = "test-topic-2";
  private static final String DLQ_TOPIC = "lora-mqtt-test-dlq";

  private static LoraRouting loraRouting;

  @Override
  protected LoraRouting createStreamDecodeInstance() {
    loraRouting = new LoraRouting(SOURCE_TOPIC, SINK_TOPIC);
    return loraRouting;
  }

  private static final String MATCHED_APPLICATION_1 =
      "{\"applicationID\":\"2\",\"applicationName\":\"testApp1-123\",\"deviceName\":\"covid-distancing-test2489\",\"devEUI\":\"0000000000014237\",\"rxInfo\":[{\"gatewayID\":\"fcc23dfffe2095aa\",\"uplinkID\":\"e0303aae-d078-41a6-91fd-2cb2fd662e3a\",\"name\":\"lora-0361-gw-1\",\"time\":\"2021-09-21T11:31:24.740187827Z\",\"rssi\":-110,\"loRaSNR\":1.5,\"location\":{\"latitude\":46.23283,\"longitude\":6.0472,\"altitude\":480}},{\"gatewayID\":\"fcc23dfffe21f4aa\",\"uplinkID\":\"5d21ba6a-1f5e-46db-997f-68cd827a1c4a\",\"name\":\"lora-0227-gw-2\",\"time\":\"2021-09-21T11:31:24.762786241Z\",\"rssi\":-109,\"loRaSNR\":-16,\"location\":{\"latitude\":46.23431,\"longitude\":6.04088,\"altitude\":488}},{\"gatewayID\":\"fcc23dfffe20a8b0\",\"uplinkID\":\"32326e10-3c57-48fb-8b31-6ead469772e3\",\"name\":\"lora-0031-gw\",\"time\":\"2021-09-21T11:31:24.764631391Z\",\"rssi\":-110,\"loRaSNR\":1,\"location\":{\"latitude\":46.23254,\"longitude\":6.04501,\"altitude\":460}},{\"gatewayID\":\"fcc23dfffe20ef66\",\"uplinkID\":\"dde1564e-4cb5-4a5b-9219-85ea95332133\",\"name\":\"lora-0361-gw-2\",\"time\":\"2021-09-21T11:31:24.740925026Z\",\"rssi\":-109,\"loRaSNR\":-3.5,\"location\":{\"latitude\":46.23283,\"longitude\":6.0472,\"altitude\":480}},{\"gatewayID\":\"fcc23dfffe0f76fb\",\"uplinkID\":\"078b4412-691b-4cbe-b1c4-519dfd3ff01e\",\"name\":\"lora-0227-gw\",\"time\":\"2021-09-21T11:31:24.737127858Z\",\"rssi\":-113,\"loRaSNR\":-4.5,\"location\":{\"latitude\":46.23431,\"longitude\":6.04088,\"altitude\":488}},{\"gatewayID\":\"fcc23dfffe0f78a6\",\"uplinkID\":\"818557c5-4f93-406e-90bf-20cdeda8bc6b\",\"name\":\"lora-0060-gw\",\"time\":\"2021-09-21T11:31:24.758842184Z\",\"rssi\":-113,\"loRaSNR\":-6.8,\"location\":{\"latitude\":46.23144,\"longitude\":6.05452,\"altitude\":480}},{\"gatewayID\":\"fcc23dfffe20d444\",\"uplinkID\":\"4a79b63a-d7d6-42a2-a81c-9839fde8ce1c\",\"name\":\"lora-3182-gw-2\",\"time\":\"2021-09-21T11:31:24.757473121Z\",\"rssi\":-112,\"loRaSNR\":-11.2,\"location\":{\"latitude\":46.23539,\"longitude\":6.05547,\"altitude\":480}},{\"gatewayID\":\"fcc23dfffe2064e4\",\"uplinkID\":\"00083f51-14a4-4f46-8d8f-6c41621b7a02\",\"name\":\"lora-0060-gw-2\",\"time\":\"2021-09-21T11:31:24.740471459Z\",\"rssi\":-111,\"loRaSNR\":-5.8,\"location\":{\"latitude\":46.23144,\"longitude\":6.05452,\"altitude\":480}},{\"gatewayID\":\"fcc23dfffe207e29\",\"uplinkID\":\"e9fe1e07-f5c1-4fad-b8f4-a2c9e3a6a773\",\"name\":\"lora-3182-gw-1\",\"time\":\"2021-09-21T11:31:24.737650437Z\",\"rssi\":-111,\"loRaSNR\":-9.2,\"location\":{\"latitude\":46.23539,\"longitude\":6.05547,\"altitude\":480}}],\"txInfo\":{\"frequency\":867900000,\"dr\":2},\"adr\":false,\"fCnt\":3,\"fPort\":5,\"data\":\"AwCxok+uAgAHqw==\"}";
  private static final String MATCHED_APPLICATION_2 =
      "{\"applicationID\":\"2\",\"applicationName\":\"testApp2-123\",\"deviceName\":\"covid-distancing-test2489\",\"devEUI\":\"0000000000014237\",\"rxInfo\":[{\"gatewayID\":\"fcc23dfffe2095aa\",\"uplinkID\":\"e0303aae-d078-41a6-91fd-2cb2fd662e3a\",\"name\":\"lora-0361-gw-1\",\"time\":\"2021-09-21T11:31:24.740187827Z\",\"rssi\":-110,\"loRaSNR\":1.5,\"location\":{\"latitude\":46.23283,\"longitude\":6.0472,\"altitude\":480}},{\"gatewayID\":\"fcc23dfffe21f4aa\",\"uplinkID\":\"5d21ba6a-1f5e-46db-997f-68cd827a1c4a\",\"name\":\"lora-0227-gw-2\",\"time\":\"2021-09-21T11:31:24.762786241Z\",\"rssi\":-109,\"loRaSNR\":-16,\"location\":{\"latitude\":46.23431,\"longitude\":6.04088,\"altitude\":488}},{\"gatewayID\":\"fcc23dfffe20a8b0\",\"uplinkID\":\"32326e10-3c57-48fb-8b31-6ead469772e3\",\"name\":\"lora-0031-gw\",\"time\":\"2021-09-21T11:31:24.764631391Z\",\"rssi\":-110,\"loRaSNR\":1,\"location\":{\"latitude\":46.23254,\"longitude\":6.04501,\"altitude\":460}},{\"gatewayID\":\"fcc23dfffe20ef66\",\"uplinkID\":\"dde1564e-4cb5-4a5b-9219-85ea95332133\",\"name\":\"lora-0361-gw-2\",\"time\":\"2021-09-21T11:31:24.740925026Z\",\"rssi\":-109,\"loRaSNR\":-3.5,\"location\":{\"latitude\":46.23283,\"longitude\":6.0472,\"altitude\":480}},{\"gatewayID\":\"fcc23dfffe0f76fb\",\"uplinkID\":\"078b4412-691b-4cbe-b1c4-519dfd3ff01e\",\"name\":\"lora-0227-gw\",\"time\":\"2021-09-21T11:31:24.737127858Z\",\"rssi\":-113,\"loRaSNR\":-4.5,\"location\":{\"latitude\":46.23431,\"longitude\":6.04088,\"altitude\":488}},{\"gatewayID\":\"fcc23dfffe0f78a6\",\"uplinkID\":\"818557c5-4f93-406e-90bf-20cdeda8bc6b\",\"name\":\"lora-0060-gw\",\"time\":\"2021-09-21T11:31:24.758842184Z\",\"rssi\":-113,\"loRaSNR\":-6.8,\"location\":{\"latitude\":46.23144,\"longitude\":6.05452,\"altitude\":480}},{\"gatewayID\":\"fcc23dfffe20d444\",\"uplinkID\":\"4a79b63a-d7d6-42a2-a81c-9839fde8ce1c\",\"name\":\"lora-3182-gw-2\",\"time\":\"2021-09-21T11:31:24.757473121Z\",\"rssi\":-112,\"loRaSNR\":-11.2,\"location\":{\"latitude\":46.23539,\"longitude\":6.05547,\"altitude\":480}},{\"gatewayID\":\"fcc23dfffe2064e4\",\"uplinkID\":\"00083f51-14a4-4f46-8d8f-6c41621b7a02\",\"name\":\"lora-0060-gw-2\",\"time\":\"2021-09-21T11:31:24.740471459Z\",\"rssi\":-111,\"loRaSNR\":-5.8,\"location\":{\"latitude\":46.23144,\"longitude\":6.05452,\"altitude\":480}},{\"gatewayID\":\"fcc23dfffe207e29\",\"uplinkID\":\"e9fe1e07-f5c1-4fad-b8f4-a2c9e3a6a773\",\"name\":\"lora-3182-gw-1\",\"time\":\"2021-09-21T11:31:24.737650437Z\",\"rssi\":-111,\"loRaSNR\":-9.2,\"location\":{\"latitude\":46.23539,\"longitude\":6.05547,\"altitude\":480}}],\"txInfo\":{\"frequency\":867900000,\"dr\":2},\"adr\":false,\"fCnt\":3,\"fPort\":5,\"data\":\"AwCxok+uAgAHqw==\"}";
  private static final String UNMATCHED_APPLICATION =
      "{\"applicationID\":\"2\",\"applicationName\":\"not-matched\",\"deviceName\":\"covid-distancing-test2489\",\"devEUI\":\"0000000000014237\",\"rxInfo\":[{\"gatewayID\":\"fcc23dfffe2095aa\",\"uplinkID\":\"e0303aae-d078-41a6-91fd-2cb2fd662e3a\",\"name\":\"lora-0361-gw-1\",\"time\":\"2021-09-21T11:31:24.740187827Z\",\"rssi\":-110,\"loRaSNR\":1.5,\"location\":{\"latitude\":46.23283,\"longitude\":6.0472,\"altitude\":480}},{\"gatewayID\":\"fcc23dfffe21f4aa\",\"uplinkID\":\"5d21ba6a-1f5e-46db-997f-68cd827a1c4a\",\"name\":\"lora-0227-gw-2\",\"time\":\"2021-09-21T11:31:24.762786241Z\",\"rssi\":-109,\"loRaSNR\":-16,\"location\":{\"latitude\":46.23431,\"longitude\":6.04088,\"altitude\":488}},{\"gatewayID\":\"fcc23dfffe20a8b0\",\"uplinkID\":\"32326e10-3c57-48fb-8b31-6ead469772e3\",\"name\":\"lora-0031-gw\",\"time\":\"2021-09-21T11:31:24.764631391Z\",\"rssi\":-110,\"loRaSNR\":1,\"location\":{\"latitude\":46.23254,\"longitude\":6.04501,\"altitude\":460}},{\"gatewayID\":\"fcc23dfffe20ef66\",\"uplinkID\":\"dde1564e-4cb5-4a5b-9219-85ea95332133\",\"name\":\"lora-0361-gw-2\",\"time\":\"2021-09-21T11:31:24.740925026Z\",\"rssi\":-109,\"loRaSNR\":-3.5,\"location\":{\"latitude\":46.23283,\"longitude\":6.0472,\"altitude\":480}},{\"gatewayID\":\"fcc23dfffe0f76fb\",\"uplinkID\":\"078b4412-691b-4cbe-b1c4-519dfd3ff01e\",\"name\":\"lora-0227-gw\",\"time\":\"2021-09-21T11:31:24.737127858Z\",\"rssi\":-113,\"loRaSNR\":-4.5,\"location\":{\"latitude\":46.23431,\"longitude\":6.04088,\"altitude\":488}},{\"gatewayID\":\"fcc23dfffe0f78a6\",\"uplinkID\":\"818557c5-4f93-406e-90bf-20cdeda8bc6b\",\"name\":\"lora-0060-gw\",\"time\":\"2021-09-21T11:31:24.758842184Z\",\"rssi\":-113,\"loRaSNR\":-6.8,\"location\":{\"latitude\":46.23144,\"longitude\":6.05452,\"altitude\":480}},{\"gatewayID\":\"fcc23dfffe20d444\",\"uplinkID\":\"4a79b63a-d7d6-42a2-a81c-9839fde8ce1c\",\"name\":\"lora-3182-gw-2\",\"time\":\"2021-09-21T11:31:24.757473121Z\",\"rssi\":-112,\"loRaSNR\":-11.2,\"location\":{\"latitude\":46.23539,\"longitude\":6.05547,\"altitude\":480}},{\"gatewayID\":\"fcc23dfffe2064e4\",\"uplinkID\":\"00083f51-14a4-4f46-8d8f-6c41621b7a02\",\"name\":\"lora-0060-gw-2\",\"time\":\"2021-09-21T11:31:24.740471459Z\",\"rssi\":-111,\"loRaSNR\":-5.8,\"location\":{\"latitude\":46.23144,\"longitude\":6.05452,\"altitude\":480}},{\"gatewayID\":\"fcc23dfffe207e29\",\"uplinkID\":\"e9fe1e07-f5c1-4fad-b8f4-a2c9e3a6a773\",\"name\":\"lora-3182-gw-1\",\"time\":\"2021-09-21T11:31:24.737650437Z\",\"rssi\":-111,\"loRaSNR\":-9.2,\"location\":{\"latitude\":46.23539,\"longitude\":6.05547,\"altitude\":480}}],\"txInfo\":{\"frequency\":867900000,\"dr\":2},\"adr\":false,\"fCnt\":3,\"fPort\":5,\"data\":\"AwCxok+uAgAHqw==\"}";
  private static final String MISSING_APPLICATION_NAME =
      "{\"applicationID\":\"2\",\"deviceName\":\"covid-distancing-test2489\",\"devEUI\":\"0000000000014237\",\"rxInfo\":[{\"gatewayID\":\"fcc23dfffe2095aa\",\"uplinkID\":\"e0303aae-d078-41a6-91fd-2cb2fd662e3a\",\"name\":\"lora-0361-gw-1\",\"time\":\"2021-09-21T11:31:24.740187827Z\",\"rssi\":-110,\"loRaSNR\":1.5,\"location\":{\"latitude\":46.23283,\"longitude\":6.0472,\"altitude\":480}},{\"gatewayID\":\"fcc23dfffe21f4aa\",\"uplinkID\":\"5d21ba6a-1f5e-46db-997f-68cd827a1c4a\",\"name\":\"lora-0227-gw-2\",\"time\":\"2021-09-21T11:31:24.762786241Z\",\"rssi\":-109,\"loRaSNR\":-16,\"location\":{\"latitude\":46.23431,\"longitude\":6.04088,\"altitude\":488}},{\"gatewayID\":\"fcc23dfffe20a8b0\",\"uplinkID\":\"32326e10-3c57-48fb-8b31-6ead469772e3\",\"name\":\"lora-0031-gw\",\"time\":\"2021-09-21T11:31:24.764631391Z\",\"rssi\":-110,\"loRaSNR\":1,\"location\":{\"latitude\":46.23254,\"longitude\":6.04501,\"altitude\":460}},{\"gatewayID\":\"fcc23dfffe20ef66\",\"uplinkID\":\"dde1564e-4cb5-4a5b-9219-85ea95332133\",\"name\":\"lora-0361-gw-2\",\"time\":\"2021-09-21T11:31:24.740925026Z\",\"rssi\":-109,\"loRaSNR\":-3.5,\"location\":{\"latitude\":46.23283,\"longitude\":6.0472,\"altitude\":480}},{\"gatewayID\":\"fcc23dfffe0f76fb\",\"uplinkID\":\"078b4412-691b-4cbe-b1c4-519dfd3ff01e\",\"name\":\"lora-0227-gw\",\"time\":\"2021-09-21T11:31:24.737127858Z\",\"rssi\":-113,\"loRaSNR\":-4.5,\"location\":{\"latitude\":46.23431,\"longitude\":6.04088,\"altitude\":488}},{\"gatewayID\":\"fcc23dfffe0f78a6\",\"uplinkID\":\"818557c5-4f93-406e-90bf-20cdeda8bc6b\",\"name\":\"lora-0060-gw\",\"time\":\"2021-09-21T11:31:24.758842184Z\",\"rssi\":-113,\"loRaSNR\":-6.8,\"location\":{\"latitude\":46.23144,\"longitude\":6.05452,\"altitude\":480}},{\"gatewayID\":\"fcc23dfffe20d444\",\"uplinkID\":\"4a79b63a-d7d6-42a2-a81c-9839fde8ce1c\",\"name\":\"lora-3182-gw-2\",\"time\":\"2021-09-21T11:31:24.757473121Z\",\"rssi\":-112,\"loRaSNR\":-11.2,\"location\":{\"latitude\":46.23539,\"longitude\":6.05547,\"altitude\":480}},{\"gatewayID\":\"fcc23dfffe2064e4\",\"uplinkID\":\"00083f51-14a4-4f46-8d8f-6c41621b7a02\",\"name\":\"lora-0060-gw-2\",\"time\":\"2021-09-21T11:31:24.740471459Z\",\"rssi\":-111,\"loRaSNR\":-5.8,\"location\":{\"latitude\":46.23144,\"longitude\":6.05452,\"altitude\":480}},{\"gatewayID\":\"fcc23dfffe207e29\",\"uplinkID\":\"e9fe1e07-f5c1-4fad-b8f4-a2c9e3a6a773\",\"name\":\"lora-3182-gw-1\",\"time\":\"2021-09-21T11:31:24.737650437Z\",\"rssi\":-111,\"loRaSNR\":-9.2,\"location\":{\"latitude\":46.23539,\"longitude\":6.05547,\"altitude\":480}}],\"txInfo\":{\"frequency\":867900000,\"dr\":2},\"adr\":false,\"fCnt\":3,\"fPort\":5,\"data\":\"AwCxok+uAgAHqw==\"}";

  @Test
  void loraRoutingTopology_RoutesToTopic_forMatchedApplication() {
    pipeRecord(MATCHED_APPLICATION_1);
    final ProducerRecord<String, JsonObject> outputRecord = testDriver.readOutput(TEST_TOPIC_1, KEY_DESERIALIZER, VALUE_DESERIALIZER);
    assertNotNull(outputRecord);

    pipeRecord(MATCHED_APPLICATION_2);
    final ProducerRecord<String, JsonObject> outputRecord2 = testDriver.readOutput(TEST_TOPIC_2, KEY_DESERIALIZER, VALUE_DESERIALIZER);
    assertNotNull(outputRecord2);
    assertNotEquals(outputRecord, outputRecord2);
  }

  @Test
  void loraRoutingTopology_DropsMessage_forNullApplicationName() {
    pipeRecord(MISSING_APPLICATION_NAME);
    final ProducerRecord<String, JsonObject> outputRecordDlq = testDriver.readOutput(TEST_TOPIC_1, KEY_DESERIALIZER, VALUE_DESERIALIZER);
    assertNull(outputRecordDlq);
  }

  @Test
  void loraRoutingTopology_RoutesToDlq_forUnmatchedApplication() {
    pipeRecord(UNMATCHED_APPLICATION);
    final ProducerRecord<String, JsonObject> outputRecordDlq = testDriver.readOutput(DLQ_TOPIC, KEY_DESERIALIZER, VALUE_DESERIALIZER);
    assertNotNull(outputRecordDlq);
    final ProducerRecord<String, JsonObject> outputRecordTestTopic1 = testDriver.readOutput(TEST_TOPIC_1, KEY_DESERIALIZER, VALUE_DESERIALIZER);
    assertNull(outputRecordTestTopic1);
  }

  @Test
  void configure_ThrowsException_forNotExistingFile() {
    final Properties routingProperties = new Properties();
    routingProperties.put(StreamConfig.RoutingProperties.ROUTING_CONFIG_PATH.getValue(), "not-existing");
    assertThrows(RuntimeException.class, () -> loraRouting.configure(routingProperties));
  }

  @Test
  void configure_ThrowsException_forUnreadableFile() {
    final Properties routingProperties = new Properties();
    routingProperties.put(StreamConfig.RoutingProperties.ROUTING_CONFIG_PATH.getValue(), "non-existing");
    assertThrows(RoutingConfigurationException.class, () -> loraRouting.configure(routingProperties));
  }

}

