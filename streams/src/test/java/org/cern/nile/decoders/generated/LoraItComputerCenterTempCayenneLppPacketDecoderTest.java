package org.cern.nile.decoders.generated;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import com.google.gson.JsonParser;
import java.util.Map;
import org.cern.nile.decoders.KaitaiPacketDecoder;
import org.cern.nile.exceptions.DecodingException;
import org.junit.jupiter.api.Test;

public class LoraItComputerCenterTempCayenneLppPacketDecoderTest {

  private static final String LORA_IT_COMPUTER_CENTER_TEMP_DATA_FRAME =
      "{\"data\":\"AWcBKQJoagNnAL8EAv/2\"}";
  private static final String LORA_IT_COMPUTER_CENTER_TEMP_UNSUPPORTED_FRAME =
      "{\"data\":\"Oh4WCwcX\"}";

  @Test
  void decode_DecodesMessageData() throws DecodingException {
    Map<String, Object> packet =
        KaitaiPacketDecoder.decode(JsonParser.parseString(LORA_IT_COMPUTER_CENTER_TEMP_DATA_FRAME).getAsJsonObject().get("data"),
            LoraItComputerCenterTempCayenneLppPacket.class);
    assertEquals(29.7, packet.get("temperature"));
    assertEquals(53.0, packet.get("relativeHumidity"));
    assertEquals(-0.1, packet.get("differentialPressure"));
    assertEquals(19.1, packet.get("dewPoint"));
  }

  @Test
  void decode_Fails_forLoraEnAccessControlUnsupportedFrame() {
    assertThrows(DecodingException.class, () -> KaitaiPacketDecoder.decode(JsonParser.parseString(LORA_IT_COMPUTER_CENTER_TEMP_UNSUPPORTED_FRAME).getAsJsonObject().get("data"),
        LoraItComputerCenterTempCayenneLppPacket.class), "Error while decoding packet: Unsupported frame");
  }

}
