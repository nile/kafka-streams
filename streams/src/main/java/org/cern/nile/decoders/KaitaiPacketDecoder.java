package org.cern.nile.decoders;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.reflect.TypeToken;
import io.kaitai.struct.ByteBufferKaitaiStream;
import io.kaitai.struct.KaitaiStream;
import io.kaitai.struct.KaitaiStruct;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Type;
import java.nio.charset.StandardCharsets;
import java.util.*;
import org.cern.nile.exceptions.DecodingException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public final class KaitaiPacketDecoder {

  private static final Logger LOGGER = LoggerFactory.getLogger(KaitaiPacketDecoder.class);

  private static final Set<String> FIELDS_TO_SKIP = new HashSet<>(Arrays.asList("_io", "_parent", "_root", "magic"));

  /**
   * Decodes the payload of a Kaitai Struct generated class into a map.
   *
   * @param <T>   the type of KaitaiStruct
   * @param data  the payload data in Base64 encoding as a JsonElement
   * @param clazz the Kaitai Struct generated class of type T
   * @return a Map containing the decoded payload data, with field names as keys and their respective values as map values
   */
  public static <T extends KaitaiStruct> Map<String, Object> decode(JsonElement data, Class<T> clazz) throws DecodingException {
    return decode(data, clazz, false);
  }

  /**
   * Decodes the payload of a Kaitai Struct generated class into a map.
   *
   * @param <T>          the type of KaitaiStruct
   * @param data         the payload data in Base64 encoding as a JsonElement
   * @param clazz        the Kaitai Struct generated class of type T
   * @param addFrameType add the frame type in the decoded map (use with Kaitai structs that have different frame types)
   * @return a Map containing the decoded payload data, with field names as keys and their respective values as map values
   */
  public static <T extends KaitaiStruct> Map<String, Object> decode(JsonElement data, Class<T> clazz, boolean addFrameType) {
    byte[] dataDecoded = Base64.getDecoder().decode(data.getAsString());
    T packet = instantiateKaitaiStruct(clazz, dataDecoded);
    KaitaiStruct payload = extractPayload(packet);
    return createRecordsMap(Objects.requireNonNullElse(payload, packet), addFrameType);
  }

  /**
   * Parses a numeric value from a string.
   *
   * @param value the string to parse
   * @return the parsed value, or the original string if it is not a numeric value
   */
  public static Object parseNumericValue(String value) {
    if (value.matches("^[-+]?\\d+$")) {
      try {
        return Integer.parseInt(value);
      } catch (NumberFormatException e) {
        return Long.parseLong(value);
      }
    } else if (value.matches("^[-+]?\\d*\\.\\d+([eE][-+]?\\d+)?$")) {
      return Double.parseDouble(value);
    }
    return value;
  }

  private static <T extends KaitaiStruct> T instantiateKaitaiStruct(Class<T> clazz, byte[] dataDecoded) {
    try {
      return clazz.getDeclaredConstructor(KaitaiStream.class).newInstance(new ByteBufferKaitaiStream(dataDecoded));
    } catch (InstantiationException | IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
      LOGGER.error("Could not instantiate Kaitai Struct generated class", e);
      throw new DecodingException("Could not instantiate Kaitai Struct generated class", e);
    }
  }

  private static <T extends KaitaiStruct> KaitaiStruct extractPayload(T packet) {
    for (Field field : packet.getClass().getDeclaredFields()) {
      if (KaitaiStruct.class.isAssignableFrom(field.getType())) {
        field.setAccessible(true);
        try {
          return (KaitaiStruct) field.get(packet);
        } catch (IllegalAccessException e) {
          LOGGER.error("Error while extracting payload", e);
          throw new DecodingException("Error while extracting payload", e);
        }
      }
    }
    return null;
  }

  private static <T extends KaitaiStruct> Map<String, Object> createRecordsMap(T packet, boolean addFrameType) {
    Map<String, String> fieldNameMapping = loadFieldNameMapping(
        packet.getClass().getDeclaringClass() != null ? packet.getClass().getDeclaringClass().getSimpleName() : packet.getClass().getSimpleName());
    HashMap<String, Object> records = new HashMap<>();

    String frameType = packet.getClass().getSimpleName();

    if (frameType.equals("UnsupportedFrame")) {
      throw new DecodingException("Error while decoding packet: Unsupported frame");
    }

    if (addFrameType) {
      records.put(fieldNameMapping.getOrDefault("frameType", "frameType"), frameType);
    }

    for (Field field : packet.getClass().getDeclaredFields()) {
      if (shouldSkipField(field.getName())) {
        continue;
      }
      field.setAccessible(true);
      try {
        // Use the field name mapping to get the new name for the field, if it exists.
        // Otherwise, use the original field name.
        String fieldName = fieldNameMapping.getOrDefault(field.getName(), field.getName());

        Object value = field.get(packet);
        if (value != null) {
          if (value instanceof KaitaiStruct) {
            records.put(fieldName, createRecordsMap((KaitaiStruct) value, false));
          } else if (value instanceof String) {
            records.put(fieldName, parseNumericValue((String) value));
          } else {
            records.put(fieldName, value);
          }
        } else {
          Object fieldValue = packet.getClass().getDeclaredMethod(field.getName()).invoke(packet);
          if (fieldValue instanceof KaitaiStruct) {
            records.put(fieldName.replace("()", ""), createRecordsMap((KaitaiStruct) fieldValue, false));
          } else {
            records.put(fieldName.replace("()", ""), fieldValue instanceof String ? parseNumericValue((String) fieldValue) : fieldValue);
          }
        }
      } catch (IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
        LOGGER.error("Error while decoding packet", e);
        throw new DecodingException("Error while decoding packet", e);
      }
    }
    return flattenMap(records);
  }


  private static boolean shouldSkipField(String fieldName) {
    return fieldName.endsWith("Hidden") || FIELDS_TO_SKIP.contains(fieldName);
  }

  @SuppressWarnings("unchecked")
  private static Map<String, Object> flattenMap(Map<String, Object> original) {
    Map<String, Object> flattened = new HashMap<>();
    for (Map.Entry<String, Object> entry : original.entrySet()) {
      Object value = entry.getValue();
      if (value instanceof Map) {
        Map<String, Object> subMap = flattenMap((Map<String, Object>) value);
        flattened.putAll(subMap);
      } else if (value instanceof List) {
        flattenList(flattened, entry.getKey(), (List<Object>) value);
      } else if (value instanceof KaitaiStruct) {
        Map<String, Object> nestedObject = createRecordsMap((KaitaiStruct) value, false);
        flattened.putAll(new HashMap<>(nestedObject));
      } else {
        flattened.put(entry.getKey(), value);
      }
    }
    return flattened;
  }

  private static void flattenList(Map<String, Object> flattened, String key, List<Object> list) {
    for (Object value : list) {
      if (value instanceof Map) {
        @SuppressWarnings("unchecked")
        Map<String, Object> subMap = flattenMap((Map<String, Object>) value);
        flattened.putAll(subMap);
      } else if (value instanceof KaitaiStruct) {
        Map<String, Object> nestedObject = createRecordsMap((KaitaiStruct) value, false);
        flattened.putAll(new HashMap<>(nestedObject));
      } else {
        flattened.put(key, value);
      }
    }
  }


  private static Map<String, String> loadFieldNameMapping(String className) {
    Gson gson = new Gson();
    Map<String, String> fieldNameMapping = new HashMap<>();
    String fileName = "/kaitai_field_name_mappings/" + className + ".json";

    try (InputStream inputStream = KaitaiPacketDecoder.class.getResourceAsStream(fileName)) {
      if (inputStream != null) {
        InputStreamReader reader = new InputStreamReader(inputStream, StandardCharsets.UTF_8);
        Type mapType = new TypeToken<HashMap<String, String>>() {
        }.getType();
        fieldNameMapping = gson.fromJson(reader, mapType);
      }
    } catch (IOException e) {
      LOGGER.warn("Error reading " + fileName + ": " + e.getMessage());
    }

    return fieldNameMapping;
  }

}
