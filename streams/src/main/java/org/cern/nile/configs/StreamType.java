package org.cern.nile.configs;

/**
 * Enum representing the types of streams supported by the application.
 */
public enum StreamType {

  ROUTING,
  DECODING,
  ENRICHMENT

}
