package org.cern.nile.decoders.generated;

import static org.junit.jupiter.api.Assertions.assertEquals;

import com.google.gson.JsonParser;
import java.util.Map;
import org.cern.nile.decoders.KaitaiPacketDecoder;
import org.cern.nile.exceptions.DecodingException;
import org.junit.jupiter.api.Test;

public class LoraCrackSensorsDecoderTest {

  private static final String LORA_CRACK_SENSORS_DATA_FRAME = "{\"data\":\"AAc/3sLha7FAX8b9\"}";

  @Test
  void decode_DecodesMessageData_forLoraCrackSensorsPacket() throws DecodingException {
    Map<String, Object> packet =
        KaitaiPacketDecoder.decode(JsonParser.parseString(LORA_CRACK_SENSORS_DATA_FRAME).getAsJsonObject().get("data"), LoraCrackSensorsPacket.class);
    assertEquals(18.55, packet.get("temperature"));
    assertEquals(3545.62, packet.get("batteryVoltage"));
    assertEquals((float) -112.710335, packet.get("displacementRaw"));
    assertEquals((float) 3.4965203, packet.get("displacementPhysical"));
  }

}
