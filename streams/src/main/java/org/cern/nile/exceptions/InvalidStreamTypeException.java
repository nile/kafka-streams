package org.cern.nile.exceptions;

public class InvalidStreamTypeException extends IllegalArgumentException {

  public InvalidStreamTypeException(String message) {
    super(message);
  }

}