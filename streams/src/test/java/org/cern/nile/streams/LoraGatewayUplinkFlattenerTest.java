package org.cern.nile.streams;

import static org.junit.jupiter.api.Assertions.assertEquals;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.junit.jupiter.api.Test;

public class LoraGatewayUplinkFlattenerTest {
  
  private static final String MESSAGE = "{\"phyPayload\":\"ACvQ6AALowQAK9DoAAujBABvBuLXGcg=\",\"txInfo\":{\"frequency\":868100000,"
      + "\"modulation\":\"LORA\",\"loRaModulationInfo\":{\"bandwidth\":125,\"spreadingFactor\":9,\"codeRate\":\"4/5\","
      + "\"polarizationInversion\":false}},\"rxInfo\":{\"gatewayID\":\"/MI9//4gp3U=\",\"time\":\"2021-03-23T09:40:08.810564582Z\","
      + "\"timeSinceGPSEpoch\":null,\"rssi\":-112,\"loRaSNR\":-7.2,\"channel\":0,\"rfChain\":1,\"board\":0,\"antenna\":0,\"location\":null,"
      + "\"fineTimestampType\":\"NONE\",\"context\":\"ExAXBA==\",\"uplinkID\":\"ovc/CJfBSCe578q2Woh18g==\",\"crcStatus\":\"CRC_OK\"}}";

  private static final String EXPECTED_REMOVED_DATA_FIELD_OUTPUT =
      "{\"txInfo\":{\"frequency\":868100000,"
          + "\"loRaModulationInfo\":{\"bandwidth\":125,\"spreadingFactor\":9,\"codeRate\":\"4/5\","
          + "\"polarizationInversion\":false}},\"rxInfo\":{\"gatewayID\":\"/MI9//4gp3U=\",\"time\":\"2021-03-23T09:40:08.810564582Z\","
          + "\"timeSinceGPSEpoch\":null,\"rssi\":-112,\"loRaSNR\":-7.2,\"channel\":0,\"rfChain\":1,\"board\":0,\"antenna\":0,\"location\":null,"
          + "\"fineTimestampType\":\"NONE\",\"context\":\"ExAXBA==\",\"uplinkID\":\"ovc/CJfBSCe578q2Woh18g==\",\"crcStatus\":\"CRC_OK\"}}";

  @Test
  void removeDataField_RemovesPhyPayloadAndTxInfoModulationFields_ForJsonObject() {
    final LoraGatewayUplinkFlattener flattener = new LoraGatewayUplinkFlattener();
    final JsonObject msgAsJson = JsonParser.parseString(MESSAGE).getAsJsonObject();
    final JsonObject output = flattener.removeDataField(msgAsJson);
    final JsonObject expectedOutput =
        JsonParser.parseString(EXPECTED_REMOVED_DATA_FIELD_OUTPUT).getAsJsonObject();
    assertEquals(expectedOutput, output);
  }
}
