package org.cern.nile.streams;

import com.github.wnameless.json.flattener.JsonFlattener;
import com.google.gson.JsonObject;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.kstream.Consumed;
import org.cern.nile.json.JsonSerde;

public class LoraGatewayUplinkFlattener extends AbstractStream {

  @Override
  protected void createTopology(StreamsBuilder builder) {
    builder
        .stream(sourceTopic, Consumed.with(Serdes.String(), new JsonSerde()))
        .mapValues(this::removeDataField)
        .mapValues(this::flatten)
        .to(sinkTopic);
  }

  JsonObject removeDataField(JsonObject value) {
    try {
      LOGGER.info(value.toString());
      value.remove("phyPayload");
      value.get("txInfo").getAsJsonObject().remove("modulation");
    } catch (Exception e) {
      if (streams != null) {
        LOGGER.warn(String.format("Streams state is: %s", streams.state().toString()));
      } else {
        LOGGER.warn("Streams object is null.");
      }
      throw new RuntimeException(e);
    }
    return value;
  }

  Object flatten(JsonObject value) {
    return new JsonFlattener(value.toString()).withSeparator('_').flattenAsMap();
  }

}
